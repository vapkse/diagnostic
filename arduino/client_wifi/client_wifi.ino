/* The circuit:
 * SD card attached to SPI bus as follows:
 ** MOSI - pin 51
 ** MISO - pin 50
 ** CLK - pin 52
 ** CS - pin 53
 */
#include <SimpleTimer.h>
#include <EasyTransfer.h>
#include <AmpTransfer.h>
#include <SD.h>

extern int __bss_end;
extern void *__brkval;

static const boolean debug = false;

EasyTransfer readDatas1; 
dataResponse readDatasStruct1;

EasyTransfer readDatas2; 
dataResponse readDatasStruct2;

EasyTransfer sendRequest1;  
ampRequest sendRequestStruct1;

EasyTransfer sendRequest2;  
ampRequest sendRequestStruct2;

ampRequestInfos ampInfo1;
ampRequestInfos ampInfo2;

unsigned long const MAX_LOG_SIZE = 2000000;
unsigned int fileIndex;
boolean sdCardInitialized = false;

boolean resetSent = false;

// LED vars 
const int ledPin = 13;

// On the Ethernet Shield, CS is pin 4. Note that even if it's not
// used as the CS pin, the hardware CS pin (10 on most Arduino boards,
// 53 on the Mega) must be left as an output or the SD library
// functions will not work.
const int chipSelect = 53;

unsigned long stabilizedTime = 0;
unsigned long failTime = 0;
unsigned long lastReceivedTime = 0;

String httpReceived;
String lineReceived;

unsigned int const MAX_BUFFER_SIZE = 2048;
char json[MAX_BUFFER_SIZE];

void setup(){
  Serial.begin(9600);  
  Serial1.begin(9600); 
  Serial2.begin(9600);  
  Serial3.begin(9600);  

  // make sure that the default chip select pin is set to
  // output, even if you don't use it:
  pinMode(chipSelect, OUTPUT);

  pinMode(ledPin, OUTPUT);

  readDatas1.begin(details(readDatasStruct1), &Serial2);
  readDatas2.begin(details(readDatasStruct2), &Serial3);

  sendRequest1.begin(details(sendRequestStruct1), &Serial2);
  sendRequest2.begin(details(sendRequestStruct2), &Serial3);

  // Init SD card
  if (debug)
  {
    Serial.println("Initializing SD card...");
  }

  // see if the card is present and can be initialized:
  if (SD.begin(chipSelect)) {
    if (debug)
    {
      Serial.println("Card initialized.");
    }

    if (!SD.exists("datas"))
    {
      SD.mkdir("datas");
    }

    if (SD.exists("datas"))
    {
      fileIndex = getLogFileIndex();      
      sdCardInitialized = true;  
    }
  }
  else if (debug)
  {
    Serial.println("Card failed, or not present");
  }

  ampInfo1.id = 0;
  ampInfo1.lastLogTime = 0;
  ampInfo1.lastReceivedTime = 0;
  ampInfo1.canBeTransfered = 0;

  ampInfo2.id = 0;
  ampInfo2.lastLogTime = 0;
  ampInfo2.lastReceivedTime = 0;
  ampInfo2.canBeTransfered = 0;

  httpReceived.reserve(256);
  lineReceived.reserve(512);
}

void parseHttpRequest(String httpReceived)
{
  if (debug)
  {
    Serial.println("httpReceived: " + httpReceived);
  }

  // Link to the reply json  
  char* buffer = json;  
  strncpy(buffer++, "{", 1);
  char* pstart = buffer;

  String error = NULL;
  const char *data = httpReceived.c_str();
  char* preq = strstr (data,"GET /");
  if (preq)
  {
    preq += 5;

    int logIndex = -1;
    char* p = strstr (preq,"loglist");
    if (p) {
      // Send log list
      if (debug){
        Serial.println("Log list requested");
      }

      sscanf(preq, "loglist?next=%d", &logIndex);

      sendJsonLogList(logIndex);
      return;
    }

    p = strstr (preq,"favicon.ico");
    if (p)
    {
      Serial1.println("HTTP/1.0 404");
      Serial1.println("\r\n");
      Serial1.println("\r\n");  
      return;
    }

	logIndex = -1;
	p = strstr (preq,"log");
    if (p) {
		// File operation
		if (debug){
			Serial.println("File operation requested");
		}

		sscanf(preq, "log?read=%d", &logIndex);
		if (logIndex >= 0)
		{
		  // open the file. note that only one file can be open at a time,
		  // so you have to close this one before opening another.
		  String logFileName = "datas/" + String(logIndex);
		  File dataFile = SD.open(logFileName.c_str());

		  // if the file is available, read it:
		  if (dataFile) {
			while (dataFile.available()) {
			  Serial1.write(dataFile.read());
			}
			dataFile.close();
			return;
		  }  
		  else
		  {
			error = "\"error\": \"File not found:" + logFileName + "\"";
		  }
		}

		logIndex = -1;
		sscanf(preq, "log?delete=%d", &logIndex);
		if (logIndex >= 0)
		{
		  String logFileName = "datas/" + String(logIndex);
		  if (SD.remove(&logFileName[0]))
		  {
			if (pstart < buffer)
			{
			  strncpy(buffer++, ",", 1);
			}

			String deleteresult = "\"deleted\": \"" + String(logIndex) + "\"";
			strncpy (buffer, deleteresult.c_str(), deleteresult.length());
			buffer += deleteresult.length();   
		  }
		  else
		  {
			error = "\"error\": \"File not found:" + logFileName + "\"";
		  }
		}
	}

    // Search for commands
    p = strstr (preq,"reset");
    if (p)
    {
      // Send reset to arduino parent
      if (debug){
        Serial.println("Reset requested");
      }    
      String confirm;  
      if (!resetSent)
      {
        resetSent = true;
        sendReset();
        confirm = "\"reset\": true";
      }
      else
      {
        confirm = "\"reset\": false";
      }
      strncpy (buffer, confirm.c_str(), confirm.length());
      buffer += confirm.length();    
    }
    else
    {
      resetSent = false;
    }

    boolean amp1InFunction = ampInfo1.lastReceivedTime > 0 && millis() - ampInfo1.lastReceivedTime < 5000;
    boolean amp2InFunction = ampInfo2.lastReceivedTime > 0 && millis() - ampInfo2.lastReceivedTime < 5000;

    p = strstr (preq,"datas");
    if (p)
    {
      if (ampInfo1.canBeTransfered != 0 && amp1InFunction)
      {
        String sid = "id=" + String(ampInfo1.id);
        p = strstr (preq, sid.c_str());
        if (p)
        {          
          if (debug){
            Serial.println("Datas requested for amp " + String(ampInfo1.id));
          }

          if (pstart < buffer)
          {
            strncpy(buffer++, ",", 1);
          }

          String sname = "\"datas_" + String(ampInfo1.id) + "\":";
          strncpy (buffer, sname.c_str(), sname.length());
          buffer += sname.length();              
          buffer = fillJsonAmpDatas(buffer, readDatasStruct1);

          ampInfo1.canBeTransfered = 0;
        }
      }

      if (ampInfo2.canBeTransfered != 0 && amp2InFunction)
      {
        String sid = "id=" + String(ampInfo2.id);
        p = strstr (preq, sid.c_str());
        if (p)
        {          
          if (debug){
            Serial.println("Datas requested for amp " + String(ampInfo2.id));
          }

          if (pstart < buffer)
          {
            strncpy(buffer++, ",", 1);
          }

          String sname = "\"datas_" + String(ampInfo2.id) + "\":";
          strncpy (buffer, sname.c_str(), sname.length());
          buffer += sname.length();   
          buffer = fillJsonAmpDatas(buffer, readDatasStruct2);

          ampInfo2.canBeTransfered = 0;
        }
      }
    }

    p = strstr (preq,"status");
    if (p)
    {
      // Get datas
      if (debug){
        Serial.println("Status requested");
      }

      if (pstart < buffer)
      {
        strncpy(buffer++, ",", 1);
      }

      String status = "\"status\":[";       
      if (amp1InFunction)
      {
        status += ampInfo1.id;
      }
      if (amp1InFunction && amp2InFunction)
      {
        status += ',';
      }
      if (amp2InFunction)
      {
        status += ampInfo2.id;
      }        
      status += "]";       

      strncpy (buffer, status.c_str(), status.length());
      buffer += status.length();    
    }
  }
  else
  {
    error = "\"error\": \"501 Unsupported HTTP request\"";
  }

  // Add error
  if (error != NULL)
  {
    if (pstart < buffer)
    {
      strncpy(buffer++, ",", 1);
    }
    strncpy (buffer, error.c_str(), error.length());
    buffer += error.length();    
  }

  strncpy (buffer++, "}", 1);
  strncpy (buffer, "\0", 1);  

  sendJson(json);

  /*else if (strncmp("deletelog?", data, 10) == 0) {
   data += 10;
   
   // delete requested log
   if (debug){
   Serial.println("delete log :");
   }
   deleteLog();
   }*/
}

unsigned int getLogFileIndex()
{

  File dir = SD.open("datas");
  dir.rewindDirectory(); 
  File entry = dir.openNextFile();
  unsigned int highestIndex = 0;
  while(entry) {   
    String name = entry.name();    
    int index = atoi(name.c_str());

    if (highestIndex <= index)
    {
      highestIndex++;
    }

    entry.close(); 
    entry = dir.openNextFile();
  }  

  return highestIndex;
}

void processDatas(dataResponse datas, ampRequestInfos* ampInfo)
{
  if (datas.message == MESSAGE_SENDVALUES)
  {
    if (debug)
    {
      Serial.println("Datas revceived for " + String(datas.id));
    }

    // If the amp is stabilized from more than 30 seconds, transfer datas only all 5 seconds to prevent big logs
    unsigned long time = millis();
    unsigned long logDelay = 0;
    if (datas.stepMaxTime == 0 && datas.errorNumber == 0)
    {
      // Stabilized
      if (stabilizedTime == 0)
      {
        stabilizedTime = time;
      }
      else if (time - stabilizedTime > 30000)
      {
        logDelay = 5000;
      }
    }
    else
    {
      stabilizedTime = 0;
    }

    // If tha amp has fail from more than 30 seconds, transfer datas only all 5 seconds to prevent big logs
    if (datas.errorNumber > 0)
    {
      // Stabilized
      if (failTime == 0)
      {
        failTime = time;
      }
      else if (time - failTime > 30000)
      {
        logDelay = 5000;
      }
    }
    else
    {
      failTime = 0;
    }    

    // if SD CARD is available, write to it:
    boolean logSuccess = sdCardInitialized;
    if (sdCardInitialized && time - ampInfo->lastLogTime > logDelay)
    {    
      if (debug)
      {
        Serial.println("Log to file for " + String(datas.id));
      }

      // Create json object to log      
      char* buffer = json;
      buffer = fillJsonAmpDatas(buffer, datas);
      strncpy (buffer, "\0", 1);  

      String fileName = "datas/" + String(fileIndex);
      const char * file = fileName.c_str();
      File dataFile = SD.open(file, FILE_WRITE);
      unsigned long logSize;
      if (dataFile)
      {
        if (debug)
        {
          Serial.println("SD card, Log to file " + fileName);
        }

        // Print to log file
        dataFile.print(json);
        dataFile.print(',');
        logSize = dataFile.size();
        dataFile.close();

        if (logSize > MAX_LOG_SIZE)
        {
          fileIndex++;
        }
      }   
      else
      {
        logSuccess = false;
      }   

      // Store last transfered time
      ampInfo->lastLogTime = time; 
    }

    if (!logSuccess) 
    {
      if (debug)
      {
        Serial.println("Fail to open file log file");
      }   

      digitalWrite(ledPin, HIGH);
      delay(1000);
      digitalWrite(ledPin, LOW);
    }

    ampInfo->id = datas.id;
    ampInfo->lastReceivedTime = time; 
    ampInfo->canBeTransfered = 1;

    lastReceivedTime = millis();
  }
  else if (debug)
  {
    Serial.println("Unknown message received from " + String(datas.id) + " number: " + datas.message);
  }
}

void sendReset()
{
  sendRequestStruct1.message = MESSAGE_RESET;
  sendRequestStruct1.id = MESSAGE_ID_ALL;
  sendRequest1.sendData();
  sendRequestStruct2.message = MESSAGE_RESET;
  sendRequestStruct2.id = MESSAGE_ID_ALL;
  sendRequest2.sendData();  
}

void sendJsonLogList(unsigned int start)
{
  // Create a reply json  
  char* buffer = json;  
  char* pmax = json;  
  pmax += MAX_BUFFER_SIZE - 30;

  strncpy(buffer++, "{", 1);

  if (sdCardInitialized)
  {    
    String header = "\"loglist\":[";
    strncpy (buffer, header.c_str(), header.length());
    buffer += header.length();

    File dir = SD.open("datas");
    dir.rewindDirectory(); 
    File entry = dir.openNextFile();
    boolean isFirst = true;
    String next;
    while(entry) {  
      char* entryName = entry.name();
      if (start >= 0 && start == atoi(entryName))  
      {
        start = -1;
      }

      if (start == -1)
      {
        if (!isFirst)
        {
          strncpy (buffer++, ",", 1);
        }
        isFirst = false;

        // Add file name
        String name = "{\"name\":\"" + String(entryName) + "\"";   
        strncpy (buffer, name.c_str(), name.length());
        buffer += name.length();

        // Add file size
        String size = ",\"size\":" + String(entry.size());
        strncpy (buffer, size.c_str(), size.length());
        buffer += size.length();

        strncpy (buffer++, "}", 1);
      }

      entry.close(); 
      entry = dir.openNextFile();

      if (entry && buffer > pmax)
      {
        // Add indicator        
        next = ",\"next\":\"" + String(entry.name()) + "\"";   
        entry.close(); 
        break;
      }
    }  

    strncpy(buffer++, "]", 1);

    if (next)
    {
      strncpy (buffer, next.c_str(), next.length());       
      buffer += next.length();        
    }
  }

  // Add footer
  strncpy(buffer++, "}", 1);
  strncpy(buffer++, "\0", 1);

  sendJson(json);  
}

char* fillJsonAmpDatas(char* buffer, dataResponse datasStruct)
{
  strncpy(buffer++, "{", 1);

  strncpy (buffer, "\"id\": ", 6);
  buffer += 6;  
  String id = String(datasStruct.id);
  strncpy (buffer, id.c_str(), id.length());
  buffer += id.length();
  strncpy (buffer++, ",", 1);

  strncpy (buffer, "\"step\": ", 8);
  buffer += 8;  
  String step = String(datasStruct.step);
  strncpy (buffer, step.c_str(), step.length());
  buffer += step.length();
  strncpy (buffer++, ",", 1);

  strncpy (buffer, "\"steptmax\": ", 12);
  buffer += 12;  
  String stepMaxTime = String(datasStruct.stepMaxTime);
  strncpy (buffer, stepMaxTime.c_str(), stepMaxTime.length());
  buffer += stepMaxTime.length();
  strncpy (buffer++, ",", 1);

  strncpy (buffer, "\"steptelaps\": ", 14);
  buffer += 14;  
  String stepElapsedTime = String(datasStruct.stepElapsedTime);
  strncpy (buffer, stepElapsedTime.c_str(), stepElapsedTime.length());
  buffer += stepElapsedTime.length();
  strncpy (buffer++, ",", 1);

  strncpy (buffer, "\"stepvmax\": ", 12);
  buffer += 12;  
  String stepMaxValue = String(datasStruct.stepMaxValue);
  strncpy (buffer, stepMaxValue.c_str(), stepMaxValue.length());
  buffer += stepMaxValue.length();
  strncpy (buffer++, ",", 1);

  strncpy (buffer, "\"stepval\": ", 11);
  buffer += 11;  
  String stepCurValue = String(datasStruct.stepCurValue);
  strncpy (buffer, stepCurValue.c_str(), stepCurValue.length());
  buffer += stepCurValue.length();
  strncpy (buffer++, ",", 1);

  strncpy (buffer, "\"tick\": ", 8);
  buffer += 8;  
  String tickCount = String(datasStruct.tickCount);
  strncpy (buffer, tickCount.c_str(), tickCount.length());
  buffer += tickCount.length();
  strncpy (buffer++, ",", 1);

  strncpy (buffer, "\"errn\": ", 8);
  buffer += 8;  
  String errorNumber = String(datasStruct.errorNumber);
  strncpy (buffer, errorNumber.c_str(), errorNumber.length());
  buffer += errorNumber.length();
  strncpy (buffer++, ",", 1);

  strncpy (buffer, "\"errt\": ", 8);
  buffer += 8;  
  String errorTube = String(datasStruct.errorTube);
  strncpy (buffer, errorTube.c_str(), errorTube.length());
  buffer += errorTube.length();
  strncpy (buffer++, ",", 1);
    
  strncpy (buffer, "\"ref\": ", 7);
  buffer += 7;  
  String refValue = String(datasStruct.refValue);
  strncpy (buffer, refValue.c_str(), refValue.length());
  buffer += refValue.length();
  strncpy (buffer++, ",", 1);
  
  strncpy (buffer, "\"min\": ", 7);
  buffer += 7;  
  String minValue = String(datasStruct.minValue);
  strncpy (buffer, minValue.c_str(), minValue.length());
  buffer += minValue.length();
  strncpy (buffer++, ",", 1);

  strncpy (buffer, "\"max\": ", 7);
  buffer += 7;  
  String maxValue = String(datasStruct.maxValue);
  strncpy (buffer, maxValue.c_str(), maxValue.length());
  buffer += maxValue.length();
  strncpy (buffer++, ",", 1);
  
  // Add an array with the values
  strncpy (buffer, "\"val\": [", 8);
  buffer += 8;  
  
  String measure = String(datasStruct.measure0);
  strncpy (buffer, measure.c_str(), measure.length());
  buffer += measure.length();
  strncpy (buffer++, ",", 1);
  
  measure = String(datasStruct.measure1);
  strncpy (buffer, measure.c_str(), measure.length());
  buffer += measure.length();
  strncpy (buffer++, ",", 1);
  
  measure = String(datasStruct.measure2);
  strncpy (buffer, measure.c_str(), measure.length());
  buffer += measure.length();
  strncpy (buffer++, ",", 1);
  
  measure = String(datasStruct.measure3);
  strncpy (buffer, measure.c_str(), measure.length());
  buffer += measure.length();
  strncpy (buffer++, ",", 1);
  
  measure = String(datasStruct.measure4);
  strncpy (buffer, measure.c_str(), measure.length());
  buffer += measure.length();
  strncpy (buffer++, ",", 1);
  
  measure = String(datasStruct.measure5);
  strncpy (buffer, measure.c_str(), measure.length());
  buffer += measure.length();
  strncpy (buffer++, ",", 1);
  
  measure = String(datasStruct.measure6);
  strncpy (buffer, measure.c_str(), measure.length());
  buffer += measure.length();
  strncpy (buffer++, ",", 1);
  
  measure = String(datasStruct.measure7);
  strncpy (buffer, measure.c_str(), measure.length());
  buffer += measure.length();
  strncpy (buffer++, "]", 1);
  strncpy (buffer++, ",", 1);

  // Add an array with the values
  strncpy (buffer, "\"out\": [", 8);
  buffer += 8;  
  
  String output = String(datasStruct.output0);
  strncpy (buffer, output.c_str(), output.length());
  buffer += output.length();
  strncpy (buffer++, ",", 1);
  
  output = String(datasStruct.output1);
  strncpy (buffer, output.c_str(), output.length());
  buffer += output.length();
  strncpy (buffer++, ",", 1);
  
  output = String(datasStruct.output2);
  strncpy (buffer, output.c_str(), output.length());
  buffer += output.length();
  strncpy (buffer++, ",", 1);
  
  output = String(datasStruct.output3);
  strncpy (buffer, output.c_str(), output.length());
  buffer += output.length();
  strncpy (buffer++, ",", 1);
  
  output = String(datasStruct.output4);
  strncpy (buffer, output.c_str(), output.length());
  buffer += output.length();
  strncpy (buffer++, ",", 1);
  
  output = String(datasStruct.output5);
  strncpy (buffer, output.c_str(), output.length());
  buffer += output.length();
  strncpy (buffer++, ",", 1);
  
  output = String(datasStruct.output6);
  strncpy (buffer, output.c_str(), output.length());
  buffer += output.length();
  strncpy (buffer++, ",", 1);
  
  output = String(datasStruct.output7);
  strncpy (buffer, output.c_str(), output.length());
  buffer += output.length();
  strncpy (buffer++, "]", 1);
  strncpy (buffer++, ",", 1);

  // Add an array with the temperatures
  strncpy (buffer, "\"temp\": [", 9);
  buffer += 9;  
  
  String temp = String(datasStruct.temperature0);
  strncpy (buffer, temp.c_str(), temp.length());
  buffer += temp.length();
  strncpy (buffer++, ",", 1);
  
  temp = String(datasStruct.temperature1);
  strncpy (buffer, temp.c_str(), temp.length());
  buffer += temp.length();
  strncpy (buffer++, ",", 1);
  
  temp = String(datasStruct.temperature2);
  strncpy (buffer, temp.c_str(), temp.length());
  buffer += temp.length();
  strncpy (buffer++, ",", 1);
  
  temp = String(datasStruct.temperature3);
  strncpy (buffer, temp.c_str(), temp.length());
  buffer += temp.length();
  strncpy (buffer++, "]", 1);
  strncpy (buffer++, "}", 1);

  return buffer;
}

void sendJson(char* jsn)
{
  if (debug)
  {
    Serial.println("send Json: ");
    Serial.println(jsn);
  }

  Serial1.println("HTTP/1.1 200 OK");
  Serial1.println("Content-Type: application/json; charset=UTF-8");
  //on HLF-RM04 better to close connection so that each client gets its own response
  //otherwise if Conn not closed then response to Client2 will actually be send to 1st unclosed connection to Client1 (try it by connecting from say Chrome and IE to IP running HLK-RM04 server)
  Serial1.println("Connection: close");    // the connection will be closed after sending back the response to client
  Serial1.print("Content-Length: ");    //need this otherwise Chrome shows page but Cursor 'wait', IE just 'wait'
  Serial1.print(strlen(jsn) + 4);      // Add 4 for the two \r\n      
  Serial1.println("\r\n");
  Serial1.println(jsn);
  Serial1.println("\r\n");
  Serial1.println("\r\n");
}

int getFreeMemory()
{
  int free_memory;

  if((int)__brkval == 0)
    free_memory = ((int)&free_memory) - ((int)&__bss_end);
  else
    free_memory = ((int)&free_memory) - ((int)__brkval);

  return free_memory;
}

void loop(){
  if (getFreeMemory() < 128)
  {
    Serial.println("Not enough memory");
    digitalWrite(ledPin, HIGH);
    delay(5000);
    digitalWrite(ledPin, LOW);
  }
}

/*
  SerialEvent occurs whenever a new data comes in the
 hardware serial RX.  This routine is run between each
 time loop() runs, so using delay inside loop can delay
 response.  Multiple bytes of data may be available.
 */
void serialEvent2() {
  if(readDatas1.receiveData()){
    processDatas(readDatasStruct1, &ampInfo1);
    digitalWrite(ledPin, HIGH);
    delay(1);
    digitalWrite(ledPin, LOW);
  }
}

void serialEvent3() {
  if(readDatas2.receiveData()){
    processDatas(readDatasStruct2, &ampInfo2);
    digitalWrite(ledPin, HIGH);
    delay(1);
    digitalWrite(ledPin, LOW);
  }
}

void serialEvent1() {
  // Read serial from wifi
  while (Serial1.available() > 0)
  {
    char received = Serial1.read();

    // Process message when a blank line
    if (received == 10)
    {
      // ignore
    }
    else if (received == 13)
    {      
      if (lineReceived == "")
      {
        parseHttpRequest(httpReceived);
        httpReceived = ""; // Clear received buffer
      }
      else
      {
        httpReceived += lineReceived + '\n';
      }

      lineReceived = "";
    }
    else
    {
      lineReceived += received; 
    }
  }
}

