/* The circuit:
 * SD card attached to SPI bus as follows:
 ** MOSI - pin 51
 ** MISO - pin 50
 ** CLK - pin 52
 ** CS - pin 53
 */
#include <EasyTransfer.h>
#include <AmpTransfer.h>
#include <SD.h>
#include <UTFT.h>

uint8_t QQE0312SPUD_ID = 122;
uint8_t QQE0312DPP_ID = 128;

extern int __bss_end;
extern void *__brkval;

// Declare which fonts we will be using
extern uint8_t SmallFont[];
extern uint8_t BigFont[];
extern uint8_t SevenSegNumFont[];

// Set the pins to the correct ones for your development shield
// ------------------------------------------------------------
// Arduino Uno / 2009:
// -------------------
// Standard Arduino Uno/2009 shield            : <display model>,A5,A4,A3,A2
// DisplayModule Arduino Uno TFT shield        : <display model>,A5,A4,A3,A2
//
// Arduino Mega:
// -------------------
// Standard Arduino Mega/Due shield            : <display model>,38,39,40,41
// CTE TFT LCD/SD Shield for Arduino Mega      : <display model>,38,39,40,41
//
// Remember to change the model parameter to suit your display module!
UTFT myGLCD(ITDB32S,38,39,40,41);
unsigned int const X_MAX = 26;
unsigned int const Y_MAX = 14;

char buffer[X_MAX * Y_MAX];
char tmpBuffer[X_MAX * Y_MAX];
int currentLine = 0;

static const boolean debug = true;

EasyTransfer readDatas; 
dataResponse readDatasStruct;

EasyTransfer sendRequest;  
ampRequest sendRequestStruct;

ampRequestInfos ampInfo;

unsigned long const MAX_LOG_SIZE = 2000000;
unsigned int fileIndex;
boolean sdCardInitialized = false;

// LED vars 
const int ledPin = 13;

// On the Ethernet Shield, CS is pin 4. Note that even if it's not
// used as the CS pin, the hardware CS pin (10 on most Arduino boards,
// 53 on the Mega) must be left as an output or the SD library
// functions will not work.
const int chipSelect = 53;

unsigned long stabilizedTime = 0;
unsigned long failTime = 0;
unsigned long lastReceivedTime = 0;

unsigned int const MAX_BUFFER_SIZE = 2048;
char json[MAX_BUFFER_SIZE];

void setup(){
  Serial.begin(9600);  
  Serial3.begin(9600);  

  // make sure that the default chip select pin is set to
  // output, even if you don't use it:
  pinMode(chipSelect, OUTPUT);

  pinMode(ledPin, OUTPUT);

  pinMode(12, OUTPUT);
  digitalWrite(12, HIGH);

  readDatas.begin(details(readDatasStruct), &Serial3);

  sendRequest.begin(details(sendRequestStruct), &Serial);

  // Init SD card
  if (debug)
  {
    Serial.println("Initializing SD card...");
  }

  // see if the card is present and can be initialized:
  if (SD.begin(chipSelect)) {
    if (debug)
    {
      Serial.println("Card initialized.");
    }

    if (!SD.exists("datas"))
    {
      SD.mkdir("datas");
    }

    if (SD.exists("datas"))
    {
      fileIndex = getLogFileIndex();      
      sdCardInitialized = true;  
    }
  }
  else if (debug)
  {
    Serial.println("Card failed, or not present");
  }

  ampInfo.id = 0;
  ampInfo.lastLogTime = 0;
  ampInfo.lastReceivedTime = 0;
  ampInfo.canBeTransfered = 0;

  myGLCD.InitLCD();

  myGLCD.clrScr();
  myGLCD.setFont(BigFont);
  myGLCD.setColor(0, 255, 0);
  myGLCD.setBackColor(0, 0, 0);
}

unsigned int getLogFileIndex()
{

  File dir = SD.open("datas");
  dir.rewindDirectory(); 
  File entry = dir.openNextFile();
  unsigned int highestIndex = 0;
  while(entry) {   
    String name = entry.name();    
    int index = atoi(name.c_str());

    if (highestIndex <= index)
    {
      highestIndex++;
    }

    entry.close(); 
    entry = dir.openNextFile();
  }  

  return highestIndex;
}

void processDatas(dataResponse datas, ampRequestInfos* ampInfo)
{
  if (datas.message == MESSAGE_SENDVALUES)
  {
    if (debug)
    {
      Serial.println("Datas revceived for " + String(datas.id));
    }

    if (datas.id == QQE0312SPUD_ID){
      printQQE0312SPUD(datas);
    } 
    else if (datas.id == QQE0312DPP_ID) {
      printQQE0312DPP(datas);
    } 
    else {
      printJSON(datas);
    } 

    // If the amp is stabilized from more than 30 seconds, transfer datas only all 5 seconds to prevent big logs
    unsigned long time = millis();
    unsigned long logDelay = 0;
    if (datas.stepMaxTime == 0 && datas.errorNumber == 0)
    {
      // Stabilized
      if (stabilizedTime == 0)
      {
        stabilizedTime = time;
      }
      else if (time - stabilizedTime > 30000)
      {
        logDelay = 5000;
      }
    }
    else
    {
      stabilizedTime = 0;
    }

    // If tha amp has fail from more than 30 seconds, transfer datas only all 5 seconds to prevent big logs
    if (datas.errorNumber > 0)
    {
      // Stabilized
      if (failTime == 0)
      {
        failTime = time;
      }
      else if (time - failTime > 30000)
      {
        logDelay = 5000;
      }
    }
    else
    {
      failTime = 0;
    }    

    // if SD CARD is available, write to it:
    boolean logSuccess = sdCardInitialized;
    if (sdCardInitialized && time - ampInfo->lastLogTime > logDelay)
    {    
      if (debug)
      {
        Serial.println("Log to file for " + String(datas.id));
      }

      // Create json object to log      
      char* buffer = json;
      buffer = fillJsonAmpDatas(buffer, datas);
      strncpy (buffer, "\0", 1);  

      String fileName = "datas/" + String(fileIndex);
      const char * file = fileName.c_str();
      File dataFile = SD.open(file, FILE_WRITE);
      unsigned long logSize;
      if (dataFile)
      {
        if (debug)
        {
          Serial.println("SD card, Log to file " + fileName);
        }

        // Print to log file
        dataFile.print(json);
        dataFile.print(',');
        logSize = dataFile.size();
        dataFile.close();

        if (logSize > MAX_LOG_SIZE)
        {
          fileIndex++;
        }
      }   
      else
      {
        logSuccess = false;
      }   

      // Store last transfered time
      ampInfo->lastLogTime = time; 
    }

    if (!logSuccess) 
    {
      if (debug)
      {
        Serial.println("Fail to open file log file");
      }   

      digitalWrite(ledPin, HIGH);
      delay(1000);
      digitalWrite(ledPin, LOW);
    }

    ampInfo->id = datas.id;
    ampInfo->lastReceivedTime = time; 
    ampInfo->canBeTransfered = 1;

    lastReceivedTime = millis();
  }
  else if (debug)
  {
    Serial.println("Unknown message received from " + String(datas.id) + " number: " + datas.message);
  }
}

void sendReset()
{
  sendRequestStruct.message = MESSAGE_RESET;
  sendRequestStruct.id = MESSAGE_ID_ALL;
  sendRequest.sendData();  
}

void sendJsonLogList(unsigned int start)
{
  // Create a reply json  
  char* buffer = json;  
  char* pmax = json;  
  pmax += MAX_BUFFER_SIZE - 30;

  strncpy(buffer++, "{", 1);

  if (sdCardInitialized)
  {    
    String header = "\"loglist\":[";
    strncpy (buffer, header.c_str(), header.length());
    buffer += header.length();

    File dir = SD.open("datas");
    dir.rewindDirectory(); 
    File entry = dir.openNextFile();
    boolean isFirst = true;
    String next;
    while(entry) {  
      char* entryName = entry.name();
      if (start >= 0 && start == atoi(entryName))  
      {
        start = -1;
      }

      if (start == -1)
      {
        if (!isFirst)
        {
          strncpy (buffer++, ",", 1);
        }
        isFirst = false;

        // Add file name
        String name = "{\"name\":\"" + String(entryName) + "\"";   
        strncpy (buffer, name.c_str(), name.length());
        buffer += name.length();

        // Add file size
        String size = ",\"size\":" + String(entry.size());
        strncpy (buffer, size.c_str(), size.length());
        buffer += size.length();

        strncpy (buffer++, "}", 1);
      }

      entry.close(); 
      entry = dir.openNextFile();

      if (entry && buffer > pmax)
      {
        // Add indicator        
        next = ",\"next\":\"" + String(entry.name()) + "\"";   
        entry.close(); 
        break;
      }
    }  

    strncpy(buffer++, "]", 1);

    if (next)
    {
      strncpy (buffer, next.c_str(), next.length());       
      buffer += next.length();        
    }
  }

  // Add footer
  strncpy(buffer++, "}", 1);
  strncpy(buffer++, "\0", 1);

  sendJson(json);  
}

char* fillJsonAmpDatas(char* buffer, dataResponse datasStruct)
{
  strncpy(buffer++, "{", 1);

  strncpy (buffer, "\"id\": ", 6);
  buffer += 6;  
  String id = String(datasStruct.id);
  strncpy (buffer, id.c_str(), id.length());
  buffer += id.length();
  strncpy (buffer++, ",", 1);

  strncpy (buffer, "\"step\": ", 8);
  buffer += 8;  
  String step = String(datasStruct.step);
  strncpy (buffer, step.c_str(), step.length());
  buffer += step.length();
  strncpy (buffer++, ",", 1);

  strncpy (buffer, "\"steptmax\": ", 12);
  buffer += 12;  
  String stepMaxTime = String(datasStruct.stepMaxTime);
  strncpy (buffer, stepMaxTime.c_str(), stepMaxTime.length());
  buffer += stepMaxTime.length();
  strncpy (buffer++, ",", 1);

  strncpy (buffer, "\"steptelaps\": ", 14);
  buffer += 14;  
  String stepElapsedTime = String(datasStruct.stepElapsedTime);
  strncpy (buffer, stepElapsedTime.c_str(), stepElapsedTime.length());
  buffer += stepElapsedTime.length();
  strncpy (buffer++, ",", 1);

  strncpy (buffer, "\"stepvmax\": ", 12);
  buffer += 12;  
  String stepMaxValue = String(datasStruct.stepMaxValue);
  strncpy (buffer, stepMaxValue.c_str(), stepMaxValue.length());
  buffer += stepMaxValue.length();
  strncpy (buffer++, ",", 1);

  strncpy (buffer, "\"stepval\": ", 11);
  buffer += 11;  
  String stepCurValue = String(datasStruct.stepCurValue);
  strncpy (buffer, stepCurValue.c_str(), stepCurValue.length());
  buffer += stepCurValue.length();
  strncpy (buffer++, ",", 1);

  strncpy (buffer, "\"tick\": ", 8);
  buffer += 8;  
  String tickCount = String(datasStruct.tickCount);
  strncpy (buffer, tickCount.c_str(), tickCount.length());
  buffer += tickCount.length();
  strncpy (buffer++, ",", 1);

  strncpy (buffer, "\"errn\": ", 8);
  buffer += 8;  
  String errorNumber = String(datasStruct.errorNumber);
  strncpy (buffer, errorNumber.c_str(), errorNumber.length());
  buffer += errorNumber.length();
  strncpy (buffer++, ",", 1);

  strncpy (buffer, "\"errt\": ", 8);
  buffer += 8;  
  String errorTube = String(datasStruct.errorTube);
  strncpy (buffer, errorTube.c_str(), errorTube.length());
  buffer += errorTube.length();
  strncpy (buffer++, ",", 1);

  strncpy (buffer, "\"ref\": ", 7);
  buffer += 7;  
  String refValue = String(datasStruct.refValue);
  strncpy (buffer, refValue.c_str(), refValue.length());
  buffer += refValue.length();
  strncpy (buffer++, ",", 1);

  strncpy (buffer, "\"min\": ", 7);
  buffer += 7;  
  String minValue = String(datasStruct.minValue);
  strncpy (buffer, minValue.c_str(), minValue.length());
  buffer += minValue.length();
  strncpy (buffer++, ",", 1);

  strncpy (buffer, "\"max\": ", 7);
  buffer += 7;  
  String maxValue = String(datasStruct.maxValue);
  strncpy (buffer, maxValue.c_str(), maxValue.length());
  buffer += maxValue.length();
  strncpy (buffer++, ",", 1);

  // Add an array with the values
  strncpy (buffer, "\"val\": [", 8);
  buffer += 8;  

  String measure = String(datasStruct.measure0);
  strncpy (buffer, measure.c_str(), measure.length());
  buffer += measure.length();
  strncpy (buffer++, ",", 1);

  measure = String(datasStruct.measure1);
  strncpy (buffer, measure.c_str(), measure.length());
  buffer += measure.length();
  strncpy (buffer++, ",", 1);

  measure = String(datasStruct.measure2);
  strncpy (buffer, measure.c_str(), measure.length());
  buffer += measure.length();
  strncpy (buffer++, ",", 1);

  measure = String(datasStruct.measure3);
  strncpy (buffer, measure.c_str(), measure.length());
  buffer += measure.length();
  strncpy (buffer++, ",", 1);

  measure = String(datasStruct.measure4);
  strncpy (buffer, measure.c_str(), measure.length());
  buffer += measure.length();
  strncpy (buffer++, ",", 1);

  measure = String(datasStruct.measure5);
  strncpy (buffer, measure.c_str(), measure.length());
  buffer += measure.length();
  strncpy (buffer++, ",", 1);

  measure = String(datasStruct.measure6);
  strncpy (buffer, measure.c_str(), measure.length());
  buffer += measure.length();
  strncpy (buffer++, ",", 1);

  measure = String(datasStruct.measure7);
  strncpy (buffer, measure.c_str(), measure.length());
  buffer += measure.length();
  strncpy (buffer++, "]", 1);
  strncpy (buffer++, ",", 1);

  // Add an array with the values
  strncpy (buffer, "\"out\": [", 8);
  buffer += 8;  

  String output = String(datasStruct.output0);
  strncpy (buffer, output.c_str(), output.length());
  buffer += output.length();
  strncpy (buffer++, ",", 1);

  output = String(datasStruct.output1);
  strncpy (buffer, output.c_str(), output.length());
  buffer += output.length();
  strncpy (buffer++, ",", 1);

  output = String(datasStruct.output2);
  strncpy (buffer, output.c_str(), output.length());
  buffer += output.length();
  strncpy (buffer++, ",", 1);

  output = String(datasStruct.output3);
  strncpy (buffer, output.c_str(), output.length());
  buffer += output.length();
  strncpy (buffer++, ",", 1);

  output = String(datasStruct.output4);
  strncpy (buffer, output.c_str(), output.length());
  buffer += output.length();
  strncpy (buffer++, ",", 1);

  output = String(datasStruct.output5);
  strncpy (buffer, output.c_str(), output.length());
  buffer += output.length();
  strncpy (buffer++, ",", 1);

  output = String(datasStruct.output6);
  strncpy (buffer, output.c_str(), output.length());
  buffer += output.length();
  strncpy (buffer++, ",", 1);

  output = String(datasStruct.output7);
  strncpy (buffer, output.c_str(), output.length());
  buffer += output.length();
  strncpy (buffer++, "]", 1);
  strncpy (buffer++, ",", 1);

  // Add an array with the temperatures
  strncpy (buffer, "\"temp\": [", 9);
  buffer += 9;  

  String temp = String(datasStruct.temperature0);
  strncpy (buffer, temp.c_str(), temp.length());
  buffer += temp.length();
  strncpy (buffer++, ",", 1);

  temp = String(datasStruct.temperature1);
  strncpy (buffer, temp.c_str(), temp.length());
  buffer += temp.length();
  strncpy (buffer++, ",", 1);

  temp = String(datasStruct.temperature2);
  strncpy (buffer, temp.c_str(), temp.length());
  buffer += temp.length();
  strncpy (buffer++, ",", 1);

  temp = String(datasStruct.temperature3);
  strncpy (buffer, temp.c_str(), temp.length());
  buffer += temp.length();
  strncpy (buffer++, "]", 1);
  strncpy (buffer++, "}", 1);

  return buffer;
}

void sendJson(char* jsn)
{
  if (debug)
  {
    Serial.println("jsn sent: ");
    Serial.println(jsn);
  }

  Serial1.println("HTTP/1.1 200 OK");
  Serial1.println("Content-Type: application/json; charset=UTF-8");
  //on HLF-RM04 better to close connection so that each client gets its own response
  //otherwise if Conn not closed then response to Client2 will actually be send to 1st unclosed connection to Client1 (try it by connecting from say Chrome and IE to IP running HLK-RM04 server)
  Serial1.println("Connection: close");    // the connection will be closed after sending back the response to client
  Serial1.print("Content-Length: ");    //need this otherwise Chrome shows page but Cursor 'wait', IE just 'wait'
  Serial1.print(strlen(jsn) + 4);      // Add 4 for the two \r\n      
  Serial1.println("\r\n");
  Serial1.println(jsn);
  Serial1.println("\r\n");
  Serial1.println("\r\n");
}

int getFreeMemory()
{
  int free_memory;

  if((int)__brkval == 0)
    free_memory = ((int)&free_memory) - ((int)&__bss_end);
  else
    free_memory = ((int)&free_memory) - ((int)__brkval);

  return free_memory;
}

void loop(){
  if (getFreeMemory() < 128)
  {
    Serial.println("Not enough memory");
    digitalWrite(ledPin, HIGH);
    delay(5000);
    digitalWrite(ledPin, LOW);
  }
}

/*
  SerialEvent occurs whenever a new data comes in the
 hardware serial RX.  This routine is run between each
 time loop() runs, so using delay inside loop can delay
 response.  Multiple bytes of data may be available.
 */
void serialEvent3() {
  if(readDatas.receiveData()){
    processDatas(readDatasStruct, &ampInfo);
    digitalWrite(ledPin, HIGH);
    delay(1);
    digitalWrite(ledPin, LOW);
  }
}

void printJSON (dataResponse datas){

}

void printQQE0312DPP (dataResponse datas){
  unsigned int percent1 = datas.measure0 * 100 / 255;
  unsigned int percent2 = datas.measure1 * 100 / 255;
  unsigned int percent3 = datas.measure2 * 100 / 255;
  unsigned int percent4 = datas.measure3 * 100 / 255;
  unsigned int percent5 = datas.measure4 * 100 / 255;
  unsigned int percent6 = datas.measure5 * 100 / 255;
  unsigned int percent7 = datas.measure6 * 100 / 255;
  unsigned int percent8 = datas.measure7 * 100 / 255;

  unsigned int output1 = datas.output0 * 100 / 255;
  unsigned int output2 = datas.output1 * 100 / 255;
  unsigned int output3 = datas.output2 * 100 / 255;
  unsigned int output4 = datas.output3 * 100 / 255;
  unsigned int output5 = datas.output4 * 100 / 255;
  unsigned int output6 = datas.output5 * 100 / 255;
  unsigned int output7 = datas.output6 * 100 / 255;
  unsigned int output8 = datas.output7 * 100 / 255;

  unsigned int percentAirTemp = max(min(datas.temperature0 - 25, 100), 0.1);
  unsigned int percentRegLTemp = max(min(datas.temperature1 - 25, 100), 0.1);
  unsigned int percentRegRTemp = max(min(datas.temperature2  - 25, 100), 0.1);
  unsigned int  modulationPeak = datas.temperature3 * 100 / 255;
  unsigned int  currentStep = datas.step;

  unsigned int percentRef = datas.refValue * 100 / 255;

  myGLCD.print("1:" + String(round(percent1 * 8.2) / 10) + "ma/" + String(round(percentRef * 8.2) / 10) + " out: " + output1 + "%", 2, 2 ,0);
  myGLCD.print("2:" + String(round(percent2 * 8.2) / 10) + "ma/" + String(round(percentRef * 8.2) / 10) + " out: " + output2 + "%", 2, 22 ,0);
  myGLCD.print("3:" + String(round(percent3 * 8.2) / 10) + "ma/" + String(round(percentRef * 8.2) / 10) + " out: " + output3 + "%", 2, 42 ,0);
  myGLCD.print("4:" + String(round(percent4 * 8.2) / 10) + "ma/" + String(round(percentRef * 8.2) / 10) + " out: " + output4 + "%", 2, 62 ,0);
  myGLCD.print("5:" + String(round(percent5 * 8.2) / 10) + "ma/" + String(round(percentRef * 8.2) / 10) + " out: " + output5 + "%", 2, 82 ,0);
  myGLCD.print("6:" + String(round(percent6 * 8.2) / 10) + "ma/" + String(round(percentRef * 8.2) / 10) + " out: " + output6 + "%", 2, 102 ,0);
  myGLCD.print("7:" + String(round(percent7 * 8.2) / 10) + "ma/" + String(round(percentRef * 8.2) / 10) + " out: " + output7 + "%", 2, 122 ,0);
  myGLCD.print("8:" + String(round(percent8 * 8.2) / 10) + "ma/" + String(round(percentRef * 8.2) / 10) + " out: " + output8 + "%", 2, 142 ,0);

  myGLCD.print("Air Temp: " + String(percentAirTemp + 25) + " degC", 2, 172 ,0);
  myGLCD.print("L Reg Temp: " + String(percentRegLTemp + 25) + " degC", 2, 192 ,0);
  myGLCD.print("R Reg Temp: " + String(percentRegRTemp + 25) + " degC", 2, 212 ,0);
}

void printQQE0312SPUD(dataResponse datas) {
  unsigned int percentLeftA1 = datas.measure0 * 100 / 255;
  unsigned int percentLeftA2 = datas.measure1 * 100 / 255;
  unsigned int percentRightA1 = datas.measure2 * 100 / 255;
  unsigned int percentRightA2 = datas.measure3 * 100 / 255;

  unsigned int percentAirTemp = max(min(datas.temperature0 - 25, 100), 0.1);
  unsigned int percentRegLTemp = max(min(datas.temperature1 - 25, 100), 0.1);
  unsigned int percentRegRTemp = max(min(datas.temperature2  - 25, 100), 0.1);

  unsigned int percentRef = datas.refValue * 100 / 290;

  myGLCD.print("L A1: " + String(round(percentLeftA1 * 3.36) / 10) + "ma/" + String(round(percentRef * 3.36) / 10  ), 2, 2 ,0);
  myGLCD.print("L A2: " + String(round(percentLeftA2 * 3.36) / 10) + "ma/" + String(round(percentRef * 3.36) / 10  ), 2, 22 ,0);
  myGLCD.print("R A1: " + String(round(percentRightA1 * 3.36) / 10) + "ma/" + String(round(percentRef * 3.36) / 10  ), 2, 42 ,0);
  myGLCD.print("R A2: " + String(round(percentRightA2 * 3.36) / 10) + "ma/" + String(round(percentRef * 3.36) / 10  ), 2, 62 ,0);

  myGLCD.print("Air Temp: " + String(percentAirTemp + 25) + " degC", 2, 102 ,0);
  myGLCD.print("L Reg Temp: " + String(percentRegLTemp + 25) + " degC", 2, 122 ,0);
  myGLCD.print("R Reg Temp: " + String(percentRegRTemp + 25) + " degC", 2, 142 ,0);
}
































































































































