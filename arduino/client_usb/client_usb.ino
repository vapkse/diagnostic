#include <SoftwareSerial.h>
#include <EasyTransfer.h>
#include <AmpTransfer.h>

const boolean debug = false;

extern void *__brkval;
extern int __bss_end;

const int Rx1 = 2;
const int Tx1 = 5;
const int Rx2 = 3;
const int Tx2 = 6;
const int ledPin = 13;

SoftwareSerial SoftSerial1(Rx1,Tx1);
SoftwareSerial SoftSerial2(Rx2,Tx2);

EasyTransfer readDatas1; 
dataResponse readDatasStruct1;

EasyTransfer readDatas2; 
dataResponse readDatasStruct2;

EasyTransfer sendRequest1;  
ampRequest sendRequestStruct1;

EasyTransfer sendRequest2;  
ampRequest sendRequestStruct2;

EasyTransfer sendDatas;  

unsigned long time;

void processDatas(dataResponse datas)
{
  if (datas.message == MESSAGE_SENDVALUES) {
    if (debug) {
      Serial.println("Datas received for " + String(datas.id));
    }
    sendDatas.begin(details(datas), &Serial);
    sendDatas.sendData();
    sendDatas.close();
    if (debug) {
      Serial.println("");
    }
  }
  else if (debug) { 
    Serial.println("Unknown message received from " + String(datas.id) + " number: " + datas.message);
  }
}

int getFreeMemory()
{
  int free_memory;

  if((int)__brkval == 0)
    free_memory = ((int)&free_memory) - ((int)&__bss_end);
  else
    free_memory = ((int)&free_memory) - ((int)__brkval);

  return free_memory;
}

void setup(){
  // setup the software serial pins
  pinMode(Rx1, INPUT);
  pinMode(Rx2, INPUT);
  pinMode(Tx1, OUTPUT);
  pinMode(Tx2, OUTPUT);

  readDatas1.begin(details(readDatasStruct1), &SoftSerial1);
  readDatas2.begin(details(readDatasStruct2), &SoftSerial2);

  sendRequest1.begin(details(sendRequestStruct1), &SoftSerial1);
  sendRequest2.begin(details(sendRequestStruct2), &SoftSerial2);

  // Open serial communications and wait for port to open:
  Serial.begin(9600);  
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }

  if (debug) {
    Serial.println("Setup done!");
  }
}

void loop () {
  if (getFreeMemory() < 128)
  {
    Serial.println("Not enough memory");
    digitalWrite(ledPin, HIGH);
    delay(5000);
    digitalWrite(ledPin, LOW);
  }

  SoftSerial1.begin(9600); 
  if (SoftSerial1.available()) {    
    if (debug) {
      Serial.println("Serial1 available");
    }
    digitalWrite(ledPin, HIGH);
    // Init timeout
    time = millis();
    while (millis() - time < 1000) 
    {
      if(readDatas1.receiveData()){
        if (debug) {
          Serial.println("Serial1 received");
        }
        processDatas(readDatasStruct1);
        break;
      }
    }
    delay(1);
    digitalWrite(ledPin, LOW);
  }
  SoftSerial1.end(); 

  SoftSerial2.begin(9600); 
  if (SoftSerial2.available()) {
    if (debug) {
      Serial.println("Serial2 available");
    }
    digitalWrite(ledPin, HIGH);
    // Init timeout
    time = millis();
    while (millis() - time < 1000) 
    {
      if(readDatas2.receiveData()){
        if (debug) {
          Serial.println("Serial2 received");
        }
        processDatas(readDatasStruct2);
        break;
      }
    }
    delay(1);
    digitalWrite(ledPin, LOW);
  }
  SoftSerial2.end(); 
}

void sendReset()
{
  SoftSerial1.begin(9600); 
  sendRequestStruct1.message = MESSAGE_RESET;
  sendRequestStruct1.id = MESSAGE_ID_ALL;
  sendRequest1.sendData();
  SoftSerial1.end(); 

  SoftSerial2.begin(9600); 
  sendRequestStruct2.message = MESSAGE_RESET;
  sendRequestStruct2.id = MESSAGE_ID_ALL;
  sendRequest2.sendData();
  SoftSerial2.end(); 
}














