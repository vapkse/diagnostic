#include <AmpTransfer.h>
#include <SimpleTimer.h>
#include <EasyTransfer.h>

// One unique id must be attrubued on each amplifier.
//static const byte QQE0312SPUD_ID = 122;
static const byte QQE0312DPP_ID = 128;
//static const byte QQE0420PPL_ID = 201;
//static const byte QQE0420PPR_ID = 202;
//static const byte G807SE_ID = 207;
//static const byte G12E1_ID = 212;
//static const byte G807SRPPL_ID = 208;
//static const byte G807SRPPR_ID = 209;

static const byte ampId = QQE0312DPP_ID;

unsigned long lastRequest = 0;

SimpleTimer readTimer;
SimpleTimer sendTimer;
SimpleTimer seqTimer;

EasyTransfer requestRx; 
ampRequest requestRxStruct;

EasyTransfer dataTx; 
dataResponse dataTxStruct;

// LED vars 
const int ledPin = 13;

int analog0 = 0;
int analog1 = 0;
int analog2 = 100;
int analog3 = 120;

int stepMaxTime;
int stepElapsedTime;
int stepMaxValue;
int stepCurValue;
unsigned long et;

// Sequence:
#define SEQ_DISCHARGE    0  // 0: Discharge
#define SEQ_HEAT         1  // 1: Heat tempo 
#define SEQ_STARTING     2  // 2: Starting High Voltage
#define SEQ_REGULATING   3  // 3: Waiting for reg
#define SEQ_FUNCTION     4  // 4: Normal Fonction
#define SEQ_FAIL         5  // 5: Fail
int sequence = SEQ_DISCHARGE;
int seqStartTime = 0;
boolean first = true;

byte errorNumber = 0;
byte errorTube = 0; 
unsigned long time = 0;

void setup(){
  Serial.begin(9600);

  requestRx.begin(details(requestRxStruct), &Serial);

  dataTxStruct.id = ampId;
  dataTx.begin(details(dataTxStruct), &Serial);
  readTimer.setInterval(50, readSerial);
  sendTimer.setInterval(200, sendDatas);
  seqTimer.setInterval(50, seqTimerCallback);

  // init LEDS
  pinMode(ledPin,OUTPUT);
  digitalWrite(ledPin,0);
}

void readSerial()
{
  if(requestRx.receiveData()){
    if (requestRxStruct.id == ampId || requestRxStruct.id == MESSAGE_ID_ALL)
    {
      processMessage(requestRxStruct.message);
    }
  }
}

void processMessage(byte message)
{
  if (message == MESSAGE_RESET)
  {
    reset();
  }  
}

void sendDatas()
{
  digitalWrite(ledPin,1);
  // Send datas
  dataTxStruct.message = MESSAGE_SENDVALUES;
  dataTxStruct.step = sequence;
  dataTxStruct.stepMaxTime = stepMaxTime;
  dataTxStruct.stepElapsedTime = stepElapsedTime;
  dataTxStruct.stepMaxValue = stepMaxValue;
  dataTxStruct.stepCurValue = stepCurValue;
  dataTxStruct.tickCount = millis();
  dataTxStruct.measure0 = analog0;
  dataTxStruct.measure1 = analog1;
  dataTxStruct.measure2 = analog2;
  dataTxStruct.measure3 = analog3;
  dataTxStruct.errorNumber = errorNumber;
  dataTxStruct.errorTube = errorTube;
  dataTx.sendData();
  digitalWrite(ledPin,0);
}

void seqTimerCallback()
{
  switch (sequence)
  {  
  case SEQ_STARTING:	
  case SEQ_REGULATING:
    analog0++;
    analog1++;  
    break;

  case SEQ_FUNCTION:
    if (first)
    {
      analog0--;
      analog1--;
    }
    else
    {
      analog0++;
      analog1++;
    }
    break;
  }
}

void reset() // Restarts program from beginning but does not reset the peripherals and registers
{
  asm volatile ("  jmp 0");  
}  

void loop(){   
  readTimer.run();  
  sendTimer.run();
  seqTimer.run();

  switch (sequence)
  {  
  case SEQ_DISCHARGE:	
    // Diagnostic
    stepMaxTime = 10000;
    stepElapsedTime = millis();
    stepMaxValue = 0;
    stepCurValue = 1; // Infinite progress

    if(millis() - seqStartTime < 2000)
    {
      break;
    }

    seqStartTime = millis();
    sequence++;

  case SEQ_HEAT: 
    // Diagnostic
    stepMaxTime = 15000;
    stepElapsedTime = millis() - seqStartTime;
    stepMaxValue = stepMaxTime;
    stepCurValue = stepElapsedTime;
    if(millis() - seqStartTime < stepMaxTime)
    {
      break;
    }

    delay(2500);  
    seqStartTime = millis();
    sequence++;

  case SEQ_STARTING:
    // Diagnostic
    stepMaxTime = 10000;
    stepElapsedTime = millis() - seqStartTime;
    stepMaxValue = 200;
    stepCurValue = min(analog0, analog1);

    if(analog0 < stepMaxValue)
    {
      break;
    }

    seqStartTime = millis();
    sequence++;

  case SEQ_REGULATING:   
    // Diagnostic
    stepMaxTime = 30000;
    stepElapsedTime = millis() - seqStartTime;
    stepMaxValue = 530 - 200;
    stepCurValue = min(analog0, analog1) - 200;

    if(analog0 < 530)
    {
      break;
    }

    seqStartTime = millis();
    sequence++;

  case SEQ_FUNCTION:
    // Diagnostic
    stepMaxTime = 0;
    stepElapsedTime = 0;
    stepMaxValue = 0;
    stepCurValue = 0;

    if (first)
    {
      if(analog0 < 450)
      {
        first = false;
        seqStartTime = millis();
        sequence--;
      }
    }
    else
    {
      if(analog0 > 1000)
      {
        seqStartTime = millis();
        sequence++;
        errorNumber = 3;
        errorTube = 1;
      }
    }
    break;

  default: 
    // Diagnostic
    stepMaxTime = 0;
    stepElapsedTime = 0;
    stepMaxValue = 0;
    stepCurValue = 0;
  }  
}













