#include <SimpleTimer.h>
#include <EasyTransfer.h>
#include <AmpTransfer.h>

// One unique id must be attrubued on each amplifier.
static const byte QQE0312SPUD_ID = 122;
//static const byte QQE0312DPP_ID = 128;
//static const byte QQE0420PPL_ID = 201;
//static const byte QQE0420PPR_ID = 202;
//static const byte G807SE_ID = 207;
//static const byte G12E1_ID = 212;
//static const byte G807SRPPL_ID = 208;
//static const byte G807SRPPR_ID = 209;

static const byte ampId = QQE0312SPUD_ID;

unsigned long lastRequest = 0;
unsigned long lastPing = 0;

SimpleTimer readTimer;
SimpleTimer sendTimer;

EasyTransfer requestRx; 
ampRequest requestRxStruct;

EasyTransfer dataTx; 
dataResponse dataTxStruct;

// LED vars 
const int ledPin = 13;

const int analogInPin0 = A0;
const int analogInPin1 = A1;
const int analogInPin2 = A2;
const int analogInPin3 = A3;
byte currentStep = 2;

void setup(){
  Serial.begin(9600);

  requestRx.begin(details(requestRxStruct), &Serial);

  readTimer.setInterval(50, readSerial);
  sendTimer.setInterval(200, sendDatas);

  dataTxStruct.id = ampId;
  dataTx.begin(details(dataTxStruct), &Serial);

  // init LEDS
  pinMode(ledPin,OUTPUT);
  digitalWrite(ledPin,0);
}

void readSerial()
{
  if(requestRx.receiveData()){
    if (requestRxStruct.id == ampId)
    {
      // Process message
    }
  }
}

void sendDatas()
{
  // Send datas
  dataTxStruct.message = MESSAGE_SENDVALUES;
  dataTxStruct.step = currentStep;
  // dataTxStruct.stepMaxTime = stepMaxTime;
  // dataTxStruct.stepElapsedTime = stepElapsedTime;
  // dataTxStruct.stepTarget = stepTarget;
  dataTxStruct.tickCount = millis();
  dataTxStruct.measure0 = analogRead(analogInPin0);
  dataTxStruct.measure1 = analogRead(analogInPin1);
  dataTxStruct.measure2 = analogRead(analogInPin2);
  dataTxStruct.measure3 = analogRead(analogInPin3);
  //dataTxStruct.errorNumber = errorNumber;
  //dataTxStruct.errorTube = errorTube;
  dataTx.sendData();
}

void loop(){
  readTimer.run();
  sendTimer.run();
}



