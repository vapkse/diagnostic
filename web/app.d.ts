/// <reference path="typings/body-parser/body-parser.d.ts" />
/// <reference path="typings/cookie-parser/cookie-parser.d.ts" />
/// <reference path="typings/debug/debug.d.ts" />
/// <reference path="typings/express/express.d.ts" />
/// <reference path="typings/node/node.d.ts" />
/// <reference path="typings/serve-favicon/serve-favicon.d.ts" />
import express = require('express');
declare var app: express.Express;
export = app;
