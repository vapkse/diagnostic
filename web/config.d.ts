/// <reference path="typings/node/node.d.ts" />
declare var config: {
    listenPort: number;
    serverCom: string;
    serverAddress: string;
    serverPort: number;
    debugPort: number;
    livereload: number;
    path: {
        base: string;
        data: any;
        views: any;
        app: any;
        server: string;
        test: any;
        temp: any;
        json: any;
        todo: any;
    };
};
export = config;
