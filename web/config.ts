/// <reference path="./typings/node/node.d.ts" />

var path = require('path');
var base = __dirname;
var config = {
    listenPort : 8000,
    serverCom : 'com4',
    serverAddress : '', //'192.168.1.80',    
    serverPort : 8080,
    debugPort: 5860,
    livereload: 38080,
    path: {
        base: base,
        data: path.join(base, 'datas'),
        views: path.join(base, 'app'),
        app: path.join(base, 'app'),
        server: base,
        test: path.join(base, 'test'),
        temp: path.join(base, '.tmp'),
        json: path.join(base, 'json'),
        todo: path.join(base, 'todo')
    },
};

export = config; 