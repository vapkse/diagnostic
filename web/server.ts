'use strict';

import fs = require('fs');
import path = require('path');
import serialPort = require('serialport');
import socketio = require('socket.io');
import net = require('net');
import http = require('http');
import underscore = require('underscore');
import config = require('./config');
import log4js = require('log4js');
import todo = require('./todo/todo');
var struct = require('./node_modules/struct/index.js');

const requestTimeout = 1500;

export class Request {
    host: string;
    port: number;
    path: string;
    method: string;
    headers: {
        [index: string]: any
    }
}

export class RequestHistory {
    time: number;
    options = new Request();
}

export interface IClients {
    [index: string]: Client
}

export interface IJson {
    [index: string]: any,
}

export class Client {
    socket: SocketIO.Socket;

    constructor(socket: SocketIO.Socket) {
        var self = this as Client;
        self.socket = socket;
    }

    _watchId: number = 0;
    _watchStatus = false;
    nextDataRequest = 0;
    nextStatusRequest = 0;
    slowRequestTime = 0;
    watchId = function(value?: number): number {
        var self = this as Client;
        if (value === undefined) {
            return Date.now() > self.nextDataRequest ? self._watchId : 0;
        }
        else {
            self._watchId = value;
            self.nextDataRequest = 0;
        }
    };
    watchStatus = function(value?: boolean): boolean {
         var self = this as Client;
        if (value === undefined) {
            return self._watchStatus && Date.now() > self.nextStatusRequest;
        }
        else {
            self._watchStatus = value;
            self.nextStatusRequest = 0;
        }
    };
    updateHistory = function(id: string, files: Array<string>) {
        var self = this as Client;
        var hresult: SocketIO.IProxyRequestResult = {
            request: 'amphistory',
            status: 'ok',
            mode: '',
            args: {
                id: id
            }
        }

        hresult.mode = 'replace';
        hresult.data = {
            list: files
        };

        self.socket.emit('response', {
            result: hresult
        });
    };
    updateStatus = function(ampStatus: IAmpStatus) {
        var self = this as Client;
        var sresult = {
            request: 'ampstatus',
            mode: 'replace',
            status: 'ok',
            data: ampStatus
        };
        self.socket.emit('response', {
            result: sresult
        });
    };
    updateAmpDatas = function(data: IAmpdata) {
        var self = this as Client;
        var time = Date.now();
        self.nextDataRequest = 0;
        if (data.steptmax === 0) {
            // Stabilized
            if (self.slowRequestTime === 0) {
                self.slowRequestTime = time;
            }
            else if (time - self.slowRequestTime > 30000) {
                self.nextDataRequest = time + 4500;
            }
        }
        else {
            self.slowRequestTime = 0;
        }

        var uresult = {
            data: data,
            request: 'ampdata',
            mode: 'replace',
            status: 'ok',
            args: {
                id: data.id,
            }
        };
        self.socket.emit('response', {
            result: uresult
        });
    }
}

export class HistoryFile {
    date: string;
    index: number;
    tick: number;
    path: string;

    constructor(path: string, filename?: string) {
        var self = this as HistoryFile;
        self.path = path;
        if (filename) {
            var args = filename.split('_');
            self.date = args[0];
            self.index = parseInt(args[1]);
            self.tick = parseInt(args[2]);
        } else {
            self.date = (new Date()).toLocaleDateString().replace(/-/g, "");
            self.index = 0;
            self.tick = 0;
        }
    }

    isNew = function() {
        var self = this as HistoryFile;
        return self.tick === 0;
    }

    filename = function() {
        var self = this as HistoryFile;
        return self.path + String(self.date) + '_' + String(self.index) + '_' + String(self.tick);
    }
}

export interface IHistoryFiles {
    [index: string]: Array<string>
}

export interface IBufferEntry {
    id: number,
    tick: number,
    data: string
}

export interface ILastHistoryEntry {
    tick: number,
    data: IAmpdata
}

export class History {
    bufferMaxSize = 20;
    autoSaveTimeout = 120000;

    logpath: string;
    buffer: {
        [index: number]: Array<IBufferEntry>,
    }
    currentFiles: {
        [index: number]: HistoryFile
    };
    lastHistory: {
        [index: number]: ILastHistoryEntry
    }
    autoStoreTimers: {
        [index: number]: any
    }

    constructor(logpath: string) {
        var self = this as History;
        if (fs.existsSync(logpath)) {
            self.logpath = logpath;
        }
        self.buffer = {};
        self.currentFiles = {};
        self.lastHistory = {};
        self.autoStoreTimers = {};
    }

    // Return if datas has changed from last time
    addHistory = function(id: number, tick: number, data: string): boolean {
        var self: History = this;

        var compareDatas = function(d1: any, d2: any): number {
            var errorCount = 0;
            for (var name in d1) {
                if (typeof d1[name] === 'object' || d1[name] instanceof Array) {
                    errorCount += compareDatas(d1[name], d2[name]);
                }
                else if (d1[name] !== d2[name]) {
                    errorCount++;
                }
            }

            return errorCount;
        }

        var checkChangesInData = function(entry: IBufferEntry) {
            var previous: ILastHistoryEntry = self.lastHistory[entry.id];
            var d: IAmpdata = JSON.parse(data);
            var result = true;
            if (previous) {
                if (entry.tick - previous.tick < 10000) {
                    result = compareDatas(d, previous.data) > 2; // Thick and stepelaps can change    
                }
            }
            self.lastHistory[entry.id] = {
                tick: tick,
                data: d
            };
            return result;
        }

        if (!self.logpath) {
            return;
        }

        if (!self.buffer[id]) {
            self.buffer[id] = [];
        }

        var entry: IBufferEntry = {
            id: id,
            tick: tick,
            data: data
        }

        // If new session, store last session
        var lasttick = self.buffer[id].length > 1 && self.buffer[id][self.buffer[id].length - 1].tick || 0;
        if (lasttick > 0 && lasttick > tick) {
            self.store(id, lasttick);
            delete self.currentFiles[id];
            delete self.lastHistory[id];
        }

        if (!checkChangesInData(entry)) {
            return false;
        }

        self.buffer[id].push(entry);

        // Auto store last datas in case of the amp take off        
        if (self.autoStoreTimers[id]) {
            clearTimeout(self.autoStoreTimers[id]);
        }
        self.autoStoreTimers[id] = setTimeout(function(args: Array<number>) {
            self.store(args[0], args[1]);
        }, self.autoSaveTimeout, [id, tick])

        // If enough datas, store current session
        if (self.buffer[id].length > self.bufferMaxSize) {
            self.store(id, tick);
        }

        return true;
    }

    getNextFile = function(id: number, tick: number): HistoryFile {
        var self: History = this;
        var hfile: HistoryFile;

        // Check for folder
        var dir = path.join(self.logpath, '/' + String(id) + '/');
        if (!fs.existsSync(dir)) {
            fs.mkdirSync(dir);
        }

        // Read files
        var files = fs.readdirSync(dir);

        // Sort by numeric name 
        files.sort(function(a, b) {
            return parseInt(a) - parseInt(b);
        });

        var lastFile = files.length && files[files.length - 1];
        if (lastFile) {
            hfile = new HistoryFile(dir, lastFile);

            var filedate = (new Date()).toLocaleDateString().replace(/-/g, "");

            if (hfile.date === filedate) {
                if (hfile.tick > tick) {
                    hfile.index = hfile.index + 1;
                    hfile.tick = 0;
                }
            } else {
                hfile.date = filedate;
                hfile.index = 0;
                hfile.tick = 0;
            }
        } else {
            hfile = new HistoryFile(dir);
        }

        return hfile;
    }

    store = function(id: number, lasttick: number) {
        var self: History = this;
        var hfile: HistoryFile = self.currentFiles[id];
        if (!hfile) {
            hfile = self.getNextFile(id, lasttick);
            self.currentFiles[id] = hfile;
        }

        var filename = hfile.filename();
        var entries = self.buffer[id];
        var isNew = hfile.isNew();
        var data = '';
        for (var i = 0; i < entries.length; i++) {
            if (isNew) {
                isNew = false;
            } else {
                data += ',';
            }
            data += entries[i].data;
        }
        fs.appendFileSync(filename, data);

        // Success, clear buffer            
        self.buffer[id] = [];
        if (self.autoStoreTimers[id]) {
            clearTimeout(self.autoStoreTimers[id]);
            delete self.autoStoreTimers[id];
        }

        hfile.tick = lasttick;
        fs.renameSync(filename, hfile.filename());
    }
}

export class server {
    logger: log4js.Logger;
    lastRequest: RequestHistory;
    receivedBody: string;
    clients: IClients;
    history: History;

    constructor(httpServer: http.Server, loggr: log4js.Logger) {
        var self: server = this;
        self.clients = {};
        self.logger = loggr;
        self.initSocketIO(httpServer);
        self.lastRequest = new RequestHistory();
        self.lastRequest.time = Date.now();
        self.history = new History(config.path.data);
        setInterval(function() {
            if (self.lastRequest.time === 0 || Date.now() - self.lastRequest.time < 200) {
                return;
            }

            try {
                self.lastRequest.time = 0;

                // Check if http server is configured
                if (config.serverAddress) {
                    self.requestHTTP();
                }
            }
            catch (e) {
                self.logger.error("Server request error: " + e.message);
                self.lastRequest.time = Date.now();
            }
        }, 50);


        // Check if USB server is configured
        if (config.serverCom) {
            var dataResponse = struct()
                .word8('id')
                .word8('msg')
                .word8('step')
                .word16Ule('steptmax')
                .word16Ule('steptelaps')
                .word16Ule('stepvmax')
                .word16Ule('stepval')
                .word32Ule('tick')
                .word8('errn')
                .word8('errt')
                .word8('min')
                .word8('ref')
                .word8('max')
                .array('out', 8, 'word8')
                .array('val', 8, 'word8')
                .array('temp', 4, 'word8')

            dataResponse.allocate();

            var dataBuffer = dataResponse.buffer();

            var bufferPos = 0;
            var calculatedChecksum = dataBuffer.length;

            var SerialPort = serialPort.SerialPort;
            var serialOptions: serialPort.SerialOptions = {
                baudrate: 9600
            }
            var serport: serialPort.SerialPort;
            var lastDataTime: number;

            var openPort = function() {
                lastDataTime = (new Date()).getTime();
                serport = new SerialPort(config.serverCom, serialOptions);

                serport.on("open", function() {
                    self.logger.info('Port ' + config.serverCom + ' open.');

                    serport.on('data', function(data: Array<number>) {
                        if (data.length < 3) {
                            return;
                        }
                        
                        for (var i = 0; i < data.length; i++) {
                            if (bufferPos === 0) {
                                // Wait for EasyTransfer header
                                if (i + 3 < data.length && data[i] === 6 && data[i + 1] === 133 && data[i + 2] === dataBuffer.length) {
                                    i += 3;
                                    dataBuffer[bufferPos] = data[i];
                                    calculatedChecksum ^= data[i];
                                    bufferPos++;
                                }
                            } else {
                                // Continue transfer to buffer
                                dataBuffer[bufferPos] = data[i];
                                calculatedChecksum ^= data[i];
                                bufferPos++;

                                // Check if buffer is full
                                if (bufferPos >= dataBuffer.length) {
                                    // Read and check checksum
                                    i++;
                                    if (i < data.length) {
                                        var checksum = data[i];
                                        if (checksum === calculatedChecksum) {
                                            // Valid datas
                                            var fields = dataResponse.fields;
                                            var json: IJson = {
                                                status: [fields.id]
                                            }
                                            json['datas_' + fields.id] = fields;
                                            self.processJson(json);
                                        } else {
                                            self.logger.debug("Datas from Arduino ignored, invalid checksum.");
                                        }
                                    }
                                    // Reset for the next stream
                                    calculatedChecksum = dataBuffer.length;
                                    bufferPos = 0;
                                }
                            }
                        }
                    });

                    /*serialPort.write(new Buffer('4', 'ascii'), function(err, results) {
                        console.log('err ' + err);
                        console.log('results ' + results);
                    });*/
                });
            }

            setInterval(function() {
                if ((new Date()).getTime() - lastDataTime > 80000) {
                    if (serport) {
                        serport.close(function() {
                            openPort();
                        });
                    } else {
                        openPort();                        
                    }
                }
            }, 10000);

            openPort();
        }

        // Load todo processor for archives
        // new todo.todo();
    }

    processJson = function(json: any) {
        var self: server = this;
        for (var name in json) {
            if (name.indexOf('datas_') === 0) {
                var j: IAmpdata = json[name];
                var s = JSON.stringify(j);
                if (!self.history.addHistory(j.id, j.tick, s)) {
                    self.logger.debug('Datas received, but still same as before.');
                }
                else {
                    self.logger.debug('Processing: ' + s);
                }
            }
        }

        var time = Date.now();
        for (var sockeId in self.clients) {
            var client = self.clients[sockeId];
            var watchId = client.watchId();
            var data = watchId && json['datas_' + watchId];

            if (data) {
                client.updateAmpDatas(data);
            }

            if (json.status) {
                // If json.status is present, or the status was requested, so why not send
                client.nextStatusRequest = json.status.length > 0 ? time + 5000 : time + 1000;
                if (client._watchStatus) {
                    client.updateStatus(json.status);
                }
            }
        }
    }

    requestHTTP = function() {
        var self: server = this;
        var id: Array<number> = [];
        var status = false;
        var reset = false;

        for (var sockeId in self.clients) {
            var client = self.clients[sockeId];
            var watchId = client.watchId();
            if (watchId) {
                id.push(watchId);
                status = true;
            }
            else if (client.watchStatus()) {
                status = true;
            }
        }

        // Construct url                
        var options = new Request();
        options.host = config.serverAddress;
        options.port = config.serverPort;
        options.path = '';
        options.method = 'GET';
        options.headers = {
            "Content-Length": 0,
            "Connection": "close"
        };

        if (id.length) {
            options.path = '/datas?';
            for (var i = 0; i < id.length; i++) {
                if (i > 0) {
                    options.path += '&';
                }
                options.path += 'id=' + id[i];
            }
        }

        if (status) {
            options.path += (options.path ? '&' : '/') + 'status';
        }

        if (reset) {
            options.path += (options.path ? '&' : '/') + 'reset';
        }

        if (!options.path) {
            // No client request something.
            self.lastRequest.time = Date.now();
            return;
        }

        self.lastRequest.options = options;
        var req = http.request(options, function(res: http.ClientResponse) {
            self.logger.debug('Server request STATUS: ' + res.statusCode);
            self.logger.debug('Server request HEADERS: ' + JSON.stringify(res.headers));
            self.receivedBody = '';
            res.on('data', function(chunk: string) {
                self.receivedBody += chunk;
            });
            res.on('end', function() {
                // Test json string
                if (/^[\],:{}\s]*$/.test(self.receivedBody.replace(/\\["\\\/bfnrtu]/g, '@').replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, ']').replace(/(?:^|:|,)(?:\s*\[)+/g, ''))) {
                    //the json is ok
                    try {
                        var json = JSON.parse(self.receivedBody);
                        self.processJson(json);
                    }
                    catch (e) {
                        self.logger.error("Server request error: " + e.message);
                    }
                }
                else {
                    //the json is not ok, ignore
                }
                self.lastRequest.time = Date.now();
            });
        });

        req.on('error', function(e: any) {
            self.logger.error("Server request error: " + e.message);
            req.abort();
            self.lastRequest.time = Date.now();
        });

        req.setTimeout(requestTimeout, function() {
            req.abort();
            self.logger.warn('Server request time out');
            self.lastRequest.time = Date.now();
        });

        req.write('');
        req.end();
    }

    initSocketIO = function(httpServer: http.Server) {
        var self: server = this;
        var io = socketio.listen(httpServer);

        io.sockets.on('connection', function(socket) {
            var client = new Client(socket)

            self.logger.info(socket.id + ' client connected');

            socket.on('disconnect', function() {
                delete self.clients[socket.id];
                self.logger.info(socket.id + " client disconnected");
            });

            socket.on('request', function(params: SocketIO.IProxyRequestParams, callback: Function) {
                var result: SocketIO.IProxyRequestResult;

                if (params) {
                    result = {
                        method: params.method,
                        cachekey: params.cachekey,
                        request: params.request,
                        args: params.args,
                        error: undefined,
                        status: undefined,
                        data: {}
                    }
                } else {
                    result = {
                        cachekey: undefined,
                        error: 'invalid request parameters.',
                        status: 'fail',
                        data: {}
                    };
                }

                if (result.error) {
                    // Error
                    self.logger.error(socket.id + ' request error: ' + result.error);
                    if (callback) {
                        callback(result);
                    }
                    return;
                }

                self.logger.info(socket.id + ' request ' + params.request);
                try {
                    switch (params.request) {
                        case 'getjson':
                            var fileName = './json/' + params.args['filename'] + '.json';
                            fs.readFile(path.join(__dirname, fileName), 'utf8', function(err, data) {
                                if (err) {
                                    console.error("socket.io request: " + err);
                                    result.error = err.toString();
                                    result.status = 'fail';
                                }
                                else {
                                    result.data = JSON.parse(data);
                                    result.status = 'ok';
                                }
                                sendResponse(result);
                            });
                            return;

                        case 'ampstatus':
                            client.watchStatus(true);
                            return;

                        case 'ampdata':
                            client.watchId(params.args['id']);
                            return;

                        case 'amphistory':
                            var id = params.args['id'];
                            var dir = path.join(config.path.data, '/' + String(id) + '/');
                            if (fs.existsSync(dir)) {
                                fs.readdir(dir, function(err, files) {
                                    if (!err) {
                                        client.updateHistory(params.args['id'], files);
                                    }
                                    else {
                                        throw err;
                                    }
                                });
                            }
                            return;

                        case 'getamphistdata':
                            var id2 = params.args['id'];
                            var filename = params.args['hist'];
                            var hfile = new HistoryFile(dir, filename);
                            var dir2 = path.join(config.path.data, '/' + String(id2) + '/');

                            var readFile = function(filename: string) {
                                // Get the file content
                                fs.readFile(filename, 'utf8', function(err: NodeJS.ErrnoException, data: string) {
                                    if (err) {
                                        console.error("socket.io request: " + err);
                                        result.error = err.toString();
                                        result.status = 'fail';
                                    }
                                    else {
                                        try {
                                            result.data.list = JSON.parse('[' + data + ']');
                                            result.status = 'ok';
                                        }
                                        catch (e) {
                                            console.error("socket.io request: " + e.message || e);
                                            result.error = e.message;
                                            result.status = 'fail';
                                        }
                                    }
                                    sendResponse(result);
                                });
                            }

                            // Search file ignoring tick
                            if (fs.existsSync(dir2)) {
                                fs.readdir(dir2, function(err, files) {
                                    if (!err) {
                                        for (var i = 0; i < files.length; i++) {
                                            var hf = new HistoryFile(dir2, files[i]);
                                            if (hf.date === hfile.date && hf.index === hfile.index) {
                                                readFile(hf.filename());
                                                return;
                                            }
                                        }

                                        var error = "file not found " + filename;
                                        console.error("socket.io request: " + error);
                                        result.error = error;
                                        result.status = 'fail';
                                    }
                                    else {
                                        console.error("socket.io request: " + err);
                                        result.error = err.toString();
                                        result.status = 'fail';
                                    }
                                    sendResponse(result);
                                });
                            }

                            return;

                        default:
                            throw "Not implemented request " + params.request;

                    }
                }
                catch (e) {
                    console.error("socket.io request: " + e.message || e);
                    result.error = e.message;
                    result.status = 'fail';
                }

                function sendResponse(result: SocketIO.IProxyRequestResult) {
                    if (!result.method || result.method === 'post' || !callback) {
                        socket.emit('response', {
                            result: result
                        });
                    }
                    else {
                        callback(result);
                    }
                }

                sendResponse(result);
            });

            self.clients[socket.id] = client;
        });
    }

    getCurrentHistoryFileIndex = function(histFiles: Array<string>, ampid: number) {
        var fileIndex = -1;
        if (histFiles && histFiles.length) {
            for (var i = 0; i < histFiles.length; i++) {
                var info = histFiles[i].split('_');
                if (info.length === 2) {
                    var id = parseInt(info[0]);
                    var index = parseInt(info[1]);
                    if (ampid === id && index > fileIndex) {
                        fileIndex = index;
                    }
                }
            }
        }

        return fileIndex >= 0 ? fileIndex : null;
    }
}

