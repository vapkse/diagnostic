/// <reference path="../node/node.d.ts" />

declare module "serialport" {
  export interface ParserFunc {
    (emitter: any, buffer: NodeBuffer): void;
  }

  interface Parsers {
    raw: ParserFunc;
    readline: (delimiter?: string) => ParserFunc;
  }

  export interface SerialOptions {
    baudrate?: number;
    databits?: number;
    stopbits?: number;
    parity?: string;
    buffersize?: number;
    parser?: ParserFunc;
  }

  export var parsers: Parsers;
  export class SerialPort {
    constructor(port: string, options: SerialOptions);
    on(event: string, listener: Function): void;
    close(callback?: ()=>void ): void;
  }
}