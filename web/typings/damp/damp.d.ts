interface ITubeinfo {
	name: string
}

interface IAmperror {
	id: number,
	descr: string
}

interface IAmpinfo{
	name: string,
	id: number,
	description: string,
	dampingfactor: number,
	power: string,
	bandwidth: string,
	amplificationfactor: number,
	inverter: boolean,
	tubes: Array<ITubeinfo>,
	errors: Array<IAmperror>,
	url: string,
	templateUrl: string,
	controllerUrl: string,
	controller: string	
}

interface IAmpStatus{
	
}

interface IAmpdata {
	id: number,
	step: number,
	steptmax: number,
	steptelaps: number,
	stepvmax: number,
	stepval: number,
	tick: number,
	errn: number,
	errt: number,
	ref: number,
	min: number,
	max: number,
	val: Array<number>,
	out: Array<number>,
	temp: Array<number>,
	fromMeasure: boolean,
}