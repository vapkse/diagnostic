declare module SocketIO {
	export interface IProxyRequestParams {
		method?: string,
		cachekey?: string,
		request?: string,
		args?: {[index: string]: any,},
	}

	export interface IProxyRequestResultDatas {
		list?: Array<any>;
	}

	export interface IProxyRequestResult extends IProxyRequestParams {
		mode?: string,
		error?: string,
		status?: string,
		data?: IProxyRequestResultDatas
	}

	export interface IProxyRequestResponse {
		result: IProxyRequestResult
	}
}
