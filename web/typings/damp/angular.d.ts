declare module angular {
	interface IAngularStatic {
		resumeBootstrap(extraModules: Array<string>): void
	}
	
	interface IRootScopeService {
		appInfos: any;
	}

	interface IFilterService {
		(name: 'find'): IFilterFind;
		<T>(name: string): T;
	}
	
	interface IFilterFind {
		/**
			* Converts string to uppercase.
			*/
		<T>(input: T[], expression: string|string[]|((value: T) => any)|((value: T) => any)[]) : T
	}
}

declare module WindowSessionStorage {
	interface Storage {
		lang: string;
	}	
}
