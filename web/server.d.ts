import http = require('http');
import log4js = require('log4js');
export declare class Request {
    host: string;
    port: number;
    path: string;
    method: string;
    headers: {
        [index: string]: any;
    };
}
export declare class RequestHistory {
    time: number;
    options: Request;
}
export interface IClients {
    [index: string]: Client;
}
export interface IJson {
    [index: string]: any;
}
export declare class Client {
    socket: SocketIO.Socket;
    constructor(socket: SocketIO.Socket);
    _watchId: number;
    _watchStatus: boolean;
    nextDataRequest: number;
    nextStatusRequest: number;
    slowRequestTime: number;
    watchId: (value?: number) => number;
    watchStatus: (value?: boolean) => boolean;
    updateHistory: (id: string, files: string[]) => void;
    updateStatus: (ampStatus: IAmpStatus) => void;
    updateAmpDatas: (data: IAmpdata) => void;
}
export declare class HistoryFile {
    date: string;
    index: number;
    tick: number;
    path: string;
    constructor(path: string, filename?: string);
    isNew: () => boolean;
    filename: () => string;
}
export interface IHistoryFiles {
    [index: string]: Array<string>;
}
export interface IBufferEntry {
    id: number;
    tick: number;
    data: string;
}
export interface ILastHistoryEntry {
    tick: number;
    data: IAmpdata;
}
export declare class History {
    bufferMaxSize: number;
    autoSaveTimeout: number;
    logpath: string;
    buffer: {
        [index: number]: Array<IBufferEntry>;
    };
    currentFiles: {
        [index: number]: HistoryFile;
    };
    lastHistory: {
        [index: number]: ILastHistoryEntry;
    };
    autoStoreTimers: {
        [index: number]: any;
    };
    constructor(logpath: string);
    addHistory: (id: number, tick: number, data: string) => boolean;
    getNextFile: (id: number, tick: number) => HistoryFile;
    store: (id: number, lasttick: number) => void;
}
export declare class server {
    logger: log4js.Logger;
    lastRequest: RequestHistory;
    receivedBody: string;
    clients: IClients;
    history: History;
    constructor(httpServer: http.Server, loggr: log4js.Logger);
    processJson: (json: any) => void;
    requestHTTP: () => void;
    initSocketIO: (httpServer: http.Server) => void;
    getCurrentHistoryFileIndex: (histFiles: string[], ampid: number) => number;
}
