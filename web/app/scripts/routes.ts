/// <reference path="../../typings/angularjs/angular.d.ts" />

module dampApp{
    'use strict';

    define(['app'], function(app: Routage.IModule)
    {
        return app.config(['$routeProvider', 'routeResolverProvider', '$controllerProvider', '$compileProvider', '$filterProvider', '$provide', '$httpProvider', '$locationProvider', 'config',
        function($routeProvider: ng.route.IRouteProvider, routeResolverProvider: any, $controllerProvider: ng.IControllerProvider, $compileProvider: ng.ICompileProvider, $filterProvider: ng.IFilterProvider, $provide: ng.IModule, $httpProvider: ng.IHttpProvider, $locationProvider: ng.ILocationProvider, config: IConfig)
        {
            $locationProvider.html5Mode(true).hashPrefix('!');
    
            app.register = {
                controller : $controllerProvider.register,
                directive : $compileProvider.directive,
                filter : $filterProvider.register,
                factory : $provide.factory,
                service : $provide.service,
                provider : $provide.provider,
                constant : $provide.constant,
            };
    
            // route.resolve(page, controller, [dependencies], [css])
            $routeProvider.when(config.page.home, routeResolverProvider.resolve('homeTemplate', 'homeController', ['homeController'], ['uikitStyle']));
    
            $routeProvider.when(config.page.logs, routeResolverProvider.resolve('logsTemplate', 'logsController', ['logsController'], ['uikitStyle']));
        
            $routeProvider.otherwise({
                redirectTo : config.page.home
            });
        }]);
    });
}

