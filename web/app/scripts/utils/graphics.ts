'use strict';

import g = graphics;
module graphics {
	export class Position {
        x = 0;
        y = 0;
        constructor(x?: number, y?: number) {
            this.x = x;
            this.y = y;
        }        
    }

    export class ReversiblePosition extends Position{
        [index: string]: number;        
    }
 
    export class Size {
        width = 0;
        height = 0;
        constructor(width?: number, height?: number) {
            this.width = width;
            this.height = height;
        }        
    }   

    export class ReversibleSize extends Size{
        [index: string]: number;        
    }

    export class Rect {
        x = 0;
        y = 0;
        width = 0;
        height = 0;
        constructor(x?: number, y?: number, width?: number, height?: number) {
            this.x = x;
            this.y = y;
            this.width = width;
            this.height = height;
        }        
    }
}