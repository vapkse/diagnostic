'use strict';
var vapkse;
(function (vapkse) {
    var DropDownItem = (function () {
        function DropDownItem() {
            this.textAttribute = '_text';
            this.valueAttribute = '_value';
            this.visible = true;
            this.text = function (value) {
                if (value !== undefined) {
                    this[this.textAttribute] = value;
                }
                else {
                    return this[this.textAttribute];
                }
            };
            this.value = function (value) {
                if (value !== undefined) {
                    this[this.valueAttribute] = value;
                }
                else {
                    return this[this.valueAttribute];
                }
            };
            this.tostring = function () {
                var formated = [];
                var text = this.text();
                var hasSpace = text.indexOf(' ') > 0;
                if (hasSpace) {
                    formated.push('"');
                }
                formated.push(text);
                if (hasSpace) {
                    formated.push('"');
                }
                return formated.join('');
            };
        }
        return DropDownItem;
    }());
    vapkse.DropDownItem = DropDownItem;
})(vapkse || (vapkse = {}));
