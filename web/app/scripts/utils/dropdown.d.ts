declare module vapkse {
    class DropDownItem {
        textAttribute: string;
        valueAttribute: string;
        visible: boolean;
        text: (value?: string) => any;
        value: (value?: string) => any;
        tostring: () => string;
    }
    interface IDropDownFilter {
        text: string;
        filter: JQuery;
        clear(): void;
    }
}
