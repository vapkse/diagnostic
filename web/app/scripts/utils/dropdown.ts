'use strict';

module vapkse {
    export class DropDownItem {
        textAttribute = '_text';
        valueAttribute = '_value';
        visible = true;
        text = function(value?: string) {
            if (value !== undefined) {
                this[this.textAttribute] = value;
            }
            else {
                return this[this.textAttribute];
            }
        };
        value = function(value?: string) {
            if (value !== undefined) {
                this[this.valueAttribute] = value;
            }
            else {
                return this[this.valueAttribute];
            }
        };
        tostring = function() {
            var formated: Array<string> = [];
            var text = this.text();
            var hasSpace = text.indexOf(' ') > 0;
            if (hasSpace) {
                formated.push('"');
            }
            formated.push(text);
            if (hasSpace) {
                formated.push('"');
            }
            return formated.join('');
        };
    }
    
    export interface IDropDownFilter{
        text: string,
        filter: JQuery,
        clear(): void
    }
}