import g = graphics;
declare module graphics {
    class Position {
        x: number;
        y: number;
        constructor(x?: number, y?: number);
    }
    class ReversiblePosition extends Position {
        [index: string]: number;
    }
    class Size {
        width: number;
        height: number;
        constructor(width?: number, height?: number);
    }
    class ReversibleSize extends Size {
        [index: string]: number;
    }
    class Rect {
        x: number;
        y: number;
        width: number;
        height: number;
        constructor(x?: number, y?: number, width?: number, height?: number);
    }
}
