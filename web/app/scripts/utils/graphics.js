'use strict';
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var g = graphics;
var graphics;
(function (graphics) {
    var Position = (function () {
        function Position(x, y) {
            this.x = 0;
            this.y = 0;
            this.x = x;
            this.y = y;
        }
        return Position;
    }());
    graphics.Position = Position;
    var ReversiblePosition = (function (_super) {
        __extends(ReversiblePosition, _super);
        function ReversiblePosition() {
            _super.apply(this, arguments);
        }
        return ReversiblePosition;
    }(Position));
    graphics.ReversiblePosition = ReversiblePosition;
    var Size = (function () {
        function Size(width, height) {
            this.width = 0;
            this.height = 0;
            this.width = width;
            this.height = height;
        }
        return Size;
    }());
    graphics.Size = Size;
    var ReversibleSize = (function (_super) {
        __extends(ReversibleSize, _super);
        function ReversibleSize() {
            _super.apply(this, arguments);
        }
        return ReversibleSize;
    }(Size));
    graphics.ReversibleSize = ReversibleSize;
    var Rect = (function () {
        function Rect(x, y, width, height) {
            this.x = 0;
            this.y = 0;
            this.width = 0;
            this.height = 0;
            this.x = x;
            this.y = y;
            this.width = width;
            this.height = height;
        }
        return Rect;
    }());
    graphics.Rect = Rect;
})(graphics || (graphics = {}));
