/// <reference path="../../typings/angularjs/angular.d.ts" />

module dampApp {
    'use strict';

    var loadJSON = function(path: string, success: Function, error?: Function) {
        var xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function() {
            if (xhr.readyState === XMLHttpRequest.DONE) {
                if (xhr.status === 200) {
                    if (success)
                        success(JSON.parse(xhr.responseText));
                } else {
                    if (error)
                        error(xhr);
                }
            }
        };
        xhr.open("GET", path, true);
        xhr.send();
    }

    // Read config
    loadJSON('config.json', function(json: IConfig) {
        var config: IConfig = json;

        config.route = {
            baseUrl: config.baseUrl, /* Must be absolute url and begin with '/' */
            paths: {
                jquery: 'bower_components/jquery/dist/jquery',
                jqueryUI: 'bower_components/jquery-ui/jquery-ui',
                angular: 'bower_components/angular/angular',
                angularDateParser: 'bower_components/angular-dateparser/dateparser',
                angularRoute: 'bower_components/angular-route/angular-route',
                angularResource: 'bower_components/angular-resource/angular-resource',
                angularAnimate: 'bower_components/angular-animate/angular-animate',
                angularTranslate: 'bower_components/angular-translate/angular-translate',
                uikit: 'bower_components/uikit/js/uikit',
                numeral: 'bower_components/numeral/numeral',
                filters: 'scripts/providers/filters',
                translateProvider: 'scripts/providers/translate-provider',
                routes: 'scripts/routes',
                app: 'scripts/app',
                graphics: 'scripts/utils/graphics',
                dropdown: 'scripts/utils/dropdown',
                routeResolver: 'scripts/providers/route-resolver',
                socketProvider: 'scripts/providers/socket-provider',
                homeController: 'scripts/controllers/home-controller',
                logsController: 'scripts/controllers/logs-controller',
                proxyService: 'scripts/services/proxy-service',
                dataService: 'scripts/services/data-service',
                modalService: 'scripts/services/modal-service',
                lazyService: 'scripts/services/lazy-service',
                gaugeDirective: 'scripts/directives/gauge-directive',
                stepDirective: 'scripts/directives/stepindicator-directive',
                sliderDirective: 'scripts/directives/slider-directive',
                titleDirective: 'scripts/directives/title-directive',
                multiselectDirective: 'scripts/directives/multiselect-directive',
                datepickerDirective: 'scripts/directives/datepicker-directive',
                contextmenuDirective: 'scripts/directives/contextmenu-directive'
            },
            css: {
                uikitStyle: 'bower_components/uikit/css/uikit.css'
            },
            templates: {
                homeTemplate: '{baseUrl}templates/home.html',
                titleTemplate: 'templates/title.html',
                logsTemplate: '{baseUrl}templates/logs.html'
            },
            shim: {
                'jquery': { 'exports': 'jquery' },
                'angular': {
                    'exports': 'angular',
                    'deps': ['jquery']
                },
                'jqueryUI': ['jquery'],
                'promiseTracker': ['angular'],
                'angularAnimate': ['angular'],
                'angularBusy': ['angular', 'angularAnimate'],
                'angularRoute': ['angular'],
                'angularResource': ['angular'],
                'angularTranslate': ['angular'],
                'angularDateParser': ['angular'],
                'homeController': ['uikit'],
                'logsController': ['uikit'],
                'stepDirective': ['numeral', 'graphics'],
                'dropdown': ['jqueryUI'],
                'contextmenuDirective': ['dropdown'],
                'multiselectDirective': ['graphics', 'dropdown'],
                'gaugeDirective': ['graphics'],
                'sliderDirective': ['graphics']
            },
            priority: ['jquery', 'angular']
        };

        // Format config
        function formatConfig(section: any) {
            var regex = new RegExp('^\{.*}', 'i');
            for (var name in section) {
                if (typeof section[name] === "object") {
                    formatConfig(section[name]);
                }
                else if (typeof section[name] === "string") {
                    var match = regex.exec(section[name]);
                    if (match) {
                        var len = match[0].length;
                        var keyw = match[0].substring(1, len - 1);
                        if (config[keyw]) {
                            section[name] = config[keyw] + section[name].substring(len);
                        }
                    }
                }
            }
        }
        formatConfig(json);

        // http://code.angularjs.org/1.2.1/docs/guide/bootstrap#overview_deferred-bootstrap
        window.name = 'NG_DEFER_BOOTSTRAP!';

        requirejs.config(config.route)(['jquery', 'angular', 'app', 'filters', 'angularAnimate', 'routes', 'translateProvider'], function($: JQuery, angular: ng.IAngularStatic, app: ng.IModule) {
            angular.module('config', []).constant('config', config);

            angular.element().ready(function() {
                angular.resumeBootstrap([app.name]);
            });
        });
    });
}