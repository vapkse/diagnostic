/// <reference path="../../../typings/angularjs/angular.d.ts" />

'use strict';

module dampApp {
    define(['app', 'scripts/amp/common.js'], function(app: Routage.IModule) {
        app.register.controller('controller019', ($scope: ng.IScope, commonService: ICommonService, $filter: ng.IFilterService) => new Controller019($scope, commonService, $filter));
    });

    interface localScope extends ng.IScope {
        selectedAmp: IAmpinfo,
        steps: StepindicatorOptions,
        /* global vapkse */
        modulation: vapkse.GaugeOptions,
        /* global vapkse */
        tube1: vapkse.GaugeOptions,
        /* global vapkse */
        tube2: vapkse.GaugeOptions,
        /* global vapkse */
        tube3: vapkse.GaugeOptions,
        /* global vapkse */
        tube4: vapkse.GaugeOptions,
        /* global vapkse */
        tube5: vapkse.GaugeOptions,
        /* global vapkse */
        tube6: vapkse.GaugeOptions,
        /* global vapkse */
        tube7: vapkse.GaugeOptions,
        /* global vapkse */
        tube8: vapkse.GaugeOptions,
        /* global vapkse */
        out1: vapkse.GaugeOptions,
        /* global vapkse */
        out2: vapkse.GaugeOptions,
        /* global vapkse */
        out3: vapkse.GaugeOptions,
        /* global vapkse */
        out4: vapkse.GaugeOptions,
        /* global vapkse */
        out5: vapkse.GaugeOptions,
        /* global vapkse */
        out6: vapkse.GaugeOptions,
        /* global vapkse */
        out7: vapkse.GaugeOptions,
        /* global vapkse */
        out8: vapkse.GaugeOptions,
        /* global vapkse */
        airtemp: vapkse.GaugeOptions,
        /* global vapkse */
        pwrtemp: vapkse.GaugeOptions,
        /* global vapkse */
        regtemp: vapkse.GaugeOptions,
    }

    export class Controller019 {
        constructor(scope: ng.IScope, commonService: ICommonService, $filter: ng.IFilterService) {
            var $scope = <localScope>scope;
            $scope.$watch('displayedDatas', function(display: IAmpdata) {
                var speed = display && display.fromMeasure ? 2 : 20;

                var val1 = display && display.val && display.val[0] * 100 / 255;
                var val2 = display && display.val && display.val[1] * 100 / 255;
                var val3 = display && display.val && display.val[2] * 100 / 255;
                var val4 = display && display.val && display.val[3] * 100 / 255;

                var percent1 = val1 + val2 > 0 && val1 * 100 / (val1 + val2);
                var percent2 = val1 + val2 > 0 && val2 * 100 / (val1 + val2);
                var percent3 = val3 + val4 > 0 && val3 * 100 / (val3 + val4);
                var percent4 = val3 + val4 > 0 && val4 * 100 / (val3 + val4);

                //(0mA - 220mA)
                var percent5 = display && display.val && display.val[4] * 100 / 255;
                var percent6 = display && display.val && display.val[5] * 100 / 255;
                var percent7 = display && display.val && display.val[6] * 100 / 255;
                var percent8 = display && display.val && display.val[7] * 100 / 255;

                // (0- 100%)
                var output1 = display && display.out && display.out[0] * 100 / 255;
                var output2 = display && display.out && display.out[1] * 100 / 255;
                var output3 = display && display.out && display.out[2] * 100 / 255;
                var output4 = display && display.out && display.out[3] * 100 / 255;
                var output5 = display && display.out && display.out[4] * 100 / 255;
                var output6 = display && display.out && display.out[5] * 100 / 255;
                var output7 = display && display.out && display.out[6] * 100 / 255;
                var output8 = display && display.out && display.out[7] * 100 / 255;

                //(0 - 125)
                var percentAirTemp = display && display.temp && display.temp[0] ? Math.max(Math.min(display.temp[0] - 25, 100), 0.1) : 0;
                var percentPsTemp = display && display.temp && display.temp[1] ? Math.max(Math.min(display.temp[1] - 25, 100), 0.1) : 0;
                var regTemp = display && display.temp && display.temp[2] ? Math.max(Math.min(display.temp[2] - 25, 100), 0.1) : 0;

                // (21% - 100%)
                var modulationPeak = display && display.val && Math.max(display.temp[3], 0) * 100 / 255;

                var step = display ? display.step : 0;

                var ref = display && display.ref && step >= 2 && display.ref * 100 / 255;

                var driverRef = display && 50;

                var finalPercentMinMax = step >= 2 ? [
                    display.min * 100 / 255,
                    display.max * 100 / 255
                ] : [];

                var error = display && display.errn && $scope.selectedAmp && $scope.selectedAmp.errors && $filter('find')($scope.selectedAmp.errors, function(err) {
                    return err.id === display.errn;
                });

                var gopts = commonService.getRegulatorGaugeOptions([], driverRef);
                gopts.titleTextSize = 16;
                gopts.valueLineWidth = 16;
                gopts.valueTextSize = 30;
                gopts.refLineWidth = 4;
                gopts.valueText = function(value: number) {
                    return angular.isNumber(value) ? Math.round(value * 2) / 10 + 'ma' : 'n.c.';
                };
                gopts.refText = function(value: number) {
                    return angular.isNumber(value) ? Math.round(value * 2) / 10 + 'ma' : '';
                };
                gopts.speed = speed;
                gopts.value = percent1;
                gopts.title = 'Tube 1';
                gopts.valueColor = (display && display.errn && display.errt === 1) ? 'hsl(0, 79%, 32%)' : 'hsl(120, 73%, 75%)';
                $scope.tube1 = gopts;

                $scope.tube2 = angular.extend({}, gopts, {
                    value: percent2,
                    title: 'Tube 2',
                    valueColor: (display && display.errn && display.errt === 2) ? 'hsl(0, 79%, 32%)' : 'hsl(120, 73%, 75%)'
                });

                $scope.tube3 = angular.extend({}, gopts, {
                    value: percent3,
                    title: 'Tube 3',
                    valueColor: (display && display.errn && display.errt === 3) ? 'hsl(0, 79%, 32%)' : 'hsl(120, 73%, 75%)'
                });

                $scope.tube4 = angular.extend({}, gopts, {
                    value: percent4,
                    title: 'Tube 4',
                    valueColor: (display && display.errn && display.errt === 4) ? 'hsl(0, 79%, 32%)' : 'hsl(120, 73%, 75%)'
                });

                gopts = commonService.getRegulatorGaugeOptions(finalPercentMinMax, ref);
                gopts.titleTextSize = 16;
                gopts.valueLineWidth = 16;
                gopts.valueTextSize = 30;
                gopts.refLineWidth = 4;
                gopts.valueText = function(value: number) {
                    return angular.isNumber(value) ? Math.round(value * 22) / 10 + 'ma' : 'n.c.';
                };
                gopts.refText = function(value: number) {
                    return angular.isNumber(value) ? Math.round(value * 22) / 10 + 'ma' : '';
                };
                gopts.speed = speed;
                gopts.value = percent5;
                gopts.title = 'Tube 5';
                gopts.valueColor = (display && display.errn && display.errt === 5) ? 'hsl(0, 79%, 32%)' : 'hsl(120, 73%, 75%)'
                $scope.tube5 = gopts;

                $scope.tube6 = angular.extend({}, gopts, {
                    value: percent6,
                    title: 'Tube 6',
                    valueColor: (display && display.errn && display.errt === 6) ? 'hsl(0, 79%, 32%)' : 'hsl(120, 73%, 75%)'
                });

                $scope.tube7 = angular.extend({}, gopts, {
                    value: percent7,
                    title: 'Tube 7',
                    valueColor: (display && display.errn && display.errt === 7) ? 'hsl(0, 79%, 32%)' : 'hsl(120, 73%, 75%)'
                });

                $scope.tube8 = angular.extend({}, gopts, {
                    value: percent8,
                    title: 'Tube 8',
                    valueColor: (display && display.errn && display.errt === 8) ? 'hsl(0, 79%, 32%)' : 'hsl(120, 73%, 75%)'
                });
                
                /* global vapkse */
                var opts = new vapkse.GaugeOptions();
                opts.titleTextSize = 16;
                opts.titleTextColor = "#ccc";
                opts.valueLineWidth = 20;
                opts.valueTextSize = 30;
                opts.limits = [9.41, 34.12, 60.39, 87.84];
                opts.valueText = function(value: number) {
                    return angular.isNumber(value) ? Math.round(value) + '%' : 'n.c.';
                };
                opts.limitsColor = "#888";
                opts.title = 'Modulation';
                opts.value = modulationPeak;
                speed = speed;
                $scope.modulation = opts;

                var oopts = commonService.getOutputGaugeOptions([1, 5, 80, 98]);
                oopts.titleTextSize = 16;
                oopts.valueTextSize = 16;
                oopts.value = output1;
                oopts.speed = 2 * speed;
                $scope.out1 = oopts;

                $scope.out2 = angular.extend({}, oopts, {
                    value: output2,
                });

                $scope.out3 = angular.extend({}, oopts, {
                    value: output3,
                });

                $scope.out4 = angular.extend({}, oopts, {
                    value: output4,
                });

                $scope.out5 = angular.extend({}, oopts, {
                    value: output5,
                });

                $scope.out6 = angular.extend({}, oopts, {
                    value: output6,
                });

                $scope.out7 = angular.extend({}, oopts, {
                    value: output7,
                });

                $scope.out8 = angular.extend({}, oopts, {
                    value: output8,
                });

                var topts = commonService.getTemperatureGaugeOptions([33, 70]);
                topts.valueText = function(value: number) {
                    return display && value > 0 ? Math.round(25 + value) + String.fromCharCode(0xB0, 0x43) : 'n.c.';
                };
                topts.titleTextSize = 16;
                topts.valueLineWidth = 20;
                topts.valueTextSize = 30;
                topts.title = 'Air Temp.';
                topts.value = percentAirTemp;
                topts.speed = speed;
                $scope.airtemp = topts;

                $scope.pwrtemp = angular.extend({}, topts, {
                    title: 'Pwr Temp.',
                    value: percentPsTemp,
                });

                $scope.regtemp = angular.extend({}, topts, {
                    title: 'Reg Temp.',
                    value: regTemp,
                });

                var stepIndicatorParams = commonService.getStepIndicatorOptions();
                if (display) {
                    stepIndicatorParams.stepIndex = display.step;
                    stepIndicatorParams.steps = [{
                        label: 'Discharge',
                        range: 8,
                    }, {
                            label: 'Heating',
                            range: 35,
                        }, {
                            label: 'Starting',
                            range: 17,
                        }, {
                            label: 'Regulating',
                            range: 40,
                        }, {
                            label: '', // Normal function
                            range: 0,
                        }, {
                            label: 'Error: ' + ((error && error.descr) || display.errn) + ':',
                            range: 0,
                            labelColor: 'hsl(0, 79%, 50%)',
                        }];
                    var hasTimeInfos = stepIndicatorParams.stepIndex < stepIndicatorParams.steps.length - 1;
                    stepIndicatorParams.stepMaxTime = hasTimeInfos ? display.steptmax : 0;
                    stepIndicatorParams.stepElapsedTime = hasTimeInfos ? display.steptelaps : 0;
                    stepIndicatorParams.stepMaxValue = hasTimeInfos ? display.stepvmax : 0;
                    stepIndicatorParams.stepValue = hasTimeInfos ? display.stepval : 0;
                }
                $scope.steps = stepIndicatorParams;
            });
        }
    }
}
