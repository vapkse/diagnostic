/// <reference path="../../../typings/angularjs/angular.d.ts" />
'use strict';
var dampApp;
(function (dampApp) {
    define(['app', 'scripts/amp/common.js'], function (app) {
        app.register.controller('controller030', function ($scope, commonService, $filter) { return new Controller030($scope, commonService, $filter); });
    });
    var Controller030 = (function () {
        function Controller030(scope, commonService, $filter) {
            var $scope = scope;
            $scope.$watch('displayedDatas', function (display) {
                var speed = display && display.fromMeasure ? 2 : 20;
                //(0mA - 220mA)
                var percent1 = display && display.val && display.val[0] * 96 / 255;
                var percent2 = display && display.val && display.val[1] * 96 / 255;
                var percent3 = display && display.val && display.val[2] * 96 / 255;
                var percent4 = display && display.val && display.val[3] * 96 / 255;
                // (21% - 100%)
                var modulationPeak = display && display.val && Math.max(display.val[4], 0) * 100 / 255;
                // (0- 100%)
                var output1 = display && display.out && display.out[0] * 100 / 255;
                var output2 = display && display.out && display.out[1] * 100 / 255;
                var output3 = display && display.out && display.out[2] * 100 / 255;
                var output4 = display && display.out && display.out[3] * 100 / 255;
                //(0 - 125)
                var percentAirTemp = display && display.temp && display.temp[0] ? Math.max(Math.min(Math.abs(display.temp[0] - 25), 100), 0.1) : 0;
                var percentRegTemp = display && display.temp && display.temp[1] ? Math.max(Math.min(Math.abs(display.temp[1] - 25), 100), 0.1) : 0;
                var percentPowerSupplyTemp = display && display.temp && display.temp[2] ? Math.max(Math.min(Math.abs(display.temp[2] - 25), 100), 0.1) : 0;
                var step = display ? display.step : 0;
                var ref = display && display.ref && step >= 4 && display.ref * 96 / 255;
                var percentMinMax = step >= 4 ? [
                    display.min * 100 / 255,
                    display.max * 100 / 255
                ] : [];
                var error = display && display.errn && $scope.selectedAmp && $scope.selectedAmp.errors && $filter('find')($scope.selectedAmp.errors, function (err) {
                    return err.id === display.errn;
                });
                var gopts = commonService.getRegulatorGaugeOptions(percentMinMax, ref);
                gopts.valueText = function (value) {
                    return angular.isNumber(value) ? Math.round(value * 22) / 10 + 'ma' : 'n.c.';
                };
                gopts.refText = function (value) {
                    return angular.isNumber(value) ? Math.round(value * 22) / 10 + 'ma' : '';
                };
                gopts.speed = speed;
                gopts.value = percent1;
                gopts.title = 'Tube Current';
                gopts.valueColor = (display && display.errn && display.errt === 1) ? 'hsl(0, 79%, 32%)' : 'hsl(120, 73%, 75%)';
                $scope.tube1 = gopts;
                $scope.tube2 = angular.extend({}, gopts, {
                    value: percent2,
                    valueColor: (display && display.errn && display.errt === 2) ? 'hsl(0, 79%, 32%)' : 'hsl(120, 73%, 75%)'
                });
                $scope.tube3 = angular.extend({}, gopts, {
                    value: percent3,
                    valueColor: (display && display.errn && display.errt === 3) ? 'hsl(0, 79%, 32%)' : 'hsl(120, 73%, 75%)'
                });
                $scope.tube4 = angular.extend({}, gopts, {
                    value: percent4,
                    valueColor: (display && display.errn && display.errt === 4) ? 'hsl(0, 79%, 32%)' : 'hsl(120, 73%, 75%)'
                });
                /* global vapkse */
                var opts = new vapkse.GaugeOptions();
                opts.titleTextSize = 16;
                opts.titleTextColor = "#ccc";
                opts.valueLineWidth = 28;
                opts.valueTextSize = 40;
                opts.limits = [21.6, 34.9, 54.5, 80.4];
                opts.valueText = function (value) {
                    return angular.isNumber(value) ? Math.round(value * 10) / 10 + '%' : 'n.c.';
                };
                opts.limitsColor = "#888";
                opts.title = 'Modulation';
                opts.value = modulationPeak;
                speed = speed;
                $scope.modulation = opts;
                var oopts = commonService.getOutputGaugeOptions([12, 30, 70, 88]);
                oopts.value = output1;
                oopts.speed = 2 * speed;
                $scope.out1 = oopts;
                $scope.out2 = angular.extend({}, oopts, {
                    value: output2,
                });
                $scope.out3 = angular.extend({}, oopts, {
                    value: output3,
                });
                $scope.out4 = angular.extend({}, oopts, {
                    value: output4,
                });
                var topts = commonService.getTemperatureGaugeOptions([33, 70]);
                topts.valueText = function (value) {
                    return display && value > 0 ? Math.round(25 + value) + String.fromCharCode(0xB0, 0x43) : 'n.c.';
                };
                topts.title = 'Air Temp.';
                topts.value = percentAirTemp;
                topts.speed = speed;
                $scope.airtemp = topts;
                $scope.pwrtemp = angular.extend({}, topts, {
                    title: 'ATX Temp.',
                    value: percentPowerSupplyTemp,
                });
                $scope.regtemp = angular.extend({}, topts, {
                    title: 'Reg Temp.',
                    value: percentRegTemp,
                });
                var stepIndicatorParams = commonService.getStepIndicatorOptions();
                if (display) {
                    stepIndicatorParams.stepIndex = display.step;
                    stepIndicatorParams.steps = [{
                            label: 'Stand-by',
                            range: 0,
                        }, {
                            label: 'Pre Stand-by',
                            range: 0,
                        }, {
                            label: 'Discharge',
                            range: 8,
                        }, {
                            label: 'Heating',
                            range: 35,
                        }, {
                            label: 'Starting',
                            range: 17,
                        }, {
                            label: 'Regulating',
                            range: 40,
                        }, {
                            label: '',
                            range: 0,
                        }, {
                            label: 'Error: ' + ((error && error.descr) || display.errn) + ':',
                            range: 0,
                            labelColor: 'hsl(0, 79%, 50%)',
                        }];
                    var hasTimeInfos = stepIndicatorParams.stepIndex < stepIndicatorParams.steps.length - 1;
                    stepIndicatorParams.stepMaxTime = hasTimeInfos ? display.steptmax : 0;
                    stepIndicatorParams.stepElapsedTime = hasTimeInfos ? display.steptelaps : 0;
                    stepIndicatorParams.stepMaxValue = hasTimeInfos ? display.stepvmax : 0;
                    stepIndicatorParams.stepValue = hasTimeInfos ? display.stepval : 0;
                }
                $scope.steps = stepIndicatorParams;
            });
        }
        return Controller030;
    }());
    dampApp.Controller030 = Controller030;
})(dampApp || (dampApp = {}));
