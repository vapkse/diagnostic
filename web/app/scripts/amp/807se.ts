/// <reference path="../../../typings/angularjs/angular.d.ts" />

'use strict';

module dampApp {
    define(['app', 'scripts/amp/common.js'], function(app: Routage.IModule) {
        app.register.controller('controller207', ($scope: ng.IScope, commonService: ICommonService, $filter: ng.IFilterService) => new Controller207($scope, commonService, $filter));
    });

    interface localScope extends ng.IScope {
        selectedAmp: IAmpinfo,
        steps: StepindicatorOptions,
        /* global vapkse */
        left: vapkse.GaugeOptions,
        /* global vapkse */
        right: vapkse.GaugeOptions,
        /* global vapkse */
        airtemp: vapkse.GaugeOptions,
    }

    export class Controller207 {
        constructor(scope: ng.IScope, commonService: ICommonService, $filter: ng.IFilterService) {
            var $scope = <localScope>scope;

            $scope.$watch('displayedDatas', function(display: IAmpdata) {
                var speed = display && display.fromMeasure ? 2 : 20;
    
                //(0mA - 100mA)
                var percentLeft = display && display.val && display.val[0] * 100 / 255;
    
                //(0mA - 100mA)
                var percentRight = display && display.val && display.val[1] * 100 / 255;
    
                //(25 - 100)
                var percentAirTemp = display && display.temp && Math.abs(display.temp[0] - 51) * 7.4 * 100 / 1023;

                var step = display ? display.step : 0;

                var ref = display && display.ref && step >= 3 && display.ref * 73 / 255;

                var percentMinMax = step >= 3 ? [
                    display.min * 100 / 255,
                    display.max * 100 / 255
                ] : [];

                var error = display && display.errn && $scope.selectedAmp && $scope.selectedAmp.errors && $filter('find')($scope.selectedAmp.errors, function(err) {
                    return err.id === display.errn;
                });

                var gopts = commonService.getRegulatorGaugeOptions(percentMinMax, ref);
                gopts.valueText = function(value: number) {
                    return angular.isNumber(value) ? Math.round(value * 0.99) + 'ma' : 'n.c.';
                };
                gopts.refText = function(value: number) {
                    return angular.isNumber(value) ? Math.round(value * 0.99) + 'ma' : '';
                };
                gopts.speed = speed;
                gopts.value = percentLeft;
                gopts.title = 'Tube Current';
                gopts.valueColor = (display && display.errn && display.errt === 1) ? 'hsl(0, 79%, 32%)' : 'hsl(120, 73%, 75%)';
                $scope.left = gopts;

                $scope.right = angular.extend({}, gopts, {
                    value: percentRight,
                    valueColor: (display && display.errn && display.errt === 2) ? 'hsl(0, 79%, 32%)' : 'hsl(120, 73%, 75%)'
                });

                var topts = commonService.getTemperatureGaugeOptions([33, 70])
                topts.valueText = function(value: number) {
                    return display ? Math.round(25 + value * 0.75) + String.fromCharCode(0xB0, 0x43) : '';
                };
                topts.title = 'Air Temperature.';
                topts.value = percentAirTemp;
                topts.speed = speed;
                /* global vapkse */
                topts.style = vapkse.GaugeStyles.vertical;
                $scope.airtemp = topts;

                var stepIndicatorParams = commonService.getStepIndicatorOptions();
                if (display) {
                    stepIndicatorParams.stepIndex = display.step;
                    stepIndicatorParams.steps = [{
                        label: 'Discharge',
                        range: 14,
                    }, {
                            label: 'Heating',
                            range: 56,
                        }, {
                            label: 'Starting',
                            range: 10,
                        }, {
                            label: 'Stabilizing',
                            range: 20,
                        }, {
                            label: '', // Normal function
                            range: 0,
                        }, {
                            label: 'Error: ' + ((error && error.descr) || display.errn) + ':',
                            range: 0,
                            labelColor: 'hsl(0, 79%, 50%)',
                        }];
                    var hasTimeInfos = stepIndicatorParams.stepIndex < stepIndicatorParams.steps.length - 1;
                    stepIndicatorParams.stepMaxTime = hasTimeInfos ? display.steptmax / 1000 : 0;
                    stepIndicatorParams.stepElapsedTime = hasTimeInfos ? display.steptelaps / 1000 : 0;
                    stepIndicatorParams.stepMaxValue = hasTimeInfos ? display.stepvmax : 0;
                    stepIndicatorParams.stepValue = hasTimeInfos ? display.stepval : 0;
                }
                $scope.steps = stepIndicatorParams;
            });
        }
    }
}

