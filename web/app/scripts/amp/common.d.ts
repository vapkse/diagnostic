declare module dampApp {
    interface ICommonService {
        getStepIndicatorOptions(): StepindicatorOptions;
        getOutputGaugeOptions(limits: Array<number>): vapkse.GaugeOptions;
        getTemperatureGaugeOptions(limits: Array<number>): vapkse.GaugeOptions;
        getRegulatorGaugeOptions(limits: Array<number>, reference: number): vapkse.GaugeOptions;
        getCenterGaugeOptions(limits: Array<number>, reference: number): vapkse.GaugeOptions;
    }
}
