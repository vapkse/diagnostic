/// <reference path="../../../typings/angularjs/angular.d.ts" />
'use strict';
var dampApp;
(function (dampApp) {
    define(['app', 'scripts/amp/common.js'], function (app) {
        app.register.controller('controller017', function ($scope, commonService, $filter) { return new Controller017($scope, commonService, $filter); });
    });
    var Controller017 = (function () {
        function Controller017(scope, commonService, $filter) {
            var $scope = scope;
            $scope.$watch('displayedDatas', function (display) {
                var speed = display && display.fromMeasure ? 2 : 20;
                //(0mA - 220mA)
                var percent1 = display && display.val && display.val[0] * 100 / 255;
                var percent2 = display && display.val && display.val[1] * 100 / 255;
                var percent3 = display && display.val && display.val[2] * 100 / 255;
                var percent4 = display && display.val && display.val[3] * 100 / 255;
                var percent5 = display && display.val && display.val[4] * 100 / 255;
                var percent6 = display && display.val && display.val[5] * 100 / 255;
                // (21% - 100%)
                var modulationPeak = display && display.val && display.val[6] * 180 / 255;
                // (0- 100%)
                var output1 = display && display.out && display.out[0] * 100 / 255;
                var output2 = display && display.out && display.out[1] * 100 / 255;
                var output3 = display && display.out && display.out[2] * 100 / 255;
                var output4 = display && display.out && display.out[3] * 100 / 255;
                var output5 = display && display.out && display.out[4] * 100 / 255;
                var output6 = display && display.out && display.out[5] * 100 / 255;
                //(0 - 125)
                var percentAirTemp = display && display.out && display.out[7] ? Math.max(Math.min(Math.abs(display.out[7] - 25), 100), 0.1) : 0;
                var percentG2Reg1Temp = display && display.temp && display.temp[0] ? Math.max(Math.min(Math.abs(display.temp[0] - 25), 100), 0.1) : 0;
                var percentG2Reg2Temp = display && display.temp && display.temp[1] ? Math.max(Math.min(Math.abs(display.temp[1] - 25), 100), 0.1) : 0;
                var percentAnodeRegTemp = display && display.temp && display.temp[2] ? Math.max(Math.min(Math.abs(display.temp[2] - 25), 100), 0.1) : 0;
                var percentPowerSupplyTemp = display && display.temp && display.temp[3] ? Math.max(Math.min(Math.abs(display.temp[3] - 25), 100), 0.1) : 0;
                var step = display ? display.step : 0;
                var ref = display && display.ref && step >= 2 && display.ref * 100 / 255;
                var percentMinMax = step >= 2 ? [
                    display.min * 100 / 255,
                    display.max * 100 / 255
                ] : [];
                var error = display && display.errn && $scope.selectedAmp && $scope.selectedAmp.errors && $filter('find')($scope.selectedAmp.errors, function (err) {
                    return err.id === display.errn;
                });
                var gopts = commonService.getRegulatorGaugeOptions(percentMinMax, ref);
                gopts.titleTextSize = 16;
                gopts.valueLineWidth = 16;
                gopts.valueTextSize = 30;
                gopts.refLineWidth = 4;
                gopts.valueText = function (value) {
                    return angular.isNumber(value) ? Math.round(value * 8.2) / 10 + 'ma' : 'n.c.';
                };
                gopts.refText = function (value) {
                    return angular.isNumber(value) ? Math.round(value * 8.2) / 10 + 'ma' : '';
                };
                gopts.speed = speed;
                gopts.value = percent1;
                gopts.title = 'Tube 1';
                gopts.valueColor = (display && display.errn && display.errt === 1) ? 'hsl(0, 79%, 32%)' : 'hsl(120, 73%, 75%)';
                $scope.tube1 = gopts;
                $scope.tube2 = angular.extend({}, gopts, {
                    value: percent2,
                    title: 'Tube 2',
                    valueColor: (display && display.errn && display.errt === 2) ? 'hsl(0, 79%, 32%)' : 'hsl(120, 73%, 75%)'
                });
                $scope.tube3 = angular.extend({}, gopts, {
                    value: percent3,
                    title: 'Tube 3',
                    valueColor: (display && display.errn && display.errt === 3) ? 'hsl(0, 79%, 32%)' : 'hsl(120, 73%, 75%)'
                });
                $scope.tube4 = angular.extend({}, gopts, {
                    value: percent4,
                    title: 'Tube 4',
                    valueColor: (display && display.errn && display.errt === 4) ? 'hsl(0, 79%, 32%)' : 'hsl(120, 73%, 75%)'
                });
                $scope.tube5 = angular.extend({}, gopts, {
                    value: percent5,
                    title: 'Tube 5',
                    valueColor: (display && display.errn && display.errt === 5) ? 'hsl(0, 79%, 32%)' : 'hsl(120, 73%, 75%)'
                });
                $scope.tube6 = angular.extend({}, gopts, {
                    value: percent6,
                    title: 'Tube 6',
                    valueColor: (display && display.errn && display.errt === 6) ? 'hsl(0, 79%, 32%)' : 'hsl(120, 73%, 75%)'
                });
                /* global vapkse */
                var opts = new vapkse.GaugeOptions();
                opts.titleTextSize = 16;
                opts.titleTextColor = "#ccc";
                opts.valueLineWidth = 20;
                opts.valueTextSize = 30;
                opts.limits = [3, 10, 28, 52, 84];
                opts.valueText = function (value) {
                    return angular.isNumber(value) ? Math.round(value) + '%' : 'n.c.';
                };
                opts.limitsColor = "#888";
                opts.title = 'Modulation';
                opts.value = modulationPeak;
                speed = speed;
                $scope.modulation = opts;
                var oopts = commonService.getOutputGaugeOptions([1, 5, 70, 88]);
                oopts.titleTextSize = 16;
                oopts.valueTextSize = 16;
                oopts.value = output1;
                oopts.speed = 2 * speed;
                $scope.out1 = oopts;
                $scope.out2 = angular.extend({}, oopts, {
                    value: output2,
                });
                $scope.out3 = angular.extend({}, oopts, {
                    value: output3,
                });
                $scope.out4 = angular.extend({}, oopts, {
                    value: output4,
                });
                $scope.out5 = angular.extend({}, oopts, {
                    value: output5,
                });
                $scope.out6 = angular.extend({}, oopts, {
                    value: output6,
                });
                var topts = commonService.getTemperatureGaugeOptions([33, 70]);
                topts.valueText = function (value) {
                    return display && value > 0 ? Math.round(25 + value) + String.fromCharCode(0xB0, 0x43) : 'n.c.';
                };
                topts.titleTextSize = 16;
                topts.valueLineWidth = 20;
                topts.valueTextSize = 30;
                topts.title = 'Air Temp.';
                topts.value = percentAirTemp;
                topts.speed = speed;
                $scope.airtemp = topts;
                $scope.g2reg1temp = angular.extend({}, topts, {
                    title: 'G2 Reg 1.',
                    value: percentG2Reg1Temp,
                });
                $scope.g2reg2temp = angular.extend({}, topts, {
                    title: 'G2 Reg 2.',
                    value: percentG2Reg2Temp,
                });
                $scope.htregtemp = angular.extend({}, topts, {
                    title: 'Anode Reg.',
                    value: percentAnodeRegTemp,
                });
                $scope.pwrtemp = angular.extend({}, topts, {
                    title: 'Pwr Supply.',
                    value: percentPowerSupplyTemp,
                });
                var stepIndicatorParams = commonService.getStepIndicatorOptions();
                if (display) {
                    stepIndicatorParams.stepIndex = display.step;
                    stepIndicatorParams.steps = [{
                            label: 'Discharge',
                            range: 8,
                        }, {
                            label: 'Heating',
                            range: 35,
                        }, {
                            label: 'Starting',
                            range: 17,
                        }, {
                            label: 'Regulating',
                            range: 40,
                        }, {
                            label: '',
                            range: 0,
                        }, {
                            label: 'Error: ' + ((error && error.descr) || display.errn) + ':',
                            range: 0,
                            labelColor: 'hsl(0, 79%, 50%)',
                        }];
                    var hasTimeInfos = stepIndicatorParams.stepIndex < stepIndicatorParams.steps.length - 1;
                    stepIndicatorParams.stepMaxTime = hasTimeInfos ? display.steptmax : 0;
                    stepIndicatorParams.stepElapsedTime = hasTimeInfos ? display.steptelaps : 0;
                    stepIndicatorParams.stepMaxValue = hasTimeInfos ? display.stepvmax : 0;
                    stepIndicatorParams.stepValue = hasTimeInfos ? display.stepval : 0;
                }
                $scope.steps = stepIndicatorParams;
            });
        }
        return Controller017;
    }());
    dampApp.Controller017 = Controller017;
})(dampApp || (dampApp = {}));
