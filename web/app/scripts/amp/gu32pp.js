/// <reference path="../../../typings/angularjs/angular.d.ts" />
'use strict';
var dampApp;
(function (dampApp) {
    define(['app', 'scripts/amp/common.js'], function (app) {
        app.register.controller('controller032', function ($scope, commonService, $filter) { return new Controller032($scope, commonService, $filter); });
    });
    var Controller032 = (function () {
        function Controller032(scope, commonService, $filter) {
            var $scope = scope;
            $scope.$watch('displayedDatas', function (display) {
                var speed = display && display.fromMeasure ? 2 : 20;
                // (0- 100%)
                var percent1 = display && display.val && display.val[0] * 100 / 255;
                var percent2 = display && display.val && display.val[1] * 100 / 255;
                // (0- 100%)
                var output1 = display && display.out && display.out[0] * 100 / 255;
                var output2 = display && display.out && display.out[1] * 100 / 255;
                var step = display ? display.step : 0;
                var ref = display && display.ref && step >= 3 && display.ref * 100 / 255;
                var percentMinMax = step >= 3 ? [
                    display.min * 100 / 255,
                    display.max * 100 / 255
                ] : [];
                var error = display && display.errn && $scope.selectedAmp && $scope.selectedAmp.errors && $filter('find')($scope.selectedAmp.errors, function (err) {
                    return err.id === display.errn;
                });
                var gopts = commonService.getRegulatorGaugeOptions(percentMinMax, ref);
                gopts.valueText = function (value) {
                    return angular.isNumber(value) ? Math.round(value * 9) / 10 + 'ma' : 'n.c.';
                };
                gopts.refText = function (value) {
                    return angular.isNumber(value) ? Math.round(value * 9) / 10 + ' ma' : '';
                };
                gopts.speed = speed;
                gopts.value = percent1;
                gopts.title = 'Tube Current';
                gopts.valueColor = (display && display.errn && display.errt === 1) ? 'hsl(0, 79%, 32%)' : 'hsl(120, 73%, 75%)';
                $scope.tube1 = gopts;
                $scope.tube2 = angular.extend({}, gopts, {
                    value: percent2,
                    valueColor: (display && display.errn && display.errt === 2) ? 'hsl(0, 79%, 32%)' : 'hsl(120, 73%, 75%)'
                });
                var oopts = commonService.getOutputGaugeOptions([12, 30, 70, 88]);
                oopts.value = output1;
                oopts.speed = 2 * speed;
                $scope.out1 = oopts;
                $scope.out2 = angular.extend({}, oopts, {
                    value: output2,
                });
                var stepIndicatorParams = commonService.getStepIndicatorOptions();
                if (display) {
                    stepIndicatorParams.stepIndex = display.step;
                    stepIndicatorParams.steps = [{
                            label: 'Discharge',
                            range: 8,
                        }, {
                            label: 'Heating',
                            range: 42,
                        }, {
                            label: 'Starting',
                            range: 8,
                        }, {
                            label: display.steptmax > 100 ? 'Fine Tuning' : 'Regulating',
                            range: 42,
                        }, {
                            label: '',
                            range: 0,
                        }, {
                            label: 'Error: ' + ((error && error.descr) || display.errn) + ':',
                            range: 0,
                            labelColor: 'hsl(0, 79%, 50%)',
                        }];
                    var hasTimeInfos = stepIndicatorParams.stepIndex < stepIndicatorParams.steps.length - 1;
                    stepIndicatorParams.stepMaxTime = hasTimeInfos ? display.steptmax : 0;
                    stepIndicatorParams.stepElapsedTime = hasTimeInfos ? display.steptelaps : 0;
                    stepIndicatorParams.stepMaxValue = hasTimeInfos ? display.stepvmax : 0;
                    stepIndicatorParams.stepValue = hasTimeInfos ? display.stepval : 0;
                }
                $scope.steps = stepIndicatorParams;
            });
        }
        return Controller032;
    }());
    dampApp.Controller032 = Controller032;
})(dampApp || (dampApp = {}));
