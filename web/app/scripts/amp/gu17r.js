/// <reference path="../../../typings/angularjs/angular.d.ts" />
'use strict';
var dampApp;
(function (dampApp) {
    define(['app', 'scripts/amp/common.js'], function (app) {
        app.register.controller('controller147', function ($scope, commonService, $filter) { return new Controller147($scope, commonService, $filter); });
    });
    var Controller147 = (function () {
        function Controller147(scope, commonService, $filter) {
            var $scope = scope;
            $scope.$watch('displayedDatas', function (display) {
                var speed = display && display.fromMeasure ? 2 : 20;
                //(0mA - 220mA)
                var percent1 = display && display.val && display.val[0] * 100 / 255;
                var percent2 = display && display.val && display.val[1] * 100 / 255;
                var percent3 = display && display.val && display.val[2] * 100 / 255;
                var percent4 = display && display.val && display.val[3] * 100 / 255;
                // (0- 100%)
                var output1 = display && display.out && display.out[0] * 100 / 255;
                var output2 = display && display.out && display.out[1] * 100 / 255;
                var output3 = display && display.out && display.out[2] * 100 / 255;
                var output4 = display && display.out && display.out[3] * 100 / 255;
                //(0 - 110)
                var airTemp = display && display.temp && display.temp[0] ? display.temp[0] * 100 / 255 : 0;
                var regTemp = display && display.temp && display.temp[1] ? display.temp[1] * 100 / 255 : 0;
                var modulationPeak = display && display.temp && display.temp[3] * 100 / 255;
                var step = display ? display.step : 0;
                var ref = display && display.ref && step >= 2 && display.ref * 100 / 255;
                var percentMinMax = step >= 2 ? [
                    display.min * 100 / 255,
                    display.max * 100 / 255
                ] : [];
                var error = display && display.errn && $scope.selectedAmp && $scope.selectedAmp.errors && $filter('find')($scope.selectedAmp.errors, function (err) {
                    return err.id === display.errn;
                });
                var gopts = commonService.getRegulatorGaugeOptions(percentMinMax, ref);
                gopts.titleTextSize = 16;
                gopts.valueLineWidth = 16;
                gopts.valueTextSize = 30;
                gopts.refLineWidth = 4;
                gopts.valueText = function (value) {
                    return angular.isNumber(value) ? Math.round(value * 7) / 10 + 'ma' : 'n.c.';
                };
                gopts.refText = function (value) {
                    return angular.isNumber(value) ? Math.round(value * 7) / 10 + 'ma' : '';
                };
                gopts.speed = speed;
                gopts.value = percent1;
                gopts.title = 'Tube 1';
                gopts.valueColor = (display && display.errn && display.errt === 1) ? 'hsl(0, 79%, 32%)' : 'hsl(120, 73%, 75%)';
                $scope.tube1 = gopts;
                $scope.tube2 = angular.extend({}, gopts, {
                    value: percent2,
                    title: 'Tube 2',
                    valueColor: (display && display.errn && display.errt === 2) ? 'hsl(0, 79%, 32%)' : 'hsl(120, 73%, 75%)'
                });
                $scope.tube3 = angular.extend({}, gopts, {
                    value: percent3,
                    title: 'Tube 3',
                    valueColor: (display && display.errn && display.errt === 3) ? 'hsl(0, 79%, 32%)' : 'hsl(120, 73%, 75%)'
                });
                $scope.tube4 = angular.extend({}, gopts, {
                    value: percent4,
                    title: 'Tube 4',
                    valueColor: (display && display.errn && display.errt === 4) ? 'hsl(0, 79%, 32%)' : 'hsl(120, 73%, 75%)'
                });
                /* global vapkse */
                var opts = new vapkse.GaugeOptions();
                opts.titleTextSize = 16;
                opts.titleTextColor = "#ccc";
                opts.valueLineWidth = 20;
                opts.valueTextSize = 30;
                opts.limits = [4, 24, 48, 72];
                opts.valueText = function (value) {
                    return angular.isNumber(value) ? Math.round(value * 10) / 10 + '%' : 'n.c.';
                };
                opts.limitsColor = "#888";
                opts.title = 'Modulation';
                opts.value = modulationPeak;
                speed = speed;
                $scope.modulation = opts;
                var oopts = commonService.getOutputGaugeOptions([1, 5, 70, 88]);
                oopts.titleTextSize = 16;
                oopts.valueTextSize = 16;
                oopts.value = output1;
                oopts.speed = 2 * speed;
                $scope.out1 = oopts;
                $scope.out2 = angular.extend({}, oopts, {
                    value: output2,
                });
                $scope.out3 = angular.extend({}, oopts, {
                    value: output3,
                });
                $scope.out4 = angular.extend({}, oopts, {
                    value: output4,
                });
                var topts = commonService.getTemperatureGaugeOptions([33, 70]);
                topts.valueText = function (value) {
                    return display && value > 0 ? Math.round(25 + value) + String.fromCharCode(0xB0, 0x43) : 'n.c.';
                };
                topts.titleTextSize = 16;
                topts.valueLineWidth = 20;
                topts.valueTextSize = 30;
                topts.title = 'Air Temp.';
                topts.value = airTemp * 100 / 110;
                topts.speed = speed;
                $scope.airtemp = topts;
                $scope.regtemp = angular.extend({}, topts, {
                    title: 'Reg Temp.',
                    value: regTemp * 100 / 110,
                });
                var stepIndicatorParams = commonService.getStepIndicatorOptions();
                if (display) {
                    stepIndicatorParams.stepIndex = display.step;
                    stepIndicatorParams.steps = [{
                            label: 'Discharge',
                            range: 8,
                        }, {
                            label: 'Heating',
                            range: 35,
                        }, {
                            label: 'Starting',
                            range: 17,
                        }, {
                            label: 'Regulating',
                            range: 40,
                        }, {
                            label: '',
                            range: 0,
                        }, {
                            label: 'Error: ' + ((error && error.descr) || display.errn) + ':',
                            range: 0,
                            labelColor: 'hsl(0, 79%, 50%)',
                        }];
                    var hasTimeInfos = stepIndicatorParams.stepIndex < stepIndicatorParams.steps.length - 1;
                    stepIndicatorParams.stepMaxTime = hasTimeInfos ? display.steptmax : 0;
                    stepIndicatorParams.stepElapsedTime = hasTimeInfos ? display.steptelaps : 0;
                    stepIndicatorParams.stepMaxValue = hasTimeInfos ? display.stepvmax : 0;
                    stepIndicatorParams.stepValue = hasTimeInfos ? display.stepval : 0;
                }
                $scope.steps = stepIndicatorParams;
            });
        }
        return Controller147;
    }());
    dampApp.Controller147 = Controller147;
})(dampApp || (dampApp = {}));
