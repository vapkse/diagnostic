/// <reference path="../../../typings/angularjs/angular.d.ts" />
'use strict';
var dampApp;
(function (dampApp) {
    define(['app', 'scripts/amp/common.js'], function (app) {
        app.register.controller('controller122', function ($scope, commonService, $filter) { return new Controller122($scope, commonService, $filter); });
    });
    var Controller122 = (function () {
        function Controller122(scope, commonService, $filter) {
            var $scope = scope;
            $scope.$watch('displayedDatas', function (display) {
                var speed = display && display.fromMeasure ? 2 : 20;
                var currentLeftA1 = display && display.val && display.val[0] * 100 / 255;
                var currentLeftA2 = display && display.val && display.val[1] * 100 / 255;
                var currentRightA1 = display && display.val && display.val[2] * 100 / 255;
                var currentRightA2 = display && display.val && display.val[3] * 100 / 255;
                var percentAirTemp = display && display.temp && display.temp[0] ? Math.max(Math.min(display.temp[0] - 25, 100), 0.1) : 0;
                var percentRegLTemp = display && display.temp && display.temp[1] ? Math.max(Math.min(display.temp[1] - 25, 100), 0.1) : 0;
                var percentRegRTemp = display && display.temp && display.temp[2] ? Math.max(Math.min(display.temp[2] - 25, 100), 0.1) : 0;
                var step = display ? display.step : 0;
                var error = display && display.errn && $scope.selectedAmp && $scope.selectedAmp.errors && $filter('find')($scope.selectedAmp.errors, function (err) {
                    return err.id === display.errn;
                });
                var percentMinMax = step >= 3 ? [
                    display.min * 100 / 255,
                    display.max * 100 / 255
                ] : [];
                var percentRef = display ? display.ref * 100 / 290 : 0;
                var gopts = commonService.getRegulatorGaugeOptions(percentMinMax, percentRef);
                gopts.valueText = function (value) {
                    return angular.isNumber(value) ? Math.round(value * 3.36) / 10 + 'ma' : 'n.c.';
                };
                gopts.refText = function (value) {
                    return angular.isNumber(value) ? Math.round(value * 3.36) / 10 + 'ma' : '';
                };
                gopts.speed = speed;
                gopts.value = currentLeftA1;
                gopts.title = 'Anode1 Current';
                gopts.valueColor = (display && display.errn && display.errt === 1) ? 'hsl(0, 79%, 32%)' : 'hsl(120, 73%, 75%)';
                $scope.lefta1 = gopts;
                $scope.lefta2 = angular.extend({}, gopts, {
                    title: 'Anode2 Current',
                    value: currentLeftA2,
                    valueColor: (display && display.errn && display.errt === 2) ? 'hsl(0, 79%, 32%)' : 'hsl(120, 73%, 75%)'
                });
                $scope.righta1 = angular.extend({}, gopts, {
                    title: 'Anode1 Current',
                    value: currentRightA1,
                    valueColor: (display && display.errn && display.errt === 3) ? 'hsl(0, 79%, 32%)' : 'hsl(120, 73%, 75%)'
                });
                $scope.righta2 = angular.extend({}, gopts, {
                    title: 'Anode2 Current',
                    value: currentRightA2,
                    valueColor: (display && display.errn && display.errt === 4) ? 'hsl(0, 79%, 32%)' : 'hsl(120, 73%, 75%)'
                });
                var topts = commonService.getTemperatureGaugeOptions([33, 70]);
                topts.valueText = function (value) {
                    return angular.isNumber(value) ? Math.round(25 + value) + String.fromCharCode(0xB0, 0x43) : '';
                };
                topts.title = 'Air Temperature.';
                topts.value = percentAirTemp;
                topts.speed = speed;
                /* global vapkse */
                topts.style = vapkse.GaugeStyles.vertical;
                topts.valueTextSize = 30;
                $scope.airtemp = topts;
                $scope.regltemp = angular.extend({}, topts, {
                    title: 'Regulators Temp.',
                    value: percentRegLTemp,
                });
                $scope.regrtemp = angular.extend({}, topts, {
                    title: 'Regulators Temp.',
                    value: percentRegRTemp,
                });
                var stepIndicatorParams = commonService.getStepIndicatorOptions();
                if (display) {
                    stepIndicatorParams.stepIndex = display.step;
                    stepIndicatorParams.steps = [{
                            label: 'Discharge',
                            range: 8,
                        }, {
                            label: 'Heating',
                            range: 35,
                        }, {
                            label: 'Starting',
                            range: 17,
                        }, {
                            label: 'Regulating',
                            range: 40,
                        }, {
                            label: '',
                            range: 0,
                        }, {
                            label: 'Error: ' + ((error && error.descr) || display.errn) + ':',
                            range: 0,
                            labelColor: 'hsl(0, 79%, 50%)',
                        }];
                    var hasTimeInfos = stepIndicatorParams.stepIndex < stepIndicatorParams.steps.length - 1;
                    stepIndicatorParams.stepMaxTime = hasTimeInfos ? display.steptmax : 0;
                    stepIndicatorParams.stepElapsedTime = hasTimeInfos ? display.steptelaps : 0;
                    stepIndicatorParams.stepMaxValue = hasTimeInfos ? display.stepvmax : 0;
                    stepIndicatorParams.stepValue = hasTimeInfos ? display.stepval : 0;
                }
                $scope.steps = stepIndicatorParams;
            });
        }
        return Controller122;
    }());
    dampApp.Controller122 = Controller122;
})(dampApp || (dampApp = {}));
