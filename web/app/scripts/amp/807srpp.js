/// <reference path="../../../typings/angularjs/angular.d.ts" />
'use strict';
var dampApp;
(function (dampApp) {
    define(['app', 'scripts/amp/common.js'], function (app) {
        app.register.controller('controller208', function ($scope, commonService, $filter) { return new Controller208($scope, commonService, $filter); });
    });
    var Controller208 = (function () {
        function Controller208(scope, commonService, $filter) {
            var $scope = scope;
            $scope.$watch('displayedDatas', function (display) {
                var speed = display && display.fromMeasure ? 2 : 20;
                var step = display ? display.step : 0;
                //(-100% - 100%)
                var percentCommand = display && (step >= 2 ? display.val[0] * 100 / 255 : 50);
                //(-100% - 100%)
                var percentOut = display && (step >= 2 ? display.val[1] * 100 / 255 : 50);
                // (0- 100%)
                var output1 = display && (step >= 2 && display.out[0] * 100 / 255);
                //(0 - 125)
                var percentAirTemp = display && display.temp && display.temp[0] ? Math.max(Math.min(Math.abs(display.temp[0] - 25), 100), 0.1) : 0;
                var percentShuntTemp = display && display.temp && display.temp[1] ? Math.max(Math.min(Math.abs(display.temp[1] - 25), 100), 0.1) : 0;
                var percentRegTemp = display && display.temp && display.temp[2] ? Math.max(Math.min(Math.abs(display.temp[2] - 25), 100), 0.1) : 0;
                var percentTransfoTemp = display && display.temp && display.temp[3] ? Math.max(Math.min(Math.abs(display.temp[3] - 25), 100), 0.1) : 0;
                var ref = display && display.ref && display.ref * 100 / 255;
                var percentMinMax = display ? [
                    display.min * 100 / 255,
                    display.max * 100 / 255
                ] : [];
                var error = display && display.errn && $scope.selectedAmp && $scope.selectedAmp.errors && $filter('find')($scope.selectedAmp.errors, function (err) {
                    return err.id === display.errn;
                });
                var copt = commonService.getCenterGaugeOptions(percentMinMax, ref);
                copt.valueText = function (value) {
                    return angular.isNumber(value) ? Math.round((value * 2 - 99.6) * 41.8) + 'mV' : '';
                };
                copt.title = 'Command';
                copt.value = percentCommand;
                copt.speed = 2 * speed;
                $scope.command = copt;
                $scope.output = angular.extend({}, copt, {
                    value: percentOut,
                    title: 'Output',
                });
                var oopt = commonService.getOutputGaugeOptions([12, 30, 70, 88]);
                oopt.title = 'Regulator Position';
                oopt.value = output1;
                oopt.speed = speed;
                $scope.regpos = oopt;
                var topts = commonService.getTemperatureGaugeOptions([33, 70]);
                topts.valueText = function (value) {
                    return display && value > 0 ? Math.round(25 + value) + String.fromCharCode(0xB0, 0x43) : 'n.c.';
                };
                topts.title = 'Air Temperature.';
                topts.value = percentAirTemp;
                topts.speed = speed;
                $scope.airtemp = topts;
                $scope.shunttemp = angular.extend({}, topts, {
                    title: 'Shunt Temp.',
                    value: percentShuntTemp,
                });
                $scope.regtemp = angular.extend({}, topts, {
                    title: 'Reg. Temp.',
                    value: percentRegTemp,
                });
                $scope.transfotemp = angular.extend({}, topts, {
                    title: 'Int. Transformer.',
                    value: percentTransfoTemp,
                });
                var stepIndicatorParams = commonService.getStepIndicatorOptions();
                if (display) {
                    stepIndicatorParams.stepIndex = display.step;
                    stepIndicatorParams.steps = [{
                            label: 'Heating',
                            range: 50,
                        }, {
                            label: 'Starting',
                            range: 10,
                        }, {
                            label: 'Stabilizing',
                            range: 40,
                        }, {
                            label: '',
                            range: 0,
                        }, {
                            label: 'Error: ' + ((error && error.descr) || display.errn) + ':',
                            range: 0,
                            labelColor: 'hsl(0, 79%, 50%)',
                        }];
                    var hasTimeInfos = stepIndicatorParams.stepIndex < stepIndicatorParams.steps.length - 1;
                    stepIndicatorParams.stepMaxTime = hasTimeInfos ? display.steptmax / 1000 : 0;
                    stepIndicatorParams.stepElapsedTime = hasTimeInfos ? display.steptelaps / 1000 : 0;
                    stepIndicatorParams.stepMaxValue = hasTimeInfos ? display.stepvmax : 0;
                    stepIndicatorParams.stepValue = hasTimeInfos ? display.stepval : 0;
                }
                $scope.steps = stepIndicatorParams;
            });
        }
        return Controller208;
    }());
    dampApp.Controller208 = Controller208;
})(dampApp || (dampApp = {}));
