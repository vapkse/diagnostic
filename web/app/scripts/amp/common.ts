module dampApp {
    'use strict';

    define(['app', 'gaugeDirective', 'stepDirective'], function(app: Routage.IModule) {
        app.register.service('commonService', CommonService);
    });

    interface IValueColor {
        color?: string,
        min?: number,
        max?: number,
        h?: number,
        s?: number,
        l?: number,
        h1?: number,
        s1?: number,
        l1?: number,
        h2?: number,
        s2?: number,
        l2?: number,
        r?: number,
        g?: number,
        b?: number,
        a?: number,
        r1?: number,
        g1?: number,
        b1?: number,
        a1?: number,
        r2?: number,
        g2?: number,
        b2?: number,
        a2?: number,
    }

    export interface ICommonService {
        getStepIndicatorOptions(): StepindicatorOptions,
        getOutputGaugeOptions(limits: Array<number>): vapkse.GaugeOptions,
        getTemperatureGaugeOptions(limits: Array<number>): vapkse.GaugeOptions,
        getRegulatorGaugeOptions(limits: Array<number>, reference: number): vapkse.GaugeOptions,
        getCenterGaugeOptions(limits: Array<number>, reference: number): vapkse.GaugeOptions,
    }

    class CommonService {
        static $inject = ['$filter'];
        public constructor($filter: ng.IFilterService) {
            return {
                getStepIndicatorOptions: function(): StepindicatorOptions {
                    var opts = new StepindicatorOptions();
                    opts.stepSeparatorColor = '#888';
                    opts.labelColor = "#eee";
                    return opts;
                },

                getOutputGaugeOptions: function(limits: Array<number>): vapkse.GaugeOptions {
                    /* globals vapkse */
                    var opts = new vapkse.GaugeOptions();
                    opts.valueColor = <Array<IValueColor>>[{
                        max: 0.1,
                        color: "#222"
                    }, {
                            min: 0.1,
                            max: limits[0],
                            h: 0,
                            s: 79,
                            l: 32
                        }, {
                            min: limits[0],
                            max: limits[1],
                            h1: 0,
                            s1: 79,
                            l1: 32,
                            h2: 120,
                            s2: 73,
                            l2: 75
                        }, {
                            min: limits[1],
                            max: limits[2],
                            h: 120,
                            s: 73,
                            l: 75
                        }, {
                            min: limits[2],
                            max: limits[3],
                            h1: 120,
                            s1: 73,
                            l1: 75,
                            h2: 0,
                            s2: 79,
                            l2: 32
                        }, {
                            min: limits[3],
                            h: 0,
                            s: 79,
                            l: 32
                        }];
                    opts.valueText = function(value: number) {
                        return angular.isNumber(value) && value > 0 ? Math.floor(value / 100 * 100) + '%' : '';
                    };
                    opts.titleTextSize = 16;
                    opts.valueTextSize = 20;
                    opts.getColor = this.getGaugeColor;
                    opts.limits = [50];
                    opts.limitsColor = "red";
                    opts.speed = 0.4;
                    opts.style = vapkse.GaugeStyles.horizontal;
                    return opts;
                },

                getTemperatureGaugeOptions: function(limits: Array<number>): vapkse.GaugeOptions {
                    /* globals vapkse */
                    var opts = new vapkse.GaugeOptions();
                    opts.valueColor = <Array<IValueColor>>[{
                        max: 0.1,
                        color: "#ccc"
                    }, {
                            min: 0.1,
                            max: limits[0],
                            h: 120,
                            s: 73,
                            l: 75
                        }, {
                            min: limits[0],
                            max: limits[1],
                            h1: 120,
                            s1: 73,
                            l1: 75,
                            h2: 0,
                            s2: 79,
                            l2: 32
                        }, {
                            min: limits[1],
                            h: 0,
                            s: 79,
                            l: 32
                        }];
                    opts.titleTextSize = 16;
                    opts.titleTextColor = "#ccc";
                    opts.valueLineWidth = 28;
                    opts.valueTextSize = 40;
                    opts.getColor = this.getGaugeColor;
                    return opts;
                },

                getRegulatorGaugeOptions: function(limits: Array<number>, reference: number): vapkse.GaugeOptions {
                    /* globals vapkse */
                    var opts = new vapkse.GaugeOptions();
                    opts.valueColor = <Array<IValueColor>>[{
                        max: 0.1,
                        color: "#ccc"
                    }, {
                            min: 0.1,
                            max: limits[0] - 10,
                            h: 195,
                            s: 53,
                            l: 79
                        }, {
                            min: limits[0] - 10,
                            max: reference - 10,
                            h1: 195,
                            s1: 53,
                            l1: 79,
                            h2: 120,
                            s2: 73,
                            l2: 75
                        }, {
                            min: reference - 10,
                            max: reference + 10,
                            h: 120,
                            s: 73,
                            l: 75
                        }, {
                            min: reference + 10,
                            max: limits[1] + 1,
                            h1: 120,
                            s1: 73,
                            l1: 75,
                            h2: 0,
                            s2: 79,
                            l2: 32
                        }, {
                            min: limits[1] + 1,
                            h: 0,
                            s: 79,
                            l: 32
                        }];
                    opts.titleTextSize = 16;
                    opts.titleTextColor = "#ccc";
                    opts.valueLineWidth = 20;
                    opts.valueTextSize = 40;
                    opts.reference = reference;
                    opts.refColor = "#ccc";
                    opts.refLineWidth = 8;
                    opts.refTextSize = 14;
                    opts.limits = limits;
                    opts.limitsColor = "red";
                    opts.getColor = this.getGaugeColor;
                    opts.speed = 0.4;
                    return opts;
                },

                getCenterGaugeOptions: function(limits: Array<number>, reference: number): vapkse.GaugeOptions {
                    /* globals vapkse */
                    var opts = new vapkse.GaugeOptions();
                    opts.valueColor = <Array<IValueColor>>[{
                        max: 0.1,
                        color: "#222"
                    }, {
                            min: 0.1,
                            max: limits[0],
                            h: 0,
                            s: 79,
                            l: 32
                        }, {
                            min: limits[0],
                            max: limits[0] + 20,
                            h1: 0,
                            s1: 79,
                            l1: 32,
                            h2: 120,
                            s2: 73,
                            l2: 75
                        }, {
                            min: limits[0] + 20,
                            max: limits[1] - 20,
                            h: 120,
                            s: 73,
                            l: 75
                        }, {
                            min: limits[1] - 20,
                            max: limits[1],
                            h1: 120,
                            s1: 73,
                            l1: 75,
                            h2: 0,
                            s2: 79,
                            l2: 32
                        }, {
                            min: limits[1],
                            h: 0,
                            s: 79,
                            l: 32
                        }];
                    opts.valueText = function(value: number) {
                        return angular.isNumber(value) ? Math.round(value * 2 - 100) + ' ' : '';
                    };
                    opts.titleTextSize = 16;
                    opts.valueLineWidth = 28;
                    opts.valueTextSize = 40;
                    opts.getColor = this.getGaugeColor;
                    opts.limits = reference ? [reference] : [];
                    opts.limitsColor = "red";
                    opts.speed = 0.4;
                    opts.style = vapkse.GaugeStyles.horizontal;
                    return opts;
                },

                getGaugeColor: function(colorName: string, value: number): string {
                    var clr = this[colorName];
                    if (clr instanceof Array) {
                        var color = <Array<IValueColor>>clr;
                        var range = $filter('find')(color, function(col) {
                            return value >= (col.min || 0) && (!col.max || value < col.max);
                        });

                        if (range) {
                            var str: Array<string> = [];
                            if (range.color) {
                                return range.color;
                            }
                            else if (range.r !== undefined && range.g !== undefined && range.b !== undefined) {
                                str.push(range.a ? 'rgba(' : 'rgb(');
                                str.push(String(range.r));
                                str.push(',');
                                str.push(String(range.g));
                                str.push(',');
                                str.push(String(range.b));
                                if (range.a) {
                                    str.push(',');
                                    str.push(String(range.a));
                                }
                                str.push(')');
                                return str.join('');
                            }
                            else if (range.h !== undefined && range.s !== undefined && range.l !== undefined) {
                                str.push(range.a ? 'hsla(' : 'hsl(');
                                str.push(String(range.h));
                                str.push(',');
                                str.push(String(range.s));
                                str.push('%,');
                                str.push(String(range.l));
                                str.push('%');
                                if (range.a) {
                                    str.push(',');
                                    str.push(String(range.a));
                                }
                                str.push(')');
                                return str.join('');
                            }
                            else if (range.h1 !== undefined && range.s1 !== undefined && range.l1 !== undefined && range.h2 !== undefined && range.s2 !== undefined && range.l2 !== undefined) {
                                var min = range.min || 0;
                                var f = (value - min) / (range.max - min);
                                var h = Math.round(range.h1 + f * (range.h2 - range.h1));
                                var s = Math.round(range.s1 + f * (range.s2 - range.s1));
                                var l = Math.round(range.l1 + f * (range.l2 - range.l1));
                                var a: number;
                                if (range.a1) {
                                    a = Math.round(range.a1 + f * (range.a2 - range.a1));
                                }
                                str.push(a ? 'hsla(' : 'hsl(');
                                str.push(String(h));
                                str.push(',');
                                str.push(String(s));
                                str.push('%,');
                                str.push(String(l));
                                str.push('%');
                                if (a) {
                                    str.push(',');
                                    str.push(String(a));
                                }
                                str.push(')');
                                return str.join('');
                            }
                        }

                        return String(color[0].color || color[0]);
                    }
                    else if (typeof clr === 'function') {
                        return clr(value);
                    }

                    return clr;
                },
            };
        }
    }
}

