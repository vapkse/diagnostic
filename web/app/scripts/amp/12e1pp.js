/// <reference path="../../../typings/angularjs/angular.d.ts" />
'use strict';
var dampApp;
(function (dampApp) {
    define(['app', 'scripts/amp/common.js'], function (app) {
        app.register.controller('controller212', function ($scope, commonService, $filter) { return new Controller212($scope, commonService, $filter); });
    });
    var Controller212 = (function () {
        function Controller212(scope, commonService, $filter) {
            var $scope = scope;
            $scope.$watch('displayedDatas', function (display) {
                var speed = display && display.fromMeasure ? 2 : 20;
                //(0mA - 148mA)
                var percent1 = display && display.val && display.val[0] * 100 / 255;
                var percent2 = display && display.val && display.val[1] * 100 / 255;
                var percent3 = display && display.val && display.val[2] * 100 / 255;
                var percent4 = display && display.val && display.val[3] * 100 / 255;
                // (0% - 100%)
                var modulationPeak = display && display.temp && display.val[4] * 4 * 100 / 255;
                var modulation = display && display.temp && display.val[5] * 4 * 100 / 255;
                // (0- 100%)
                var output1 = display && display.out && display.out[0] * 100 / 255;
                var output2 = display && display.out && display.out[1] * 100 / 255;
                var output3 = display && display.out && display.out[2] * 100 / 255;
                var output4 = display && display.out && display.out[3] * 100 / 255;
                //(25 - 100)
                var percentAirTemp = display && display.temp && Math.abs(display.temp[0] - 25) * 14.8 * 100 / 1023;
                var percentPsTemp = display && display.temp && Math.abs(Math.max(display.temp[1], display.temp[2]) - 25) * 14.8 * 100 / 1023;
                var step = display ? display.step : 0;
                var ref = display && display.ref && step >= 2 && display.ref * 100 / 255;
                var percentMinMax = step >= 2 ? [
                    display.min * 100 / 255,
                    display.max * 100 / 255
                ] : [];
                var error = display && display.errn && $scope.selectedAmp && $scope.selectedAmp.errors && $filter('find')($scope.selectedAmp.errors, function (err) {
                    return err.id === display.errn;
                });
                var gopts = commonService.getRegulatorGaugeOptions(percentMinMax, ref);
                gopts.valueText = function (value) {
                    return angular.isNumber(value) ? Math.round(value * 23) / 10 + 'ma' : 'n.c.';
                };
                gopts.refText = function (value) {
                    return angular.isNumber(value) ? Math.round(value * 23) / 10 + 'ma' : '';
                };
                gopts.speed = speed;
                gopts.value = percent1;
                gopts.title = 'Tube Current';
                gopts.valueColor = (display && display.errn && display.errt === 1) ? 'hsl(0, 79%, 32%)' : 'hsl(120, 73%, 75%)';
                $scope.tube1 = gopts;
                $scope.tube2 = angular.extend({}, gopts, {
                    value: percent2,
                    valueColor: (display && display.errn && display.errt === 2) ? 'hsl(0, 79%, 32%)' : 'hsl(120, 73%, 75%)'
                });
                $scope.tube3 = angular.extend({}, gopts, {
                    value: percent3,
                    valueColor: (display && display.errn && display.errt === 3) ? 'hsl(0, 79%, 32%)' : 'hsl(120, 73%, 75%)'
                });
                $scope.tube4 = angular.extend({}, gopts, {
                    value: percent4,
                    valueColor: (display && display.errn && display.errt === 4) ? 'hsl(0, 79%, 32%)' : 'hsl(120, 73%, 75%)'
                });
                /* global vapkse */
                var opts = new vapkse.GaugeOptions();
                opts.titleTextSize = 16;
                opts.titleTextColor = "#ccc";
                opts.valueLineWidth = 28;
                opts.valueTextSize = 40;
                opts.reference = modulation;
                opts.refColor = "#ddd";
                opts.refLineWidth = 8;
                opts.limits = [7.8 * 4, 15.6 * 4];
                opts.valueText = function (value) {
                    return angular.isNumber(value) ? Math.round(value) + '%' : 'n.c.';
                };
                opts.limitsColor = "#888";
                opts.title = 'Modulation';
                opts.value = modulationPeak;
                speed = speed;
                $scope.modulation = opts;
                var oopts = commonService.getOutputGaugeOptions([5, 15, 85, 95]);
                oopts.value = output1;
                oopts.speed = 2 * speed;
                $scope.out1 = oopts;
                $scope.out2 = angular.extend({}, oopts, {
                    value: output2,
                });
                $scope.out3 = angular.extend({}, oopts, {
                    value: output3,
                });
                $scope.out4 = angular.extend({}, oopts, {
                    value: output4,
                });
                var topts = commonService.getTemperatureGaugeOptions([33, 70]);
                topts.valueText = function (value) {
                    return angular.isNumber(value) ? Math.round(25 + value * 0.75) + String.fromCharCode(0xB0, 0x43) : 'n.c.';
                };
                topts.title = 'Air Temperature.';
                topts.value = percentAirTemp;
                topts.speed = speed;
                $scope.airtemp = topts;
                $scope.pwrtemp = angular.extend({}, topts, {
                    title: 'PSupply Temp.',
                    value: percentPsTemp,
                });
                var stepIndicatorParams = commonService.getStepIndicatorOptions();
                if (display) {
                    stepIndicatorParams.stepIndex = display.step;
                    stepIndicatorParams.steps = [{
                            label: 'Discharge',
                            range: 8,
                        }, {
                            label: 'Heating',
                            range: 35,
                        }, {
                            label: 'Starting',
                            range: 17,
                        }, {
                            label: 'Regulating',
                            range: 40,
                        }, {
                            label: '',
                            range: 0,
                        }, {
                            label: 'Error: ' + ((error && error.descr) || display.errn) + ':',
                            range: 0,
                            labelColor: 'hsl(0, 79%, 50%)',
                        }];
                    var hasTimeInfos = stepIndicatorParams.stepIndex < stepIndicatorParams.steps.length - 1;
                    stepIndicatorParams.stepMaxTime = hasTimeInfos ? display.steptmax : 0;
                    stepIndicatorParams.stepElapsedTime = hasTimeInfos ? display.steptelaps : 0;
                    stepIndicatorParams.stepMaxValue = hasTimeInfos ? display.stepvmax : 0;
                    stepIndicatorParams.stepValue = hasTimeInfos ? display.stepval : 0;
                }
                $scope.steps = stepIndicatorParams;
            });
        }
        return Controller212;
    }());
    dampApp.Controller212 = Controller212;
})(dampApp || (dampApp = {}));
