/// <reference path="../../typings/angularjs/angular.d.ts" />
var dampApp;
(function (dampApp) {
    'use strict';
    define(['app'], function (app) {
        return app.config(['$routeProvider', 'routeResolverProvider', '$controllerProvider', '$compileProvider', '$filterProvider', '$provide', '$httpProvider', '$locationProvider', 'config',
            function ($routeProvider, routeResolverProvider, $controllerProvider, $compileProvider, $filterProvider, $provide, $httpProvider, $locationProvider, config) {
                $locationProvider.html5Mode(true).hashPrefix('!');
                app.register = {
                    controller: $controllerProvider.register,
                    directive: $compileProvider.directive,
                    filter: $filterProvider.register,
                    factory: $provide.factory,
                    service: $provide.service,
                    provider: $provide.provider,
                    constant: $provide.constant,
                };
                // route.resolve(page, controller, [dependencies], [css])
                $routeProvider.when(config.page.home, routeResolverProvider.resolve('homeTemplate', 'homeController', ['homeController'], ['uikitStyle']));
                $routeProvider.when(config.page.logs, routeResolverProvider.resolve('logsTemplate', 'logsController', ['logsController'], ['uikitStyle']));
                $routeProvider.otherwise({
                    redirectTo: config.page.home
                });
            }]);
    });
})(dampApp || (dampApp = {}));
