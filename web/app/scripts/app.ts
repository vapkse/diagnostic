/// <reference path="../../typings/angularjs/angular.d.ts" />

'use strict';

module dampApp {
    define(['angular', 'routeResolver', 'socketProvider', 'angularRoute', 'angularTranslate', 'angularAnimate', 'angularDateParser'], function(angular: ng.IAngularStatic) {
        /* global Routage */
        angular.module('routeResolverServices', ['config']).provider('routeResolver', Routage.RouteResolver.provider);

        /* global SocketIO */
        angular.module('socketProviders', []).provider('socketFactory', SocketIO.SocketProviders.provider)
            .factory('socket', SocketIO.SocketProviders.factory);

        /* global Translation, Utilities */
        return angular.module("dampApp", ['ngRoute', 'routeResolverServices', 'socketProviders', 'ngAnimate', 'config', 'pascalprecht.translate', 'dateParser'])
            .config(Translation.TranslateProvider.configure)
            .filter('notExists', Utilities.Filters.propertyMissing)
            .filter('exists', Utilities.Filters.propertyExists)
            .filter('getByProperty', Utilities.Filters.getByProperty)
            .filter('find', Utilities.Filters.find)
            .run(function($rootScope: ng.IRootScopeService, config: IConfig) {
                $rootScope.appInfos = config.appInfos;
            });
    });
}