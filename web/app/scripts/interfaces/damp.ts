'use strict';

module dampApp {
    export interface IAppInfos {
        app: string,
        version: string,
        pageTitle: string,
        productTitle: string
    }

    export interface IAppPages {
        home: string,
        logs: string    
    }
    
    export interface IConfig extends Routage.IRoutageConfig{
        [index: string]: any,
        baseUrl: string,
        appInfos: IAppInfos,
        page: IAppPages
    }

    export interface IAmpData{
        id: number,
        step: number,
        steptmax: number,
        steptelaps: number,
        stepvmax: number,
        stepval: number,
        tick: number,
        errn: number,
        errt: number,
        ref: number,
        min: number,
        max: number,
        val: Array<number>,
        out: Array<number>,
        temp: Array<number>
    }
}

