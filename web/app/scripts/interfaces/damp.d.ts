declare module dampApp {
    interface IAppInfos {
        app: string;
        version: string;
        pageTitle: string;
        productTitle: string;
    }
    interface IAppPages {
        home: string;
        logs: string;
    }
    interface IConfig extends Routage.IRoutageConfig {
        [index: string]: any;
        baseUrl: string;
        appInfos: IAppInfos;
        page: IAppPages;
    }
    interface IAmpData {
        id: number;
        step: number;
        steptmax: number;
        steptelaps: number;
        stepvmax: number;
        stepval: number;
        tick: number;
        errn: number;
        errt: number;
        ref: number;
        min: number;
        max: number;
        val: Array<number>;
        out: Array<number>;
        temp: Array<number>;
    }
}
