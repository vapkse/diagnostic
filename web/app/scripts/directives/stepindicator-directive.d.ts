/// <reference path="../../../typings/angularjs/angular.d.ts" />
declare module dampApp {
    interface IStepindicatorStep {
        label: string;
        range: number;
        maxTime?: number;
        maxValue?: number;
        valueLineColor?: string;
        timeLineColor?: string;
        labelColor?: string;
        stepSeparatorColor?: string;
        backgroundColor?: string;
    }
    class StepindicatorOptions {
        valueLineColor: string;
        timeLineColor: string;
        labelColor: string;
        stepSeparatorColor: string;
        backgroundColor: string;
        labelTextSize: number;
        progressSize: number;
        speed: number;
        steps: Array<IStepindicatorStep>;
        stepMaxTime: number;
        stepElapsedTime: number;
        stepMaxValue: number;
        stepValue: number;
        stepIndex: number;
        element: JQuery;
        getColor: (colorName: string, value?: number) => any;
    }
    class StepindicatorDirective implements ng.IDirective {
        constructor($window: ng.IWindowService);
    }
}
