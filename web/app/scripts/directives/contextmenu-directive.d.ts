/// <reference path="../../../typings/angularjs/angular.d.ts" />
declare module vapkse {
    class ContextMenuOptions {
        autoFocus: boolean;
        autoSelect: boolean;
        position: JQueryUI.JQueryPositionOptions;
        items: Array<DropDownItem>;
        parentElement: JQuery;
        selectedIndex: number;
        visibleItems: () => DropDownItem[];
        next: (value?: number | DropDownItem) => any;
        prev: (value?: number | DropDownItem) => any;
        first: () => any;
        last: () => any;
        selectedItem: (value?: number | DropDownItem | JQuery) => any;
        scrollToView: (element: JQuery) => boolean;
        close: (value?: boolean) => void;
        open: () => any;
        currentFilter: IDropDownFilter;
        left: number;
        element: JQuery;
        isVisible: (value?: boolean) => any;
        refresh: () => any;
        onopened: () => any;
        onopening: (e: OpeningEvent) => void;
        onclosed: () => any;
        onresized: () => any;
        onitemclicked: (value: string) => void;
        onitemselected: () => any;
        onitemsrequired: Function;
    }
    interface OpeningEvent extends Event {
        options: ContextMenuOptions;
    }
}
