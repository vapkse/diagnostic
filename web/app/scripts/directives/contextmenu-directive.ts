/// <reference path="../../../typings/angularjs/angular.d.ts" />

'use strict';

module vapkse {
    if (define) {
        define(['app', 'modalService'], function(app: Routage.IModule) {
            app.register.directive('contextmenu', ($document: ng.IDocumentService, $compile: ng.ICompileService, $filter: ng.IFilterService, modalService: Modal.IModalService, $timeout: ng.ITimeoutService) => new ContextMenuDirective($document, $compile, $filter, modalService, $timeout));
        });
    } else {
        angular.module('vapkse').directive('contextmenu', ($document: ng.IDocumentService, $compile: ng.ICompileService, $filter: ng.IFilterService, modalService: Modal.IModalService, $timeout: ng.ITimeoutService) => new ContextMenuDirective($document, $compile, $filter, modalService, $timeout));
    }

    var $filter: ng.IFilterService;

    export class ContextMenuOptions {
        autoFocus = false;
        autoSelect = true;
        position: JQueryUI.JQueryPositionOptions = {
            my: "left top",
            at: "left bottom",
            collision: "flipfit",
            within: "body"
        };
        items: Array<DropDownItem> = [];
        parentElement: JQuery;
        selectedIndex = -1;
        visibleItems = function(): Array<DropDownItem> {
            return <Array<DropDownItem>>$filter('filter')(this.items, function(item: DropDownItem) {
                return item.visible !== false && !(<localItem>item).filtered && item.value();
            });
        };
        next = function(value?: number | DropDownItem) {
            var item = typeof value === 'number' ? this.items[value] : value || this.selectedItem();
            if (item) {
                var visibleItems = this.visibleItems();
                var index = visibleItems.indexOf(item);
                return index >= 0 && index < visibleItems.length - 1 && visibleItems[++index];
            }
            return this.first();
        };
        prev = function(value?: number | DropDownItem) {
            var item = typeof value === 'number' ? this.items[value] : value || this.selectedItem();
            if (item) {
                var visibleItems = this.visibleItems();
                var index = visibleItems.indexOf(item);
                return index > 0 && index < visibleItems.length && visibleItems[--index];
            }
            return this.last();
        };
        first = function() {
            var visibleItems = this.visibleItems();
            return visibleItems.length && visibleItems[0];
        };
        last = function() {
            var visibleItems = this.visibleItems();
            return visibleItems.length && visibleItems[visibleItems.length - 1];
        };
        selectedItem = function(value?: number | DropDownItem | JQuery) {
            var that = this;
            var selectedItem: localItem;

            if (typeof value === 'number') {
                // Selected index specified
                that.selectedItem(value >= 0 && value < that.items.length ? that.items[value] : null);
            }
            else if (value instanceof jQuery) {
                // Selected element specified                
                angular.forEach(that.items, function(item) {
                    if (item.element.is(value)) {
                        selectedItem = item;
                    }
                });

                that.selectedItem(selectedItem);
            }
            else if (value instanceof DropDownItem) {
                var selectedValue = value && value.value && value.value() || value;
                that.selectedIndex = -1;
                angular.forEach(that.items, function(item, index) {
                    if (selectedValue && item.value() === selectedValue) {
                        selectedItem = item;
                        that.selectedIndex = index;
                        if (item.element) {
                            item.element.addClass("contextmenu-item-active");
                        }
                    }
                    else {
                        if (item.element) {
                            item.element.removeClass("contextmenu-item-active");
                        }
                    }
                });

                that.scrollToView(selectedItem && selectedItem.element);
    
                // Callback
                that.onitemselected(selectedItem);
            }
            return that.selectedIndex >= 0 && that.selectedIndex < that.items.length ? that.items[that.selectedIndex] : null;
        };
        scrollToView = function(element: JQuery) {
            if (!element || !element.is(":visible")) {
                return;
            }

            var parent = element.parent();
            var maxHeight = parent.height() - element.height() - 1;
            var min = parent.scrollTop();
            var max = min + maxHeight;
            var position = min + element.position().top;
            if (position < min) {
                parent.animate({
                    scrollTop: position
                }, 100);
                // Not in view so scroll to it
                return false;
            }
            else if (position > max) {
                parent.animate({
                    scrollTop: position - maxHeight
                }, 100);
                // Not in view so scroll to it
                return false;
            }
            return true;
        };
        close = function(value?: boolean) {};
        open = $.noop;
        currentFilter: IDropDownFilter = {
            text: undefined,
            filter: undefined,
            clear: function(): void {
                this.text = undefined;
                this.filter = undefined;
            }
        };
        left: number;
        element: JQuery;
        isVisible = function(value?: boolean) {
            if (value !== undefined) {
                if (value) {
                    this.open();
                }
                else {
                    this.close();
                }
            }
            return this.element && this.element.is(":visible");
        };
        refresh = $.noop;
        // Callbacks
        onopened = $.noop;
        onopening = function(e: OpeningEvent) { };
        onclosed = $.noop;
        onresized = $.noop;
        onitemclicked = function(value: string) { };
        onitemselected = $.noop;
        onitemsrequired: Function;
    }

    export interface OpeningEvent extends Event {
        options: ContextMenuOptions    
    }

    interface localItem extends DropDownItem {
        filtered: boolean,
        element: ng.IAugmentedJQuery,
        onclicked(): void,
        isSeparator: boolean,
    }

    interface localOptions extends ContextMenuOptions {        
        filter(filter: string): void,
        resize(): void,
    }

    interface localScope extends ng.IScope {
        options: localOptions;
        onitemclicked(value: string): void,
    }

    class ContextMenuDirective {
        constructor($document: ng.IDocumentService, $compile: ng.ICompileService, filter: ng.IFilterService, modalService: Modal.IModalService, $timeout: ng.ITimeoutService) {
            $filter = filter;

            return {
                restrict: 'E',
                template: '<ul class="contextmenu transparent"></ul>',
                replace: true,
                scope: true,
                compile: function() {
                    return {
                        pre: function(scope: ng.IScope, element: ng.IAugmentedJQuery, attrs: any) {
                            var $scope = <localScope>scope;
                            var backdropHtml = '<div class="contextmenu-backdrop" ng-click="options.close()"></div>';
                            var backdrop = angular.element(backdropHtml);
                            $compile(backdrop)($scope);
                            $('body').append(backdrop);
                            backdrop.hide();
                            element.hide();

                            var escape = function(value: any) {
                                return value && value.toString().replace(/[\-\[\]{}()*+?.,\\\^$|#\s]/g, "\\$&");
                            };

                            var onKeyDown = function(e: JQueryKeyEventObject) {
                                switch (e.keyCode) {
                                    case $.ui.keyCode.HOME:
                                        $scope.options.selectedItem($scope.options.first());
                                        break;
                                    case $.ui.keyCode.END:
                                        $scope.options.selectedItem($scope.options.last());
                                        break;
                                    case $.ui.keyCode.PAGE_UP:
                                    case $.ui.keyCode.UP:
                                        if (e.altKey) {
                                            $scope.options.close();
                                        }
                                        else if (!e.ctrlKey) {
                                            $scope.options.selectedItem($scope.options.prev());
                                        }
                                        break;
                                    case $.ui.keyCode.PAGE_DOWN:
                                    case $.ui.keyCode.DOWN:
                                        if (e.altKey) {
                                            $scope.options.close();
                                        }
                                        else if (!e.ctrlKey) {
                                            $scope.options.selectedItem($scope.options.next());
                                        }
                                        break;
                                    case $.ui.keyCode.LEFT:
                                    case $.ui.keyCode.ESCAPE:
                                    case $.ui.keyCode.RIGHT:
                                        $scope.options.close();
                                        break;
                                    case $.ui.keyCode.ENTER:
                                    case $.ui.keyCode.SPACE:
                                        var selectedItem = $scope.options.selectedItem();
                                        if (selectedItem) {
                                            $scope.onitemclicked(selectedItem.value());
                                        }
                                        break;
                                    default:
                                        if (!$scope.options.autoSelect) {
                                            return;
                                        }

                                        var previousText = $scope.options.currentFilter && $scope.options.currentFilter.text || "";
                                        var currentText = String.fromCharCode(e.keyCode);
                                        var selectedElement = $scope.options.selectedItem() && $scope.options.selectedItem().element;

                                        $timeout.cancel(this.filterTimer);

                                        if (currentText === previousText) {
                                            // Next match
                                        }
                                        else {
                                            // New Match
                                            currentText = previousText + currentText;

                                            var regex = new RegExp("^" + escape(currentText), "i");
                                            $scope.options.currentFilter.filter = element.children(".contextmenu-item").filter(function() {
                                                return regex.test($(this).children("a").text());
                                            });

                                            $scope.options.currentFilter.text = currentText;
                                        }

                                        // If no matches on the current filter, reset to the last character pressed
                                        // to move down the menu to the first item that starts with that character
                                        if (!$scope.options.currentFilter.filter.length) {
                                            currentText = String.fromCharCode(e.keyCode);
                                            var regex2 = new RegExp("^" + escape(currentText), "i");
                                            $scope.options.currentFilter.filter = element.children(".contextmenu-item").filter(function() {
                                                return regex2.test($(this).children("a").text());
                                            });

                                            $scope.options.currentFilter.text = currentText;
                                        }

                                        if ($scope.options.currentFilter.filter.length) {
                                            var currentIndex = selectedElement ? $scope.options.currentFilter.filter.index(selectedElement) : -1;
                                            if (++currentIndex >= $scope.options.currentFilter.filter.length) {
                                                currentIndex = 0;
                                            }
                                            $scope.options.selectedItem($scope.options.currentFilter.filter.eq(currentIndex));

                                            if ($scope.options.currentFilter.filter.length > 1) {
                                                this.filterTimer = $timeout(function() {
                                                    $scope.options.currentFilter.clear();
                                                }, 1000);
                                            }
                                            else {
                                                $scope.options.currentFilter.clear();
                                            }
                                        }
                                        else {
                                            $scope.options.currentFilter.clear();
                                        }

                                }
                            };

                            $scope.onitemclicked = function(value: string) {
                                var clickedItems = $filter('filter')($scope.options.items, function(item) {
                                    return item.value() === value;
                                });

                                if (clickedItems.length) {
                                    var item = <localItem>clickedItems[0];
                                    if (item.onclicked) {
                                        item.onclicked();
                                        $scope.options.close();
                                    }
                                    else {
                                        $scope.options.onitemclicked(value);
                                    }
                                }
                            };

                            var optionsWatcher = function(value: ContextMenuOptions) {
                                var closeTimer: ng.IPromise<any>;

                                $scope.options = <localOptions>(value || new ContextMenuOptions());
                                $scope.options.element = element;

                                // Complete the current option instance
                                angular.extend($scope.options, new ContextMenuOptions(), angular.extend({}, $scope.options));

                                $scope.options.refresh = function() {
                                    angular.forEach($scope.options.items, function(item) {
                                        var localItem = <localItem>item;
                                        if (item.visible && !localItem.filtered) {
                                            localItem.element.removeClass('hidden');
                                        }
                                        else {
                                            localItem.element.addClass('hidden');
                                        }

                                        localItem.element.find('a').text(item.text());
                                    });
                                };

                                $scope.options.open = function() {
                                    if (!element.is(":visible")) {
                                        // Close all other instances
                                        modalService.closeAllModalInstances();

                                        // Load on demand
                                        if ($scope.options.onitemsrequired) {
                                            if ($scope.options.onitemsrequired($scope.options, function(options: ContextMenuOptions) {
                                                // By callback
                                                if (options) {
                                                    optionsWatcher(options);
                                                }

                                                showMenu(true);
                                            })) {
                                                // Open directly
                                                showMenu(true);
                                            }
                                            else {
                                                showMenu(false);
                                            }

                                            // Waiting for callback
                                            return;
                                        }

                                        showMenu(true);
                                    }
                                };

                                $scope.options.filter = function(filter: string) {
                                    filter = filter && filter.toLowerCase();
                                    var visibleCount = 0;
                                    var lastVisibleItem: localItem;
                                    angular.forEach($scope.options.visibleItems(), function(item) {
                                        var localItem = <localItem>item;
                                        localItem.filtered = filter && item.text().toLowerCase().indexOf(filter) !== 0;
                                        if (!localItem.filtered) {
                                            visibleCount++;
                                            lastVisibleItem = localItem;
                                        }
                                    });

                                    $scope.options.refresh();
                                    $scope.options.selectedItem(visibleCount === 1 ? lastVisibleItem : null);
                                };

                                function htmlDecode(value: string) {
                                    return $('<div/>').html(value).text();
                                }

                                var showMenu = function(refreshHtml: boolean) {
                                    var event = <OpeningEvent>new Event('opening');
                                    event.options = $scope.options;
                                    $scope.options.onopening(event);
                                    if (event.cancelBubble) {
                                        return;
                                    }

                                    if (refreshHtml) {
                                        element.empty();
                                        angular.forEach($scope.options.items, function(item) {
                                            var localItem = <localItem>item;
                                            var itemhtml: string;
                                            if (localItem.isSeparator) {
                                                itemhtml = '<li class="contextmenu-separator' + (localItem.visible === false || localItem.filtered ? ' hidden' : '') + '">' + htmlDecode(localItem.text()) + '</li>';
                                            }
                                            else {
                                                itemhtml = '<li class="contextmenu-item' + (localItem.visible === false || localItem.filtered ? ' hidden' : '') + '"><a ng-click="onitemclicked(\'' + escape(localItem.value()) + '\')">' + (htmlDecode(localItem.text()) || '&nbsp;') + '</a></li>';
                                            }
                                            var itemelement = angular.element(itemhtml);
                                            $compile(itemelement)($scope);
                                            localItem.element = itemelement;
                                            element.append(itemelement);
                                        });
                                    }

                                    delete $scope.options.currentFilter;

                                    // size and position menu
                                    element.show();
                                    backdrop.show();

                                    $scope.options.resize();

                                    if ($scope.options.left) {
                                        $scope.options.position.at = "left+" + $scope.options.left + " bottom";
                                    }
                                    else {
                                        $scope.options.position.at = "left bottom";
                                    }

                                    var position = angular.extend({
                                        of: $scope.options.parentElement
                                    }, $scope.options.position);
                                    element.position(position);

                                    if ($scope.options.autoFocus && $scope.options.selectedIndex === -1) {
                                        $scope.options.selectedItem(0);
                                    }
                                    else {
                                        $scope.options.selectedItem($scope.options.selectedIndex);
                                    }

                                    element.removeClass('transparent');

                                    $scope.options.onopened();
                                    $document.on('keydown', onKeyDown);
                                };

                                $scope.options.close = function(immediate: boolean) {
                                    if (element.is(":visible")) {
                                        backdrop.hide();
                                        $document.off('keydown', onKeyDown);
                                        $scope.options.onclosed();
                                        element.addClass('transparent');
                                        if (closeTimer) {
                                            $timeout.cancel(closeTimer);
                                            closeTimer = null;
                                        }
                                        if (immediate) {
                                            element.hide();
                                        }
                                        else {
                                            closeTimer = $timeout(function() {
                                                element.hide();
                                            }, 200);
                                        }
                                    }
                                };

                                modalService.addModalInstance(String($scope.$id), function() {
                                    $scope.options.close(true);
                                });

                                $scope.options.resize = function() {
                                    // Firefox wraps long text (possibly a rounding bug)
                                    // so we add 1px to avoid the wrapping (#7513)
                                    if ($scope.options.left) {
                                        element.outerWidth(Math.max(element.width("").outerWidth() + 1, 100));
                                    }
                                    else {
                                        element.outerWidth(Math.max(element.width("").outerWidth() + 1, ($scope.options.parentElement && $scope.options.parentElement.outerWidth()) || 1000));
                                    }
                                    $scope.options.onresized();
                                };
                            };

                            $scope.$parent.$watch(attrs.options, optionsWatcher);
                        },
                    };
                },
            };
        }
    }
}

