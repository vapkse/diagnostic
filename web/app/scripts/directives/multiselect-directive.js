/// <reference path="../../../typings/angularjs/angular.d.ts" />
'use strict';
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var vapkse;
(function (vapkse) {
    var multiselect;
    (function (multiselect_1) {
        if (define) {
            define(['app', 'datepickerDirective', 'contextmenuDirective'], function (app) {
                app.register.constant('multiselectModes', Modes);
                app.register.directive('multiselect', function ($filter, $compile, $parse, $dateParser, $timeout) { return new MultiSelectDirective($filter, $compile, $parse, $dateParser, $timeout); });
            });
        }
        else {
            angular.module('vapkse').constant('multiselectModes', Modes).directive('multiselect', function ($filter, $compile, $parse, $dateParser, $timeout) { return new MultiSelectDirective($filter, $compile, $parse, $dateParser, $timeout); });
        }
        (function (Modes) {
            Modes[Modes["select"] = 1] = "select";
            Modes[Modes["autoComplete"] = 2] = "autoComplete";
            Modes[Modes["freeText"] = 4] = "freeText";
            Modes[Modes["datePicker"] = 8] = "datePicker";
            Modes[Modes["multiselect"] = 16] = "multiselect";
            Modes[Modes["hasInput"] = 30] = "hasInput";
            Modes[Modes["hasDropDown"] = 13] = "hasDropDown";
        })(multiselect_1.Modes || (multiselect_1.Modes = {}));
        var Modes = multiselect_1.Modes;
        var Item = vapkse.DropDownItem;
        // Wait item
        var waititem = angular.extend({}, new Item());
        var $filter;
        var $dateParser;
        var Options = (function () {
            function Options() {
                this.mode = Modes.select;
                this.items = [waititem];
                this.selectedtext = '';
                this.textAttribute = '_text';
                this.valueAttribute = '_value';
                this.dateFormat = 'dd/MM/yy';
                this.dropDownWithin = "body";
                this.formatSelectionCssClass = function (element) {
                    return undefined;
                };
                this._isReadOnly = false;
                this.isReadOnly = function (value) {
                    if (value !== undefined) {
                        this._isReadOnly = value;
                    }
                    return this._isReadOnly;
                };
                this.visibleItems = function () {
                    return $filter('filter')(this.items, function (item) {
                        return item.visible !== false;
                    });
                };
                this.normalizedItems = function (items) {
                    var that = this;
                    var resultitems = [];
                    var normalizeItem = function (value, resultitems) {
                        /*jslint bitwise: true */
                        if ((that.mode & Modes.multiselect) !== 0) {
                            var msitem;
                            if (typeof value === 'string') {
                                msitem = new MSItem();
                                var text = that.getItemText(value);
                                msitem.text(text);
                                msitem.value(value);
                            }
                            else if (value instanceof MSItem) {
                                msitem = value;
                            }
                            else {
                                msitem = new MSItem();
                                msitem.text(value.text());
                                msitem.value(value.value());
                            }
                            resultitems.push(msitem);
                        }
                        else {
                            var item;
                            if (typeof value === 'string') {
                                item = new Item();
                                var text2 = that.getItemText(value);
                                item.text(text2);
                                item.value(value);
                            }
                            else {
                                item = value || new Item();
                            }
                            item.textAttribute = that.textAttribute;
                            item.valueAttribute = that.valueAttribute;
                            resultitems.push(item);
                        }
                    };
                    if (items) {
                        if (items instanceof Array) {
                            angular.forEach(items, function (item) {
                                normalizeItem(item, resultitems);
                            }, resultitems);
                        }
                        else {
                            normalizeItem(items, resultitems);
                        }
                    }
                    return resultitems;
                };
                this.andOrOperatorsItems = function () {
                    var items = this.normalizedItems(['AND', 'OR']);
                    items[0].isOperator = true;
                    items[1].isOperator = true;
                    return items;
                };
                this.rangeOperatorItems = function () {
                    var items = this.normalizedItems(['-']);
                    items[0].isOperator = true;
                    return items;
                };
                this.openParenthesisItems = function () {
                    var items = this.normalizedItems(['(']);
                    items[0].isOpenParenthesis = true;
                    items.isParenthesis = true;
                    return items;
                };
                this.closeParenthesisItems = function () {
                    var items = this.normalizedItems([')']);
                    items[0].isCloseParenthesis = true;
                    items.isParenthesis = true;
                    return items;
                };
                this.next = function (value) {
                    var item = typeof value === 'number' ? this.items[value] : value || this.selectedItem();
                    if (item) {
                        var visibleItems = this.visibleItems();
                        var index = visibleItems.indexOf(item);
                        return index >= 0 && index < visibleItems.length - 1 && visibleItems[++index];
                    }
                    return this.first();
                };
                this.prev = function (value) {
                    var item = typeof value === 'number' ? this.items[value] : value || this.selectedItem();
                    if (item) {
                        var visibleItems = this.visibleItems();
                        var index = visibleItems.indexOf(item);
                        return index > 0 && index < visibleItems.length && visibleItems[--index];
                    }
                    else {
                        return this.last();
                    }
                };
                this.first = function () {
                    var visibleItems = this.visibleItems();
                    return visibleItems.length && visibleItems[0];
                };
                this.last = function () {
                    var visibleItems = this.visibleItems();
                    return visibleItems.length && visibleItems[visibleItems.length - 1];
                };
                this.queryString = function (value) {
                    if (value === undefined) {
                        return this._selectedItem && this._selectedItem.toQueryString() || '';
                    }
                    /*jslint bitwise: true */
                    if ((this.mode & Modes.multiselect) !== 0) {
                        return this.selectedItem(MultiSelect.parse(this, value, true));
                    }
                    else {
                        return this.selectedItem(value);
                    }
                };
                this.selectedItem = function (value) {
                    var that = this;
                    if (value === undefined) {
                        return that._selectedItem;
                    }
                    /*jslint bitwise: true */
                    if ((that.mode & Modes.multiselect) !== 0) {
                        if (value instanceof MSItem) {
                            // Check for modification
                            if (MultiSelect.equals(that._selectedItem, value)) {
                                that._selectedItem = value;
                                return that._selectedItem;
                            }
                            that._selectedItem = value;
                            that.selectedtext = that._selectedItem && that._selectedItem.tostring() || '';
                        }
                        else if (typeof value === 'string') {
                            return that.selectedItem(MultiSelect.parse(that, value));
                        }
                        else {
                            throw 'Not implemented';
                        }
                    } /*jslint bitwise: true */
                    else if ((that.mode & Modes.datePicker) !== 0) {
                        if (value instanceof Date || value === undefined) {
                            var dateItem = angular.extend({}, new Item());
                            dateItem.value(value);
                            dateItem.text(value instanceof Date ? $filter('date')(value, that.dateFormat) : value);
                            dateItem.isValue = true;
                            dateItem.isDate = true;
                            if (angular.equals(dateItem, that._selectedItem)) {
                                that._selectedItem = dateItem;
                                return that._selectedItem;
                            }
                            that._selectedItem = dateItem;
                            that.selectedtext = dateItem.text();
                        }
                        else {
                            // Try to parse
                            var date;
                            try {
                                date = $dateParser(value.toString(), that.dateFormat);
                            }
                            catch (e) {
                            }
                            return that.selectedItem(date instanceof Date ? date : null);
                        }
                    }
                    else if (typeof value === 'number') {
                        // Selected index specified
                        return that.selectedItem(value >= 0 && value < that.items.length ? that.items[value] : null);
                    }
                    else {
                        var selectedValue = value instanceof Item ? value.value() : value;
                        var selectedItem = null;
                        for (var i = 0; i < (that.items || []).length; i++) {
                            if (that.items[i].value() === selectedValue) {
                                selectedItem = that.items[i];
                                break;
                            }
                        }
                        if (angular.equals(that._selectedItem, selectedItem)) {
                            return null;
                        }
                        // Callback
                        that._selectedItem = selectedItem;
                        that.selectedtext = selectedItem && selectedItem.text() || '';
                    }
                    return (that.onitemselected && that.onitemselected(that._selectedItem)) || that._selectedItem;
                };
                this.getItemText = function (value) { return value; };
                // Callbacks
                this.onitemselected = function (item) { };
                this.ondropdownopening = function (e) { };
                this.ondropdownopened = function (that) { };
                this.onkeydown = function (e) { };
                this.ontextchanged = function (text) { };
            }
            return Options;
        }());
        multiselect_1.Options = Options;
        var MultiSelectDirective = (function () {
            function MultiSelectDirective(filter, $compile, $parse, dateParser, $timeout) {
                $filter = filter;
                $dateParser = dateParser;
                return {
                    restrict: 'E',
                    template: '<span class="multiselect" tabindex="0"><input ng-show="hasInput()" ng-model="options.selectedtext" ng-trim="false"></input><span class="placeHolder" ng-hide="hasInput()">{{options.selectedtext}}</span><span ng-show="hasDropDown()" class="multiselect-dropdown-button"></span></span>',
                    replace: true,
                    scope: true,
                    compile: function () {
                        return {
                            pre: function (scope, element, attrs) {
                                var $scope = scope;
                                var selectChoice = 'input';
                                var multiSelecteParserTimer;
                                /* globals graphics */
                                var lastClickedPosition = new graphics.Position();
                                $scope.hasInput = function () {
                                    /*jslint bitwise: true */
                                    return ($scope.options.mode & Modes.hasInput) !== 0;
                                };
                                $scope.hasDropDown = function () {
                                    /*jslint bitwise: true */
                                    return ($scope.options.mode & Modes.hasDropDown) !== 0;
                                };
                                // For multiselect only, current listed item
                                $scope.dropdown = {
                                    datepicker: null,
                                    context: null,
                                };
                                var dtopts = new vapkse.DatePickerOptions();
                                dtopts.parentElement = element;
                                dtopts.ondateselected = function (date) {
                                    /*jslint bitwise: true */
                                    if (($scope.options.mode & Modes.multiselect) !== 0) {
                                        var currents = this.currents;
                                        var current = currents.current;
                                        if (!$scope.options._selectedItem) {
                                            var newItem = new MultiSelect($scope.options.items);
                                            current = $scope.options.selectedItem(newItem);
                                        }
                                        if (currents && current && (currents.items === '[date]' || currents.items === '[date-range]')) {
                                            var dateItem;
                                            if (current.isDate) {
                                                if (date) {
                                                    // Change an existing date
                                                    current.value(date);
                                                    current.text($filter('date')(date, $scope.options.dateFormat));
                                                }
                                            }
                                            else if (date) {
                                                // Add a new date
                                                dateItem = new MSItem();
                                                dateItem.value(date.toString());
                                                dateItem.text($filter('date')(date, $scope.options.dateFormat));
                                                dateItem.isValue = true;
                                                dateItem.isDate = true;
                                                current._selectedItem = dateItem;
                                            }
                                            else {
                                                delete current._selectedItem;
                                            }
                                            if (currents.isAtEnd && current.items === '[date-range]') {
                                                var rangeOperators = $scope.options.rangeOperatorItems();
                                                rangeOperators[0].items = '[date]';
                                                dateItem._selectedItem = rangeOperators[0];
                                            }
                                            $scope.options.refresh(currents);
                                            this.close();
                                            if (currents.isAtEnd) {
                                                ensureMultiSelect();
                                            }
                                        }
                                        else {
                                            throw "invalid string format.";
                                        }
                                    }
                                    else {
                                        $scope.options.selectedItem(date);
                                        this.close();
                                    }
                                };
                                dtopts.onopening = function (e) {
                                    /*jslint bitwise: true */
                                    if (($scope.options.mode & Modes.multiselect) !== 0) {
                                        this.currents = $scope.options.getCurrents();
                                    }
                                    e.options = this;
                                    $scope.options.ondropdownopening(e);
                                };
                                dtopts.onopened = function () {
                                    var value;
                                    /*jslint bitwise: true */
                                    if (($scope.options.mode & Modes.multiselect) !== 0) {
                                        if (this.currents && !this.currents.isAtEnd && this.currents.current && this.currents.current.isDate) {
                                            value = this.currents.current.value();
                                        }
                                        element.addClass('multiselect-opened');
                                    }
                                    else {
                                        trySelectItem(selectElement().value());
                                        value = $scope.options.selectedItem();
                                        value = value && value.value();
                                        element.addClass('datepicker-opened');
                                    }
                                    if (value instanceof Date) {
                                        this.selectedDate(value);
                                    }
                                    $scope.options.ondropdownopened(this);
                                };
                                dtopts.onclosed = function () {
                                    selectElement().focus();
                                    /*jslint bitwise: true */
                                    if (($scope.options.mode & Modes.multiselect) !== 0) {
                                        element.removeClass('multiselect-opened');
                                    }
                                    else {
                                        element.removeClass('datepicker-opened');
                                    }
                                };
                                $scope.dtpoptions = dtopts;
                                var ctxopts = new vapkse.ContextMenuOptions();
                                ctxopts.autoSelect = false;
                                ctxopts.parentElement = element;
                                ctxopts.onitemclicked = function (selectedValue) {
                                    /*jslint bitwise: true */
                                    if (($scope.options.mode & Modes.multiselect) !== 0) {
                                        var currents = this.currents;
                                        var current = currents.current;
                                        if (!$scope.options._selectedItem) {
                                            var newItem = new MultiSelect($scope.options.items);
                                            current = $scope.options.selectedItem(newItem);
                                        }
                                        else if (!current) {
                                            current = $scope.options._selectedItem;
                                        }
                                        // Search for current object
                                        var currentItems = $scope.options.normalizedItems(currents.items);
                                        var selectedItem = $filter('filter')(currentItems, function (item) {
                                            return item.value() === selectedValue;
                                        })[0];
                                        if (currents.isAtEnd) {
                                            // add a new item
                                            current._selectedItem = selectedItem;
                                        }
                                        else {
                                            // Change an existing item
                                            current.value(selectedItem.value());
                                            current.text(selectedItem.text());
                                        }
                                        $scope.options.refresh(currents);
                                        this.close();
                                        if (!currents || currents.isAtEnd) {
                                            ensureMultiSelect();
                                        }
                                    }
                                    else {
                                        $scope.options.selectedItem(selectedValue);
                                        this.close();
                                    }
                                };
                                ctxopts.onopening = function (e) {
                                    /*jslint bitwise: true */
                                    if (($scope.options.mode & Modes.multiselect) !== 0) {
                                        this.currents = $scope.options.getCurrents();
                                    }
                                    e.options = this;
                                    $scope.options.ondropdownopening(e);
                                };
                                ctxopts.onopened = function () {
                                    /*jslint bitwise: true */
                                    if (($scope.options.mode & Modes.multiselect) !== 0) {
                                        if (this.selectedItem) {
                                            this.selectedItem(null);
                                        }
                                        element.addClass('multiselect-opened');
                                    }
                                    else {
                                        var item = $scope.options.selectedItem();
                                        if (item && item.value && this.selectedItem) {
                                            this.selectedItem(item.value());
                                        }
                                        element.addClass('combobox-opened');
                                    }
                                    $scope.options.ondropdownopened(this);
                                };
                                ctxopts.onclosed = function () {
                                    /*jslint bitwise: true */
                                    if (($scope.options.mode & Modes.multiselect) !== 0) {
                                        element.removeClass('multiselect-opened');
                                    }
                                    else {
                                        element.removeClass('combobox-opened');
                                    }
                                    selectElement().focus();
                                };
                                ctxopts.onitemsrequired = function (options) {
                                    /*jslint bitwise: true */
                                    if (($scope.options.mode & Modes.multiselect) !== 0) {
                                        var currents = $scope.options.getCurrents();
                                        if (!currents) {
                                            return false;
                                        }
                                        var current = currents.current;
                                        if (current && (current.isOpenParenthesis || current.isCloseParenthesis) && !currents.isAtEnd) {
                                            return false;
                                        }
                                        var msitems = $scope.options.normalizedItems(currents.items);
                                        options.items = currents.isAtEnd ? msitems : $filter('filter')(msitems, function (item) {
                                            if (typeof current.updatable === 'number' || typeof item.updatable === 'number') {
                                                return item.updatable === current.updatable;
                                            }
                                            else {
                                                return item.updatable;
                                            }
                                        });
                                    }
                                    else {
                                        var items = $scope.options.normalizedItems($scope.options.items);
                                        if (angular.equals(options.items, items)) {
                                            return false;
                                        }
                                        options.items = items;
                                        if (options.items.length === 1 && options.items[0] === waititem) {
                                            options.element.addClass("waiting");
                                        }
                                        else {
                                            options.element.removeClass("waiting");
                                        }
                                    }
                                    return true;
                                };
                                $scope.ctxoptions = ctxopts;
                                var onBlur = function () {
                                    isFocus(false);
                                };
                                var onFocus = function () {
                                    isFocus(true);
                                };
                                var isFocus = function (state) {
                                    if (state !== undefined) {
                                        if (!$scope.options.isReadOnly() && state) {
                                            element.addClass("multiselect-active");
                                        }
                                        else {
                                            element.removeClass("multiselect-active");
                                        }
                                    }
                                    return !$scope.options.isReadOnly() && selectElement().is(":focus") || $scope.options.dropDownIsVisible();
                                };
                                var selectElement = function () {
                                    var el = element.find(selectChoice);
                                    el.value = $scope.hasInput() ? el.val : el.html;
                                    return el;
                                };
                                var optionsWatcher = function (value) {
                                    $scope.options = (value || new Option());
                                    // Extend the current option instance with isolated attributes
                                    if (attrs.mode && $scope.options.mode === undefined) {
                                        $scope.options.mode = Modes[attrs.mode] || eval(attrs.mode);
                                    }
                                    var valueAttr = attrs.value;
                                    if (valueAttr && $scope.options.selectedValue === undefined) {
                                        $scope.options.selectedValue = $scope.$eval(valueAttr);
                                    }
                                    if (attrs.itemText && $scope.options.textAttribute === undefined) {
                                        $scope.options.textAttribute = attrs.itemText;
                                    }
                                    if (attrs.itemValue && $scope.options.valueAttribute === undefined) {
                                        $scope.options.valueAttribute = attrs.itemValue;
                                    }
                                    if (attrs.onitemselected && $scope.options.onitemselected === undefined) {
                                        $scope.options.onitemselected = $scope.$eval(attrs.onitemselected);
                                    }
                                    if (attrs.dateFormat && $scope.options.dateFormat === undefined) {
                                        $scope.options.dateFormat = attrs.dateFormat;
                                    }
                                    if (attrs.isreadonly && $scope.options._isReadOnly === undefined) {
                                        $scope.options._isReadOnly = attrs.isreadonly;
                                    }
                                    if (attrs.items && $scope.options.items === undefined) {
                                        $scope.options.items = $scope.$eval(attrs.items);
                                    }
                                    if (valueAttr) {
                                        $scope.options.onitemselected = function (item) {
                                            var model = $parse(valueAttr);
                                            model.assign($scope, item && item.value());
                                        };
                                        $scope.options.ontextchanged = function (text) {
                                            var model = $parse(valueAttr);
                                            model.assign($scope, text);
                                        };
                                    }
                                    // Extend the current option instance with default values
                                    delete $scope.options.selectedItem;
                                    angular.extend($scope.options, new Options(), angular.extend({}, $scope.options));
                                    selectChoice = $scope.hasInput() ? 'input' : '.placeHolder';
                                    $scope.options.items = $scope.options.normalizedItems($scope.options.items);
                                    $scope.dtpoptions.dateFormat = $scope.options.dateFormat;
                                    if ($scope.options._isReadOnly) {
                                        element.addClass('readonly');
                                    }
                                    else {
                                        element.removeClass('readonly');
                                    }
                                    delete $scope.options._isReadOnly;
                                    $scope.options.isReadOnly = function (value) {
                                        if (value !== undefined) {
                                            if (value) {
                                                element.addClass('readonly');
                                            }
                                            else {
                                                element.removeClass('readonly');
                                            }
                                        }
                                        else {
                                            return element.hasClass('readonly');
                                        }
                                    };
                                    $scope.options.dropDownIsVisible = function () {
                                        return ($scope.ctxoptions.isVisible && $scope.ctxoptions.isVisible()) || ($scope.dtpoptions.isVisible && $scope.dtpoptions.isVisible());
                                    };
                                    $scope.options.getCurrents = function () {
                                        var input = inputElement[0];
                                        if (input.selectionStart !== input.selectionEnd) {
                                            return null;
                                        }
                                        var selectedItem = $scope.options._selectedItem;
                                        if (selectedItem instanceof MultiSelect) {
                                            var multiselect = selectedItem;
                                            if (multiselect._freeText) {
                                                return null;
                                            }
                                            var currentItemInfos = multiselect.atPosition(input.selectionEnd);
                                            var currentItem = currentItemInfos && currentItemInfos.current;
                                            var previousItem = currentItem ? multiselect.previous(currentItem) : multiselect.last();
                                            var listItem = currentItem && previousItem || previousItem;
                                            var items;
                                            if (!listItem || (listItem && listItem.isOpenParenthesis) || (listItem.items && listItem.items.isParenthesis) || (!listItem.items && listItem.isOperator)) {
                                                items = $scope.options.items;
                                            }
                                            else if (!listItem.items) {
                                                items = $scope.options.andOrOperatorsItems();
                                            }
                                            else {
                                                items = listItem.items;
                                            }
                                            return {
                                                instance: multiselect,
                                                textBefore: currentItemInfos && currentItemInfos.textBefore || '',
                                                current: currentItem || previousItem,
                                                items: items,
                                                isLast: !currentItem || !currentItem._selectedItem,
                                                isAtEnd: !currentItem,
                                            };
                                        }
                                        else {
                                            return {
                                                instance: null,
                                                textBefore: '',
                                                current: null,
                                                items: $scope.options.items,
                                                isLast: true,
                                                isAtEnd: true,
                                            };
                                        }
                                    };
                                    $scope.options.refresh = function (currents) {
                                        var item = (currents && currents.instance) || $scope.options.selectedItem();
                                        var text = item.tostring();
                                        selectElement().value(text);
                                        $scope.options.selectedtext = text;
                                    };
                                    var cssClass = $scope.options.formatSelectionCssClass(element);
                                    if (cssClass !== undefined) {
                                        element.addClass(cssClass);
                                    }
                                    if ($scope.options.selectedValue !== undefined) {
                                        delete $scope.options._selectedItem;
                                        // Force event
                                        $scope.options.selectedItem($scope.options.selectedValue);
                                        delete $scope.options.selectedValue;
                                    }
                                    else if ($scope.options._queryString !== undefined) {
                                        $scope.options.selectedItem(MultiSelect.parse($scope.options, $scope.options._queryString, true));
                                        delete $scope.options._queryString;
                                    }
                                    element.off('blur', selectChoice, onBlur);
                                    element.off('focus', selectChoice, onFocus);
                                    element.off('focus', $scope.focus);
                                    element.on('blur', selectChoice, onBlur);
                                    element.on('focus', selectChoice, onFocus);
                                    element.on('focus', $scope.focus);
                                    /*jslint bitwise: true */
                                    if (!$scope.dropdown.datepicker && (($scope.options.mode & Modes.datePicker) !== 0 || ($scope.options.mode & Modes.multiselect) !== 0)) {
                                        var dropdownHtml = '<datepicker class="multiselect-dropdown" options="dtpoptions"></datepicker>';
                                        var dropdown = angular.element(dropdownHtml);
                                        $compile(dropdown)($scope);
                                        $('body').append(dropdown);
                                        $scope.dropdown.datepicker = dropdown;
                                    }
                                    /*jslint bitwise: true */
                                    if (!$scope.dropdown.context && ($scope.options.mode & Modes.datePicker) === 0) {
                                        var dropdownHtml2 = '<contextmenu class="multiselect-dropdown" options="ctxoptions"></contextmenu>';
                                        var dropdown2 = angular.element(dropdownHtml2);
                                        $compile(dropdown2)($scope);
                                        $('body').append(dropdown2);
                                        $scope.dropdown.context = dropdown2;
                                    }
                                    if ($scope.options.dropDownIsVisible()) {
                                        closeDropDown(true);
                                        toggleDropDown();
                                    }
                                };
                                if (attrs.options) {
                                    $scope.$parent.$watch(attrs.options, optionsWatcher);
                                }
                                $scope.focus = function () {
                                    selectElement().focus(100);
                                };
                                var trySelectItem = function (text) {
                                    return $scope.options.selectedItem(text);
                                };
                                var getMeasureElement = function () {
                                    var tmElement = element.find("#textmeasure");
                                    if (!tmElement.length) {
                                        tmElement = angular.element('<div id="textmeasure"></div>');
                                        element.append(tmElement);
                                    }
                                    return tmElement;
                                };
                                var toggleDropDown = function () {
                                    if ($scope.options.dropDownIsVisible()) {
                                        closeDropDown();
                                    } /*jslint bitwise: true */
                                    else if (($scope.options.mode & Modes.datePicker) !== 0) {
                                        $scope.dtpoptions.open();
                                    } /*jslint bitwise: true */
                                    else if (($scope.options.mode & Modes.multiselect) === 0) {
                                        if ($scope.options.onitemsrequired) {
                                            if ($scope.options.onitemsrequired($scope.options, function (options) {
                                                // By callback
                                                if (options) {
                                                    optionsWatcher(options);
                                                }
                                                $scope.ctxoptions.open();
                                            })) {
                                                // Open directly
                                                $scope.ctxoptions.open();
                                            }
                                        }
                                        else {
                                            $scope.ctxoptions.open();
                                        }
                                    }
                                    else {
                                        // Multi select
                                        var currents = $scope.options.getCurrents();
                                        if (!currents) {
                                            return;
                                        }
                                        if (currents.items === '[value]' || currents.items === '[value-range]') {
                                            // Waiting for text
                                            return;
                                        }
                                        // Calc left menu position
                                        var leftPos = 0;
                                        try {
                                            var textmeasure = getMeasureElement();
                                            var previousText;
                                            if (currents.isAtEnd) {
                                                previousText = $scope.options.selectedtext;
                                                if (previousText && previousText.trim()) {
                                                    textmeasure.html(previousText);
                                                    leftPos = textmeasure.width();
                                                }
                                                leftPos = Math.max(leftPos + 8, 13);
                                            }
                                            else if (currents.current && currents.current.position && currents.current.position.start < 2) {
                                                leftPos = 5;
                                            }
                                            else {
                                                previousText = currents.textBefore;
                                                if (previousText) {
                                                    textmeasure.html(previousText);
                                                    leftPos = textmeasure.width();
                                                }
                                                leftPos = Math.max(lastClickedPosition.x - leftPos + 4, 5);
                                            }
                                        }
                                        catch (e) {
                                            console.log("Error at MultiSelect.toggleDropDown: fail to measure text size, " + e.toString());
                                            leftPos = 0;
                                        }
                                        var dropDownWithin = leftPos < element.width() / 2 ? null : $scope.options.dropDownWithin;
                                        if (currents.items === '[date]' || currents.items === '[date-range]') {
                                            // Open calendar
                                            $scope.dtpoptions.left = leftPos;
                                            $scope.dtpoptions.position.within = dropDownWithin;
                                            $scope.dtpoptions.open();
                                            return;
                                        }
                                        else {
                                            $scope.ctxoptions.left = leftPos;
                                            $scope.ctxoptions.position.within = dropDownWithin;
                                            if ($scope.options.onitemsrequired) {
                                                if ($scope.options.onitemsrequired($scope.options, function (options) {
                                                    // By callback
                                                    if (options) {
                                                        optionsWatcher(options);
                                                    }
                                                    $scope.ctxoptions.open();
                                                }, currents)) {
                                                    // Open directly
                                                    $scope.ctxoptions.open();
                                                }
                                            }
                                            else {
                                                $scope.ctxoptions.open();
                                            }
                                        }
                                    }
                                };
                                var closeDropDown = function (immediate) {
                                    $scope.dtpoptions.close(immediate);
                                    $scope.ctxoptions.close(immediate);
                                };
                                element.on('click', function (e) {
                                    if ($scope.options.isReadOnly()) {
                                        return;
                                    }
                                    /*jslint bitwise: true */
                                    if (($scope.options.mode & Modes.multiselect) !== 0) {
                                        if (selectElement().is($(e.target))) {
                                            ensureMultiSelect('click');
                                        }
                                    }
                                    else if (!selectElement().is($(e.target)) || !$scope.hasInput()) {
                                        toggleDropDown();
                                    }
                                    selectElement().focus(100);
                                });
                                var clearMultiSelecteParserTimer = function () {
                                    if (!multiSelecteParserTimer) {
                                        return;
                                    }
                                    $timeout.cancel(multiSelecteParserTimer);
                                    multiSelecteParserTimer = null;
                                };
                                var inputElement = element.find('input');
                                inputElement.on('dblclick', function () {
                                    /*jslint bitwise: true */
                                    if (($scope.options.mode & Modes.multiselect) !== 0) {
                                        clearMultiSelecteParserTimer();
                                        closeDropDown();
                                    }
                                });
                                inputElement.on('mouseup', function (e) {
                                    /* globals graphics */
                                    lastClickedPosition = new graphics.Position(e.clientX - inputElement.offset().left, e.clientY - inputElement.offset().top);
                                    /*jslint bitwise: true */
                                    if (($scope.options.mode & Modes.multiselect) !== 0 && e.clientX > $(this).width() - 20) {
                                        // Delete button is pressed
                                        closeDropDown();
                                    }
                                });
                                inputElement.on('keyup', function (e) {
                                    switch (e.keyCode) {
                                        case $.ui.keyCode.BACKSPACE:
                                            /*jslint bitwise: true */
                                            if (($scope.options.mode & Modes.multiselect) !== 0 && !$scope.options.dropDownIsVisible()) {
                                                ensureMultiSelect($.ui.keyCode.BACKSPACE);
                                            }
                                            break;
                                    }
                                });
                                inputElement.on('keydown', function (e) {
                                    if ($scope.options.isReadOnly()) {
                                        return;
                                    }
                                    switch (e.keyCode) {
                                        case $.ui.keyCode.BACKSPACE:
                                            /*jslint bitwise: true */
                                            if (($scope.options.mode & Modes.multiselect) !== 0) {
                                                var currents = $scope.options.getCurrents();
                                                var lastitem = currents && currents.isLast && currents.current;
                                                var text = selectElement().value();
                                                if (lastitem && text) {
                                                    if (!lastitem.isValue && lastitem.text) {
                                                        if (text.match("\"$")) {
                                                            text = text.substring(0, text.length - 1);
                                                        }
                                                        var searchText = lastitem.text();
                                                        if (searchText.length === 1) {
                                                            searchText = '\\' + searchText;
                                                        }
                                                        if (text.match(searchText + "[\"\']*$")) {
                                                            var selectedItem = $scope.options._selectedItem;
                                                            var previousItem = currents.instance.previous(lastitem);
                                                            if (previousItem) {
                                                                delete previousItem._selectedItem;
                                                                delete previousItem.filteredItems;
                                                            }
                                                            else {
                                                                delete selectedItem._selectedItem;
                                                                delete selectedItem.filteredItems;
                                                            }
                                                            $scope.options.refresh(currents);
                                                            closeDropDown(true);
                                                            if (!previousItem.isValue) {
                                                                ensureMultiSelect();
                                                            }
                                                            e.stopPropagation();
                                                        }
                                                    }
                                                    else {
                                                        closeDropDown();
                                                    }
                                                }
                                            }
                                            break;
                                        case $.ui.keyCode.ESCAPE:
                                            // Prevent escape to cancel the text of the input
                                            closeDropDown();
                                            this.blur();
                                            this.focus();
                                            e.stopPropagation();
                                            break;
                                    }
                                });
                                inputElement.on('change', function () {
                                    var item = trySelectItem(selectElement().value());
                                    /*jslint bitwise: true */
                                    if (!item && ($scope.options.mode & Modes.freeText) !== 0) {
                                        $scope.options.ontextchanged(selectElement().value());
                                    }
                                });
                                inputElement.on('cut', function () {
                                    /*jslint bitwise: true */
                                    if (($scope.options.mode & Modes.multiselect) !== 0) {
                                        closeDropDown();
                                    }
                                });
                                inputElement.on('paste', function () {
                                    /*jslint bitwise: true */
                                    if (($scope.options.mode & Modes.multiselect) !== 0) {
                                        closeDropDown();
                                    }
                                });
                                var ensureMultiSelect = function (code) {
                                    if (code === $.ui.keyCode.ENTER) {
                                        return;
                                    }
                                    clearMultiSelecteParserTimer();
                                    var delay = 20;
                                    if (code === $.ui.keyCode.ESCAPE) {
                                        closeDropDown();
                                        return;
                                    }
                                    else if (code === 'click') {
                                        // Waiting for double click
                                        delay = 200;
                                        code = null;
                                    }
                                    // Ensure the scope is digest and selectedText is up to date
                                    multiSelecteParserTimer = $timeout(function () {
                                        if (!$scope.options.selectedtext) {
                                            if ($scope.options._selectedItem) {
                                                delete $scope.options._selectedItem;
                                            }
                                            if (code === $.ui.keyCode.BACKSPACE) {
                                                closeDropDown(true);
                                            }
                                            toggleDropDown();
                                        }
                                        else {
                                            var item = MultiSelect.parse($scope.options, $scope.options.selectedtext);
                                            if (item && item.items && item.items.length && !item.inquote) {
                                                $scope.options._selectedItem = item;
                                                var currents = $scope.options.getCurrents();
                                                if (currents) {
                                                    if (!code || currents.isAtEnd && code === $.ui.keyCode.SPACE || code === 40 || code === 41) {
                                                        if (currents.isAtEnd && currents.items instanceof Array && currents.items.length === 1) {
                                                            var msItem = currents.items[0];
                                                            if (msItem.isOperator && msItem.items) {
                                                                // Auto select the unique operator
                                                                currents.current._selectedItem = msItem;
                                                                // Current is modified, just refresh
                                                                $scope.options.refresh(currents);
                                                                // Move the cursor to the end
                                                                var input = inputElement[0];
                                                                input.selectionStart = input.selectionEnd = $scope.options.selectedtext.length;
                                                            }
                                                        }
                                                        closeDropDown(true);
                                                        toggleDropDown();
                                                    }
                                                    // Auto select the first item containing the additional text if any
                                                    var filteredItems = currents.current.filteredItems;
                                                    if (filteredItems && filteredItems.length && $scope.ctxoptions.isVisible()) {
                                                        var selectedDropDownValue = $scope.ctxoptions.selectedItem();
                                                        selectedDropDownValue = selectedDropDownValue && selectedDropDownValue.value();
                                                        if (!selectedDropDownValue || $filter('filter')(filteredItems, function (item) {
                                                            return item.value() === selectedDropDownValue;
                                                        }).length === 0) {
                                                            $scope.ctxoptions.selectedItem(filteredItems[0].value());
                                                            filteredItems.currentIndex = 0;
                                                        }
                                                    }
                                                }
                                            }
                                            else if (item._freeText) {
                                                $scope.options.selectedItem(item);
                                                closeDropDown();
                                            }
                                        }
                                    }, delay);
                                };
                                inputElement.on('keypress', function (e) {
                                    /*jslint bitwise: true */
                                    if (($scope.options.mode & Modes.multiselect) !== 0) {
                                        ensureMultiSelect(e.keyCode || e.charCode);
                                    }
                                });
                                inputElement.on('keyup', function (e) {
                                    var input = inputElement[0];
                                    /*jslint bitwise: true */
                                    if (($scope.options.mode & Modes.multiselect) !== 0) {
                                        switch (e.keyCode) {
                                            case $.ui.keyCode.PAGE_UP:
                                            case $.ui.keyCode.UP:
                                            case $.ui.keyCode.PAGE_DOWN:
                                            case $.ui.keyCode.DOWN:
                                                var currents = $scope.options.getCurrents();
                                                if (e.altKey && (!currents || currents.isAtEnd)) {
                                                    toggleDropDown();
                                                    e.stopPropagation();
                                                }
                                                else if (e.ctrlKey && $scope.options.dropDownIsVisible()) {
                                                    input.setSelectionRange(32767, 32767);
                                                    currents = $scope.options.getCurrents();
                                                    if (currents) {
                                                        var filteredItems = currents.current && currents.current.filteredItems;
                                                        if (filteredItems && filteredItems.length) {
                                                            var currentIndex;
                                                            if (e.keyCode === $.ui.keyCode.PAGE_UP || e.keyCode === $.ui.keyCode.UP) {
                                                                currentIndex = isNaN(filteredItems.currentIndex) ? filteredItems.length : filteredItems.currentIndex;
                                                                if (--currentIndex < 0) {
                                                                    currentIndex = filteredItems.length - 1;
                                                                }
                                                            }
                                                            else {
                                                                currentIndex = isNaN(filteredItems.currentIndex) ? -1 : filteredItems.currentIndex;
                                                                if (++currentIndex >= filteredItems.length) {
                                                                    currentIndex = 0;
                                                                }
                                                            }
                                                            $scope.ctxoptions.selectedItem(filteredItems[currentIndex].value());
                                                            filteredItems.currentIndex = currentIndex;
                                                        }
                                                    }
                                                    $timeout(function () {
                                                        input.setSelectionRange(32767, 32767);
                                                    }, 200);
                                                }
                                        }
                                    }
                                });
                                element.on('keydown', function (e) {
                                    if ($scope.options && $scope.options.onkeydown) {
                                        $scope.options.onkeydown(e);
                                    }
                                    if ($scope.options.isReadOnly() || e.isPropagationStopped()) {
                                        return;
                                    }
                                    function escape(value) {
                                        return value.replace(/[\-\[\]{}()*+?.,\\\^$|#\s]/g, "\\$&");
                                    }
                                    /*jslint bitwise: true */
                                    if (($scope.options.mode & Modes.multiselect) !== 0) {
                                        return;
                                    }
                                    switch (e.keyCode) {
                                        case $.ui.keyCode.HOME:
                                            if ($scope.options.dropDownIsVisible()) {
                                                return;
                                            }
                                            $scope.options.selectedItem($scope.options.first());
                                            break;
                                        case $.ui.keyCode.END:
                                            if ($scope.options.dropDownIsVisible()) {
                                                return;
                                            }
                                            $scope.options.selectedItem($scope.options.last());
                                            break;
                                        case $.ui.keyCode.PAGE_UP:
                                        case $.ui.keyCode.UP:
                                            if (e.altKey) {
                                                toggleDropDown();
                                            }
                                            else if ($scope.options.dropDownIsVisible()) {
                                                return;
                                            } /*jslint bitwise: true */
                                            else if (($scope.options.mode & Modes.datePicker) !== 0) {
                                                var date = $scope.options.selectedItem() || new Date();
                                                if (e.ctrlKey) {
                                                    date.setFullYear(date.getFullYear() + 1);
                                                }
                                                else if (e.shiftKey) {
                                                    date.setMonth(date.getMonth() + 1);
                                                }
                                                else {
                                                    date.setDate(date.getDate() + 1);
                                                }
                                                $scope.options.selectedItem(date);
                                            }
                                            else {
                                                $scope.options.selectedItem($scope.options.prev());
                                            }
                                            break;
                                        case $.ui.keyCode.PAGE_DOWN:
                                        case $.ui.keyCode.DOWN:
                                            if (e.altKey) {
                                                toggleDropDown();
                                            }
                                            else if ($scope.options.dropDownIsVisible()) {
                                                return;
                                            } /*jslint bitwise: true */
                                            else if (($scope.options.mode & Modes.datePicker) !== 0) {
                                                var date2 = $scope.options.selectedItem() || new Date();
                                                if (e.ctrlKey) {
                                                    date2.setFullYear(date2.getFullYear() - 1);
                                                }
                                                else if (e.shiftKey) {
                                                    date2.setMonth(date2.getMonth() - 1);
                                                }
                                                else {
                                                    date2.setDate(date2.getDate() - 1);
                                                }
                                                $scope.options.selectedItem(date2);
                                            }
                                            else {
                                                $scope.options.selectedItem($scope.options.next());
                                            }
                                            break;
                                        default:
                                            /*jslint bitwise: true */
                                            if (($scope.options.mode & Modes.autoComplete) !== 0) {
                                                // Autocomplete
                                                $timeout(function () {
                                                    var text = selectElement().value();
                                                    if (!text) {
                                                        return;
                                                    }
                                                    var regex = new RegExp(escape(text), "i");
                                                    var match = $filter('filter')($scope.options.items, function (value) {
                                                        value.visible = regex.test(value.text());
                                                        return value.visible;
                                                    });
                                                    if (match.length) {
                                                        $scope.ctxoptions.selectedItem(null);
                                                        $scope.ctxoptions.open();
                                                        $scope.ctxoptions.refresh();
                                                    }
                                                    else {
                                                        $scope.ctxoptions.close();
                                                    }
                                                }, 100);
                                            }
                                            else if (!$scope.hasInput()) {
                                                var previousText = $scope.options.currentFilter && $scope.options.currentFilter.text || "";
                                                var currentText = String.fromCharCode(e.keyCode);
                                                var selectedItem = $scope.options.selectedItem();
                                                $timeout.cancel(this.filterTimer);
                                                if (currentText === previousText) {
                                                }
                                                else {
                                                    // New Match
                                                    currentText = previousText + currentText;
                                                    var regex = new RegExp("^" + escape(currentText), "i");
                                                    $scope.options.currentFilter = $filter('filter')($scope.options.items, function (item) {
                                                        return regex.test(item.text());
                                                    });
                                                    $scope.options.currentFilter.text = currentText;
                                                }
                                                // If no matches on the current filter, reset to the last character pressed
                                                // to move down the menu to the first item that starts with that character
                                                if (!$scope.options.currentFilter.length) {
                                                    currentText = String.fromCharCode(e.keyCode);
                                                    var regex2 = new RegExp("^" + escape(currentText), "i");
                                                    $scope.options.currentFilter = $filter('filter')($scope.options.items, function (item) {
                                                        return regex2.test(item.text());
                                                    });
                                                    $scope.options.currentFilter.text = currentText;
                                                }
                                                if ($scope.options.currentFilter.length) {
                                                    var currentIndex = selectedItem ? $scope.options.currentFilter.indexOf(selectedItem) : -1;
                                                    if (++currentIndex >= $scope.options.currentFilter.length) {
                                                        currentIndex = 0;
                                                    }
                                                    selectedItem = $scope.options.selectedItem($scope.options.currentFilter[currentIndex]);
                                                    if ($scope.ctxoptions.isVisible()) {
                                                        $scope.ctxoptions.selectedItem(selectedItem.value());
                                                    }
                                                    if ($scope.options.currentFilter.length > 1) {
                                                        this.filterTimer = $timeout(function () {
                                                            delete $scope.options.currentFilter;
                                                        }, 1000);
                                                    }
                                                    else {
                                                        delete $scope.options.currentFilter;
                                                    }
                                                }
                                                else {
                                                    delete $scope.options.currentFilter;
                                                }
                                                return;
                                            }
                                    }
                                    e.stopPropagation();
                                });
                                element.on('mousewheel', function (e) {
                                    if ($scope.options.isReadOnly()) {
                                        return;
                                    }
                                    var item = !$scope.ctxoptions.isVisible() && e.originalEvent.wheelDelta > 0 ? $scope.options.prev() : $scope.options.next();
                                    if (item) {
                                        $scope.options.selectedItem(item);
                                        selectElement().select();
                                    }
                                });
                                optionsWatcher(null);
                            }
                        };
                    },
                };
            }
            return MultiSelectDirective;
        }());
        var MSItem = (function (_super) {
            __extends(MSItem, _super);
            function MSItem() {
                _super.apply(this, arguments);
                this.isOpenParenthesis = false;
                this.isCloseParenthesis = false;
                this.isOperator = false;
                this.isValue = false;
                this.isDate = false;
            }
            return MSItem;
        }(vapkse.DropDownItem));
        multiselect_1.MSItem = MSItem;
        // Complexe object for the selected item in multiselect mode
        var MultiSelect = (function (_super) {
            __extends(MultiSelect, _super);
            function MultiSelect(items) {
                _super.call(this);
                this.tostring = function () {
                    var that = this;
                    if (that._freeText) {
                        return that._freeText;
                    }
                    var stack = [];
                    var currentItem = that._selectedItem;
                    var additionalText = that.filteredItems && that.filteredItems.additionalText;
                    var currentPosition = 0;
                    var lastPosition = 0;
                    while (currentItem) {
                        // Add the item text
                        var text = currentItem.tostring && currentItem.tostring() || currentItem.toString();
                        stack.push(text);
                        currentItem.position = {
                            start: lastPosition,
                            end: currentPosition + text.length,
                        };
                        currentPosition = currentItem.position.end;
                        lastPosition = currentPosition;
                        // Add a space
                        stack.push(' ');
                        currentPosition++;
                        // Get the additional text of the last item
                        additionalText = currentItem.filteredItems ? currentItem.filteredItems.additionalText : '';
                        currentItem = currentItem._selectedItem;
                    }
                    // Add the additional text of the last item
                    stack.push(additionalText);
                    return stack.join('');
                };
                this.toQueryString = function () {
                    var that = this;
                    if (that._freeText) {
                        return that._freeText.indexOf(' ') ? '"' + that._freeText + '"' : that._freeText;
                    }
                    var text = [];
                    var currentItem = that._selectedItem;
                    while (currentItem) {
                        var value = currentItem.value();
                        var txt = value instanceof Date ? value.toISOString() : value && value.toString() || currentItem.toString();
                        var hasSpace = txt.indexOf(' ') > 0;
                        if (hasSpace) {
                            text.push('"');
                        }
                        text.push(txt);
                        if (hasSpace) {
                            text.push('"');
                        }
                        text.push(' ');
                        currentItem = currentItem._selectedItem;
                    }
                    return text.join('');
                };
                this.last = function () {
                    var that = this;
                    var current = that._selectedItem;
                    while (current && current._selectedItem) {
                        current = current._selectedItem;
                    }
                    return current || that;
                };
                this.previous = function (item) {
                    var that = this;
                    var current = that._selectedItem !== item && that._selectedItem;
                    while (current && current._selectedItem && current._selectedItem !== item) {
                        current = current._selectedItem;
                    }
                    return current;
                };
                this.atPosition = function (position) {
                    var that = this;
                    var current = that._selectedItem;
                    if (!current || !current.position) {
                        that.tostring();
                    }
                    while (current) {
                        if (current.position && current.position.start <= position && current.position.end > position) {
                            return {
                                current: current,
                                textBefore: current.tostring().substr(0, position - current.position.start - 1)
                            };
                        }
                        current = current._selectedItem;
                    }
                    return null;
                };
                this.items = items;
            }
            MultiSelect.equals = function (item1, item2) {
                if (!item1 && !item2) {
                    return true;
                }
                else if (item1 ? !item2 : item2) {
                    return false;
                }
                else if (item1._freeText !== item2._freeText) {
                    return false;
                }
                var current1 = item1._selectedItem;
                var current2 = item2._selectedItem;
                while (current1 && current2) {
                    if (!angular.equals(current1.value(), current2.value()) || current1.text() !== current2.text()) {
                        return false;
                    }
                    current1 = current1._selectedItem;
                    current2 = current2._selectedItem;
                }
                return !current1 && !current2;
            };
            MultiSelect.parse = function (options, text, isQueryString) {
                try {
                    var items = options.normalizedItems(options.items);
                    var item = new MultiSelect(items);
                    var currentItem;
                    var openParenthesis = options.openParenthesisItems()[0];
                    var closeParenthesis = options.closeParenthesisItems()[0];
                    if (text) {
                        var validateSelection = function (currentItem, displayName, isQueryString) {
                            var selectedItem;
                            var msItem;
                            var currentItems;
                            var filteredItems;
                            if (currentItem.items === '[date]' || currentItem.items === '[date-range]') {
                                // Try to parse
                                var date;
                                var text;
                                try {
                                    date = $dateParser(displayName, isQueryString ? '' : options.dateFormat);
                                    text = isQueryString ? $filter('date')(date, options.dateFormat) : displayName;
                                }
                                catch (e) {
                                    return null;
                                }
                                var dateItem = angular.extend({}, new Item());
                                dateItem.value(date);
                                dateItem.text(text);
                                dateItem.isValue = true;
                                dateItem.isDate = true;
                                selectedItem = dateItem;
                                if (currentItem.items === '[date-range]') {
                                    selectedItem.items = options.rangeOperatorItems();
                                    msItem = selectedItem.items[0];
                                    msItem.items = '[date]';
                                }
                            }
                            else if (currentItem.items === '[value]' || currentItem.items === '[value-range]') {
                                selectedItem = options.normalizedItems([displayName])[0];
                                selectedItem.isValue = true;
                                if (currentItem.items === '[value-range]') {
                                    selectedItem.items = options.rangeOperatorItems();
                                    msItem = selectedItem.items[0];
                                    msItem.items = '[value]';
                                }
                            }
                            else if (currentItem.isOperator || currentItem.isOpenParenthesis) {
                                // No current item, root list is displayed
                                currentItems = options.normalizedItems(options.items);
                            }
                            else if (!currentItem.items) {
                                currentItems = options.andOrOperatorsItems();
                            }
                            else {
                                currentItems = options.normalizedItems(currentItem.items);
                            }
                            if (currentItems) {
                                var matchItems;
                                if (isQueryString) {
                                    matchItems = $filter('filter')(currentItems, function (item) {
                                        return item.value() === displayName;
                                    });
                                }
                                else {
                                    matchItems = $filter('filter')(currentItems, function (item) {
                                        return item.text().toLowerCase() === displayName.toLowerCase();
                                    });
                                    if (!matchItems.length) {
                                        filteredItems = $filter('filter')(currentItems, function (item) {
                                            return item.text().toLowerCase().indexOf(displayName.toLowerCase()) === 0;
                                        });
                                        filteredItems.additionalText = filteredItems.length > 0 ? displayName : '';
                                    }
                                }
                                selectedItem = matchItems.length > 0 ? matchItems[0] : null;
                            }
                            if (selectedItem) {
                                currentItem._selectedItem = selectedItem;
                            }
                            currentItem.filteredItems = filteredItems;
                            return selectedItem;
                        };
                        var createFreeTextItem = function (text) {
                            var item = new MultiSelect();
                            item._freeText = text;
                            return item;
                        };
                        item.inquote = undefined;
                        var currentWord = [];
                        var ensureNextChar = false;
                        angular.forEach(text, function (chr) {
                            if (currentItem) {
                                if (ensureNextChar) {
                                    currentWord.push(chr);
                                    ensureNextChar = false;
                                }
                                else if (!item.inquote && chr === openParenthesis.text()) {
                                    // Validate current word
                                    if (currentWord.length) {
                                        currentItem = validateSelection(currentItem, currentWord.join(''), isQueryString);
                                        currentWord = [];
                                    }
                                    currentItem.items = options.openParenthesisItems();
                                    currentItem._selectedItem = currentItem.items[0];
                                    currentItem = currentItem._selectedItem;
                                }
                                else if (!item.inquote && chr === closeParenthesis.text()) {
                                    if (currentWord.length) {
                                        currentItem = validateSelection(currentItem, currentWord.join(''), isQueryString);
                                        currentWord = [];
                                    }
                                    currentItem.items = options.closeParenthesisItems();
                                    currentItem._selectedItem = currentItem.items[0];
                                    currentItem = currentItem._selectedItem;
                                }
                                else if (item.inquote && item.inquote === chr) {
                                    item.inquote = undefined;
                                }
                                else if (chr === '"' || chr === "'") {
                                    item.inquote = chr;
                                }
                                else if (chr === '\\') {
                                    currentWord.push(chr);
                                    ensureNextChar = true;
                                }
                                else if (chr !== ' ' || item.inquote) {
                                    currentWord.push(chr);
                                }
                                else if (currentWord.length) {
                                    // Validate current word
                                    currentItem = validateSelection(currentItem, currentWord.join(''), isQueryString);
                                    currentWord = [];
                                }
                            }
                        });
                        var lastItem = currentItem;
                        if (lastItem && currentWord.length) {
                            // Validate last word
                            lastItem = validateSelection(lastItem, currentWord.join(''), isQueryString);
                            currentItem.position = {
                                start: text.length - currentWord.length,
                                end: text.length - 1,
                            };
                        }
                        if (!lastItem) {
                            var regex = new RegExp("^\".*\"$", "i");
                            if (regex.test(text)) {
                                return MultiSelect.parse(options, text.substr(1, text.length - 2), isQueryString);
                            }
                            else if (currentItem && currentItem.filteredItems && currentItem.filteredItems.length > 0) {
                            }
                            else {
                                return createFreeTextItem(text);
                            }
                        }
                    }
                    return item;
                }
                catch (e) {
                    console.log("Error at MultiSelect.parse: fail to parse text, " + e.toString());
                    return createFreeTextItem(text);
                }
            };
            return MultiSelect;
        }(MSItem));
        multiselect_1.MultiSelect = MultiSelect;
    })(multiselect = vapkse.multiselect || (vapkse.multiselect = {}));
})(vapkse || (vapkse = {}));
