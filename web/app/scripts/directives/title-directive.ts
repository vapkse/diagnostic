/// <reference path="../../../typings/angularjs/angular.d.ts" />

'use strict';

module dampApp {
    define(['app'], function(app: Routage.IModule) {
        app.register.directive('titlebox', ($timeout: ng.ITimeoutService, config: Routage.IRoutageConfig) => new TitleDirective($timeout, config));
    });
    
    interface localScope extends ng.IScope {
        folded(value: boolean, userclick: boolean): boolean;
        toggle(value: boolean) : boolean;
        onFoldChanged(value: boolean, userclick: boolean): void;
        externalFolded: boolean;
        $$folded: boolean;
        foldElement: string;
    }
    
    export class TitleDirective implements ng.IDirective {
        constructor($timeout: ng.ITimeoutService, config: Routage.IRoutageConfig) {
            return {
                restrict: 'E',
                templateUrl: config.route.templates['titleTemplate'],
                replace: true,
                scope: {
                    text: '@',
                    foldElement: '@',
                    onFoldChanged: '=',
                    externalFolded: '=folded'
                },                
                compile: function() {
                    return {
                        pre: function(scope: ng.IScope, element: ng.IAugmentedJQuery) {
                            var $scope = <localScope>scope;
                            $scope.$watch('externalFolded', function() {
                                $scope.folded($scope.externalFolded, false);
                            });
        
                            $scope.toggle = function(userclick: boolean) {
                                return $scope.folded(!$scope.$$folded, userclick);
                            };
        
                            $scope.folded = function(value: boolean, userclick: boolean) {
                                if (value === undefined) {
                                    return $scope.$$folded;
                                }
        
                                if ($scope.foldElement) {
                                    $timeout(function() {
                                        var $element = $('#' + $scope.foldElement);
                                        $element.addClass('titlebox-content');
                                        if (value) {
                                            element.addClass('titlebox-folded');
                                            $element.addClass('titlebox-folded');
                                        }
                                        else {
                                            element.removeClass('titlebox-folded');
                                            $element.removeClass('titlebox-folded');
                                        }
                                    }, 1);
                                }
        
                                if ($scope.onFoldChanged) {
                                    $scope.onFoldChanged(value, userclick);
                                }
        
                                $scope.$$folded = value;
                                return value;
                            };
        
                            $scope.folded(true, false);
                        }        
                    };
                }
            }
        }
    }
}
