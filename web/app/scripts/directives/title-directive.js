/// <reference path="../../../typings/angularjs/angular.d.ts" />
'use strict';
var dampApp;
(function (dampApp) {
    define(['app'], function (app) {
        app.register.directive('titlebox', function ($timeout, config) { return new TitleDirective($timeout, config); });
    });
    var TitleDirective = (function () {
        function TitleDirective($timeout, config) {
            return {
                restrict: 'E',
                templateUrl: config.route.templates['titleTemplate'],
                replace: true,
                scope: {
                    text: '@',
                    foldElement: '@',
                    onFoldChanged: '=',
                    externalFolded: '=folded'
                },
                compile: function () {
                    return {
                        pre: function (scope, element) {
                            var $scope = scope;
                            $scope.$watch('externalFolded', function () {
                                $scope.folded($scope.externalFolded, false);
                            });
                            $scope.toggle = function (userclick) {
                                return $scope.folded(!$scope.$$folded, userclick);
                            };
                            $scope.folded = function (value, userclick) {
                                if (value === undefined) {
                                    return $scope.$$folded;
                                }
                                if ($scope.foldElement) {
                                    $timeout(function () {
                                        var $element = $('#' + $scope.foldElement);
                                        $element.addClass('titlebox-content');
                                        if (value) {
                                            element.addClass('titlebox-folded');
                                            $element.addClass('titlebox-folded');
                                        }
                                        else {
                                            element.removeClass('titlebox-folded');
                                            $element.removeClass('titlebox-folded');
                                        }
                                    }, 1);
                                }
                                if ($scope.onFoldChanged) {
                                    $scope.onFoldChanged(value, userclick);
                                }
                                $scope.$$folded = value;
                                return value;
                            };
                            $scope.folded(true, false);
                        }
                    };
                }
            };
        }
        return TitleDirective;
    }());
    dampApp.TitleDirective = TitleDirective;
})(dampApp || (dampApp = {}));
