/// <reference path="../../../typings/angularjs/angular.d.ts" />
declare module vapkse.multiselect {
    enum Modes {
        select = 1,
        autoComplete = 2,
        freeText = 4,
        datePicker = 8,
        multiselect = 16,
        hasInput = 30,
        hasDropDown = 13,
    }
    import Item = vapkse.DropDownItem;
    class Options {
        mode: Modes;
        items: Item[];
        _selectedItem: Item;
        _queryString: string;
        selectedValue: string;
        selectedtext: string;
        textAttribute: string;
        valueAttribute: string;
        dateFormat: string;
        dropDownWithin: string;
        element: JQuery;
        formatSelectionCssClass: (element: JQuery) => string;
        _isReadOnly: boolean;
        isReadOnly: (value?: boolean) => any;
        visibleItems: () => Item[];
        normalizedItems: (items: Item[] | string[] | string) => Item[];
        andOrOperatorsItems: () => MSItems;
        rangeOperatorItems: () => MSItems;
        openParenthesisItems: () => MSItems;
        closeParenthesisItems: () => MSItems;
        next: (value?: Item | number) => any;
        prev: (value?: Item | number) => any;
        first: () => any;
        last: () => any;
        queryString: (value: string) => any;
        selectedItem: (value?: string | Date | number | Item) => any;
        getItemText: (value: string) => string;
        onitemselected: (item: Item) => void;
        ondropdownopening: (e: Event) => void;
        ondropdownopened: (that: any) => void;
        onkeydown: (e: JQueryKeyEventObject) => void;
        ontextchanged: (text: string) => void;
        onitemsrequired: Function;
    }
    interface filteredItems extends Array<Item> {
        currentIndex: number;
        additionalText: string;
    }
    interface MSItems extends Array<MSItem> {
        isParenthesis: boolean;
    }
    class MSItem extends DropDownItem {
        _selectedItem: MSItem;
        _freeText: string;
        items: MSItems | string;
        filteredItems: filteredItems;
        isOpenParenthesis: boolean;
        isCloseParenthesis: boolean;
        isOperator: boolean;
        updatable: any;
        position: position;
        isValue: boolean;
        isDate: boolean;
        inquote: string;
    }
    interface position {
        start: number;
        end: number;
    }
    class MultiSelect extends MSItem {
        constructor(items?: Array<Item>);
        tostring: () => string;
        toQueryString: () => string;
        last: () => MSItem;
        previous: (item: Item) => MSItem;
        atPosition: (position: number) => {
            current: MSItem;
            textBefore: string;
        };
        static equals: (item1: MSItem, item2: MSItem) => boolean;
        static parse: (options: Options, text: string, isQueryString?: boolean) => MSItem;
    }
}
