/// <reference path="../../../typings/angularjs/angular.d.ts" />
'use strict';
var vapkse;
(function (vapkse) {
    if (define) {
        define(['app', 'modalService'], function (app) {
            app.register.directive('datepicker', function ($document, $compile, $translate, $dateParser, modalService, $timeout, $filter) { return new DatePickerDirective($document, $compile, $translate, $dateParser, modalService, $timeout, $filter); });
        });
    }
    else {
        angular.module('vapkse').directive('datepicker', function ($document, $compile, $translate, $dateParser, modalService, $timeout, $filter) { return new DatePickerDirective($document, $compile, $translate, $dateParser, modalService, $timeout, $filter); });
    }
    var DatePickerOptions = (function () {
        function DatePickerOptions() {
            this.autoFocus = false;
            this.dateFormat = 'dd/MM/yy';
            this.position = {
                my: "left top",
                at: "left bottom",
                collision: "flipfit",
                within: "body"
            };
            this.selectedDate = function (date) {
                if (date !== undefined) {
                    this._selectedDate = date instanceof Date ? date : null;
                }
                return this._selectedDate;
            };
            this.close = function (value) { };
            this.open = $.noop;
            this.isVisible = function (value) {
                if (value !== undefined) {
                    if (value) {
                        this.open();
                    }
                    else {
                        this.close();
                    }
                }
                return this.element && this.element.is(":visible");
            };
            // Callbacks
            this.onopening = function (e) { };
            this.onopened = $.noop;
            this.onclosed = $.noop;
            this.onresized = $.noop;
            this.ondateselected = function (date) { };
        }
        return DatePickerOptions;
    }());
    vapkse.DatePickerOptions = DatePickerOptions;
    var DatePickerDirective = (function () {
        function DatePickerDirective($document, $compile, $translate, $dateParser, modalService, $timeout, $filter) {
            return {
                restrict: 'E',
                template: '<span class="datepicker transparent"></span>',
                replace: true,
                scope: true,
                compile: function () {
                    return {
                        pre: function (scope, element, attrs) {
                            var $scope = scope;
                            var backdropHtml = '<div class="datepicker-backdrop" ng-click="options.close()"></div>';
                            var backdrop = angular.element(backdropHtml);
                            $compile(backdrop)($scope);
                            $('body').append(backdrop);
                            backdrop.hide();
                            element.hide();
                            var that = this;
                            var onKeyDown = function (e) {
                                var date;
                                switch (e.keyCode) {
                                    case $.ui.keyCode.HOME:
                                        date = $scope.options.selectedDate();
                                        date.setDate(1);
                                        $scope.options.selectedDate(date);
                                        break;
                                    case $.ui.keyCode.END:
                                        date = $scope.options.selectedDate();
                                        date.setMonth(date.getMonth() + 1);
                                        date.setDate(0);
                                        $scope.options.selectedDate(date);
                                        break;
                                    case $.ui.keyCode.PAGE_UP:
                                    case $.ui.keyCode.UP:
                                        date = $scope.options.selectedDate();
                                        if (e.ctrlKey) {
                                            date.setFullYear(date.getFullYear() - 1);
                                        }
                                        else if (e.shiftKey) {
                                            date.setMonth(date.getMonth() - 1);
                                        }
                                        else {
                                            date.setDate(date.getDate() - 7);
                                        }
                                        $scope.options.selectedDate(date);
                                        break;
                                    case $.ui.keyCode.PAGE_DOWN:
                                    case $.ui.keyCode.DOWN:
                                        date = $scope.options.selectedDate();
                                        if (e.ctrlKey) {
                                            date.setFullYear(date.getFullYear() + 1);
                                        }
                                        else if (e.shiftKey) {
                                            date.setMonth(date.getMonth() + 1);
                                        }
                                        else {
                                            date.setDate(date.getDate() + 7);
                                        }
                                        $scope.options.selectedDate(date);
                                        break;
                                    case $.ui.keyCode.LEFT:
                                        date = $scope.options.selectedDate();
                                        if (e.ctrlKey) {
                                            date.setFullYear(date.getFullYear() - 1);
                                        }
                                        else if (e.shiftKey) {
                                            date.setMonth(date.getMonth() - 1);
                                        }
                                        else {
                                            date.setDate(date.getDate() - 1);
                                        }
                                        $scope.options.selectedDate(date);
                                        break;
                                    case $.ui.keyCode.RIGHT:
                                        date = $scope.options.selectedDate();
                                        if (e.ctrlKey) {
                                            date.setFullYear(date.getFullYear() + 1);
                                        }
                                        else if (e.shiftKey) {
                                            date.setMonth(date.getMonth() + 1);
                                        }
                                        else {
                                            date.setDate(date.getDate() + 1);
                                        }
                                        $scope.options.selectedDate(date);
                                        break;
                                    case $.ui.keyCode.ESCAPE:
                                        $scope.options.close();
                                        break;
                                    case $.ui.keyCode.ENTER:
                                    case $.ui.keyCode.SPACE:
                                        date = $scope.options.selectedDate();
                                        $scope.options.ondateselected(date);
                                        break;
                                    default:
                                        $timeout.cancel(this.filterTimer);
                                        this.previousFilter = (this.previousFilter || "") + e.char;
                                        if (this.previousFilter === 'now' || this.previousFilter === 'today') {
                                            $scope.options.selectedDate(new Date());
                                            delete this.previousFilter;
                                        }
                                        else {
                                            try {
                                                date = $dateParser(this.previousFilter, $scope.options.dateFormat);
                                            }
                                            catch (e) {
                                            }
                                            if (date) {
                                                $scope.options.selectedDate(date);
                                            }
                                            this.filterTimer = $timeout(function () {
                                                delete that.previousFilter;
                                            }, 2000);
                                        }
                                }
                                e.stopPropagation();
                            };
                            var dataWatcher = function (value) {
                                var closeTimer;
                                $scope.options = (value || new DatePickerOptions());
                                $scope.options.element = element;
                                // Complete the current option instance
                                angular.extend($scope.options, new DatePickerOptions(), angular.extend({}, $scope.options));
                                $scope.options.open = function () {
                                    if (!element.is(":visible")) {
                                        var event = new Event('opening');
                                        event.options = $scope.options;
                                        $scope.options.onopening(event);
                                        if (event.cancelBubble) {
                                            return;
                                        }
                                        // Close all other instances
                                        modalService.closeAllModalInstances();
                                        // size and position menu
                                        element.show();
                                        backdrop.show();
                                        $scope.options.resize();
                                        if ($scope.options.left) {
                                            $scope.options.position.at = "left+" + $scope.options.left + " bottom";
                                        }
                                        else {
                                            $scope.options.position.at = "left bottom";
                                        }
                                        element.position(angular.extend({
                                            of: $scope.options.parentElement
                                        }, $scope.options.position));
                                        if ($scope.options._selectedDate) {
                                            $scope.options.selectedDate($scope.options._selectedDate);
                                            $scope.options._selectedDate = null;
                                        }
                                        element.removeClass('transparent');
                                        $scope.options.onopened();
                                        $document.on('keydown', onKeyDown);
                                    }
                                };
                                $scope.options.selectedDate = function (value) {
                                    if (value instanceof Date) {
                                        $(element).datepicker("setDate", value);
                                    }
                                    else if (value === null) {
                                        $scope.options.selectedDate(new Date());
                                    }
                                    else if (value !== undefined) {
                                        // Try to parse
                                        /* global $filter */
                                        var date = $filter('date')(date[value.toString()], that.dateFormat);
                                        $scope.options.selectedDate(date instanceof Date ? date : null);
                                    }
                                    else {
                                        return element.datepicker("getDate");
                                    }
                                };
                                $scope.options.close = function (immediate) {
                                    if (element.is(":visible")) {
                                        backdrop.hide();
                                        $scope.options.onclosed();
                                        $document.off('keydown', onKeyDown);
                                        element.addClass('transparent');
                                        if (closeTimer) {
                                            $timeout.cancel(closeTimer);
                                            closeTimer = null;
                                        }
                                        if (immediate) {
                                            element.hide();
                                        }
                                        else {
                                            closeTimer = $timeout(function () {
                                                element.hide();
                                            }, 200);
                                        }
                                    }
                                };
                                modalService.addModalInstance(String($scope.$id), function () {
                                    $scope.options.close(true);
                                });
                                $scope.options.onSelect = function () {
                                    $scope.$apply(function () {
                                        $scope.options.ondateselected(element.datepicker("getDate"));
                                    });
                                };
                                $scope.options.resize = function () {
                                    // Firefox wraps long text (possibly a rounding bug)
                                    // so we add 1px to avoid the wrapping (#7513)
                                    element.outerWidth(Math.max(element.width("").outerWidth() + 1, 100));
                                    $scope.options.onresized();
                                };
                                element.datepicker($scope.options);
                            };
                            $scope.$parent.$watch(attrs.options, dataWatcher);
                        },
                    };
                },
            };
        }
        return DatePickerDirective;
    }());
})(vapkse || (vapkse = {}));
