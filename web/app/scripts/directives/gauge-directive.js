/// <reference path="../../../typings/angularjs/angular.d.ts" />
'use strict';
var vapkse;
(function (vapkse) {
    if (define) {
        define(['app'], function (app) {
            app.register.constant('gaugeStyles', GaugeStyles);
            app.register.directive('gauge', function ($window) { return new GaugeDirective($window); });
        });
    }
    else {
        angular.module('vapkse').constant('gaugeStyles', GaugeStyles).directive('gauge', function ($window) { return new GaugeDirective($window); });
    }
    (function (GaugeStyles) {
        GaugeStyles[GaugeStyles["circular"] = 0] = "circular";
        GaugeStyles[GaugeStyles["horizontal"] = 1] = "horizontal";
        GaugeStyles[GaugeStyles["vertical"] = 2] = "vertical";
    })(vapkse.GaugeStyles || (vapkse.GaugeStyles = {}));
    var GaugeStyles = vapkse.GaugeStyles;
    var GaugeOptions = (function () {
        function GaugeOptions() {
            this.backgroundColor = '#222';
            this.titleTextColor = "#ccc";
            this.valueColor = 'lightblue';
            this.refColor = '#ccc';
            this.limitsColor = 'red';
            this.valueLineWidth = 20;
            this.refLineWidth = 0;
            this.valueTextSize = 40;
            this.refTextSize = 14;
            this.titleTextSize = 14;
            this.valueText = '';
            this.refText = '';
            this.speed = 0.1; // 0 to 1;
            this.limits = []; // Array of limits indicators
            this.style = GaugeStyles.circular;
            this.getColor = function (colorName, value) {
                var color = this[colorName];
                if (angular.isNumber(value) && color instanceof Array && this.limits && this.limits.length) {
                    var index = this.limits.length;
                    while (index > 0) {
                        if (value > this.limits[index - 1]) {
                            return color[index];
                        }
                        index--;
                    }
                    return color[0];
                }
                else if (typeof color === 'function') {
                    return color(value);
                }
                return color;
            };
            this.getText = function (textName, value) {
                var text = this[textName];
                if (typeof text === 'function') {
                    return text(value);
                }
                else {
                    return text || (angular.isNumber(value) ? Math.floor(value / 100 * 100) + '%' : '');
                }
            };
        }
        return GaugeOptions;
    }());
    vapkse.GaugeOptions = GaugeOptions;
    var GaugeDirective = (function () {
        function GaugeDirective($window) {
            return {
                restrict: 'E',
                template: '<canvas class="gauge"></canvas>',
                replace: true,
                scope: true,
                compile: function () {
                    return {
                        pre: function (scope, element, attrs) {
                            var $scope = scope;
                            //canvas initialization
                            var canvas = element[0];
                            var ctx = canvas.getContext('2d');
                            var currentValue;
                            var currentRef;
                            var width;
                            var height;
                            var requestAnimationId = 0;
                            var animationLastTime = 0;
                            var optionsWatcher = function (value) {
                                $scope.options = value || new GaugeOptions();
                                $scope.options.element = element;
                                $scope.options.speed = $scope.options.speed || 20;
                                if (attrs.valueText && $scope.options.valueText === undefined) {
                                    $scope.options.valueText = $scope.$eval(attrs.valueText);
                                }
                                // Complete the current option instance
                                angular.extend($scope.options, new GaugeOptions(), angular.extend({}, $scope.options));
                                //dimensions
                                width = canvas.width = element.width();
                                height = canvas.height = element.height();
                                startAnimation();
                            };
                            function startAnimation() {
                                if (!$window.performance || !$window.requestAnimationFrame) {
                                    currentValue = $scope.options.value;
                                    currentRef = $scope.options.reference;
                                }
                                else {
                                    stopAnimation();
                                    animationLastTime = $window.performance.now();
                                    requestAnimationId = $window.requestAnimationFrame(animate);
                                    if (angular.isNumber($scope.options.value)) {
                                        currentValue = currentValue || 0;
                                    }
                                    if (angular.isNumber($scope.options.reference)) {
                                        currentRef = currentRef || 0;
                                    }
                                }
                                draw();
                            }
                            function stopAnimation() {
                                if (requestAnimationId) {
                                    window.cancelAnimationFrame(requestAnimationId);
                                    requestAnimationId = 0;
                                }
                            }
                            function animate() {
                                var hasValue = angular.isNumber($scope.options.value);
                                var hasRef = angular.isNumber($scope.options.reference);
                                var now = $window.performance.now();
                                var diff = now - animationLastTime;
                                //This will animate the gauge to new positions
                                //clear animation loop if value reaches to newValue
                                var valueMatch = !hasValue || Math.abs(currentValue - $scope.options.value) < 0.01;
                                var refMatch = !hasRef || Math.abs(currentRef - $scope.options.reference) < 0.01;
                                if (!hasValue) {
                                    currentValue = null;
                                }
                                else if (valueMatch) {
                                    currentValue = $scope.options.value;
                                }
                                else if ($scope.options.value < currentValue) {
                                    currentValue -= diff / $scope.options.speed;
                                    if (currentValue < $scope.options.value) {
                                        currentValue = $scope.options.value;
                                    }
                                }
                                else {
                                    currentValue += diff / $scope.options.speed;
                                    if (currentValue > $scope.options.value) {
                                        currentValue = $scope.options.value;
                                    }
                                }
                                if (!hasRef) {
                                    currentRef = null;
                                }
                                else if (refMatch) {
                                    currentRef = $scope.options.reference;
                                }
                                else if ($scope.options.reference < currentRef) {
                                    currentRef -= diff / $scope.options.speed;
                                    if (currentRef < $scope.options.reference) {
                                        currentRef = $scope.options.reference;
                                    }
                                }
                                else {
                                    currentRef += diff / $scope.options.speed;
                                    if (currentRef > $scope.options.reference) {
                                        currentRef = $scope.options.reference;
                                    }
                                }
                                draw();
                                if (!valueMatch || !refMatch) {
                                    // Next frame
                                    animationLastTime = now;
                                    requestAnimationId = window.requestAnimationFrame(animate);
                                }
                            }
                            function draw() {
                                // Position function for horizontal layout
                                var getPos = $scope.options.style !== GaugeStyles.circular && function (value) {
                                    return $scope.options.style === GaugeStyles.horizontal ? halfLineWidth + (width - 2 * halfLineWidth) * value / 100 : height - (halfLineWidth + (height - 2 * halfLineWidth) * value / 100);
                                };
                                //Clear the canvas everytime a chart is drawn
                                ctx.clearRect(0, 0, width, height);
                                // Measure text's size 
                                var valueText = $scope.options.getText('valueText', currentValue);
                                ctx.font = $scope.options.valueTextSize + 'px titilliumregular';
                                var valueTextWidth = valueText && ctx.measureText(valueText).width;
                                var titleText = $scope.options.getText('title');
                                ctx.font = $scope.options.titleTextSize + 'px titilliumregular';
                                var titleTextWidth = titleText && ctx.measureText(titleText).width;
                                var refText = $scope.options.getText('refText', currentRef);
                                ctx.font = $scope.options.refTextSize + 'px bebas';
                                var refTextWidth = refText && ctx.measureText(refText).width;
                                //Background 
                                var lineWidth = 0;
                                var halfLineWidth = 0;
                                ctx.beginPath();
                                ctx.strokeStyle = $scope.options.getColor('backgroundColor', currentValue);
                                switch ($scope.options.style) {
                                    case GaugeStyles.horizontal:
                                        //Background rounded line
                                        lineWidth = ctx.lineWidth = $scope.options.valueLineWidth;
                                        halfLineWidth = Math.ceil(lineWidth / 2);
                                        ctx.lineCap = 'round';
                                        ctx.moveTo(halfLineWidth, height - halfLineWidth);
                                        ctx.lineTo(width - halfLineWidth, height - halfLineWidth);
                                        break;
                                    case GaugeStyles.vertical:
                                        //Background rounded line
                                        lineWidth = ctx.lineWidth = $scope.options.valueLineWidth;
                                        halfLineWidth = Math.ceil(lineWidth / 2);
                                        ctx.lineCap = 'round';
                                        ctx.moveTo(halfLineWidth, halfLineWidth);
                                        ctx.lineTo(halfLineWidth, height - halfLineWidth);
                                        break;
                                    default:
                                        //Background 360 degree arc
                                        lineWidth = ctx.lineWidth = $scope.options.valueLineWidth + $scope.options.refLineWidth + 1;
                                        var radius = (width - ctx.lineWidth) / 2;
                                        ctx.arc(width / 2, height / 2, radius, 0, Math.PI * 2, false);
                                        break;
                                }
                                ctx.stroke();
                                var valueColor = $scope.options.getColor('valueColor', currentValue);
                                var valuePos = getPos && getPos(currentValue);
                                if (angular.isNumber(currentValue)) {
                                    ctx.beginPath();
                                    switch ($scope.options.style) {
                                        case GaugeStyles.horizontal:
                                            ctx.fillStyle = valueColor;
                                            ctx.arc(valuePos, height - halfLineWidth, halfLineWidth, 0, 2 * Math.PI, false);
                                            ctx.fill();
                                            break;
                                        case GaugeStyles.vertical:
                                            ctx.fillStyle = valueColor;
                                            ctx.arc(halfLineWidth, valuePos, halfLineWidth, 0, 2 * Math.PI, true);
                                            ctx.fill();
                                            break;
                                        default:
                                            ctx.strokeStyle = valueColor;
                                            //gauge will be a simple arc
                                            //Angle in radians = angle in value * PI / 180
                                            ctx.lineWidth = $scope.options.valueLineWidth;
                                            var radians = currentValue * Math.PI / 50;
                                            var valueRadius = (width - ctx.lineWidth) / 2;
                                            //The arc starts from the rightmost end. If we deduct 90 value from the angles
                                            //the arc will start from the topmost end
                                            ctx.arc(width / 2, height / 2, valueRadius, 0 - 90 * Math.PI / 180, radians - 90 * Math.PI / 180, false);
                                            ctx.stroke();
                                            break;
                                    }
                                }
                                //Lets add the valueText
                                var valueTextPos = 0;
                                if (valueText) {
                                    ctx.fillStyle = valueColor;
                                    ctx.font = $scope.options.valueTextSize + 'px titilliumregular';
                                    switch ($scope.options.style) {
                                        case GaugeStyles.horizontal:
                                            ctx.textBaseline = "bottom";
                                            valueTextPos = valueText && Math.min(Math.max(0, (valuePos || 0) - valueTextWidth / 2), width - valueTextWidth);
                                            ctx.fillText(valueText, valueTextPos, height - lineWidth - 2);
                                            break;
                                        case GaugeStyles.vertical:
                                            ctx.textBaseline = "middle";
                                            valueTextPos = valueText && Math.min(Math.max($scope.options.valueTextSize / 2, valuePos || 0), height - $scope.options.valueTextSize / 2);
                                            ctx.fillText(valueText, lineWidth + 10, valueTextPos);
                                            break;
                                        default:
                                            ctx.textBaseline = "middle";
                                            ctx.fillText(valueText, width / 2 - valueTextWidth / 2, height / 2);
                                            break;
                                    }
                                }
                                if (titleText) {
                                    ctx.fillStyle = $scope.options.getColor('titleTextColor');
                                    ctx.font = $scope.options.titleTextSize + 'px titilliumregular';
                                    switch ($scope.options.style) {
                                        case GaugeStyles.horizontal:
                                            ctx.textBaseline = "Bottom";
                                            ctx.fillText(titleText, width / 2 - titleTextWidth / 2, height - lineWidth - $scope.options.valueTextSize - 10);
                                            break;
                                        case GaugeStyles.vertical:
                                            if (!valueTextPos || valueTextPos - $scope.options.valueTextSize > 5) {
                                                ctx.textBaseline = "top";
                                                ctx.fillText(titleText, lineWidth + 10, 5);
                                            }
                                            break;
                                        default:
                                            ctx.textBaseline = "Bottom";
                                            ctx.fillText(titleText, width / 2 - titleTextWidth / 2, (height - $scope.options.valueTextSize) / 2 - 10);
                                            break;
                                    }
                                }
                                var refColor;
                                if ($scope.options.refLineWidth && angular.isNumber(currentRef)) {
                                    refColor = $scope.options.getColor('refColor', currentRef);
                                    ctx.beginPath();
                                    ctx.strokeStyle = refColor;
                                    var refLineWidth = ctx.lineWidth = $scope.options.refLineWidth;
                                    switch ($scope.options.style) {
                                        case GaugeStyles.horizontal:
                                            ctx.lineCap = 'square';
                                            var x = getPos(currentRef);
                                            ctx.moveTo(x, height - lineWidth + refLineWidth / 2);
                                            ctx.lineTo(x, height - refLineWidth / 2);
                                            break;
                                        case GaugeStyles.vertical:
                                            ctx.lineCap = 'square';
                                            var y = getPos(currentRef);
                                            ctx.moveTo(refLineWidth / 2, y);
                                            ctx.lineTo(lineWidth - refLineWidth / 2, y);
                                            break;
                                        default:
                                            //Angle in radians = angle in value * PI / 180
                                            var radians2 = currentRef * Math.PI / 50;
                                            var radius2 = (width - 2 * $scope.options.valueLineWidth - $scope.options.refLineWidth - 2) / 2;
                                            //The arc starts from the rightmost end. If we deduct 90 value from the angles
                                            //the arc will start from the topmost end
                                            ctx.arc(width / 2, height / 2, radius2, 0 - 90 * Math.PI / 180, radians2 - 90 * Math.PI / 180, false);
                                            break;
                                    }
                                    ctx.stroke();
                                }
                                if (refText) {
                                    refColor = refColor || $scope.options.getColor('refColor', currentRef);
                                    ctx.beginPath();
                                    ctx.strokeStyle = refColor;
                                    ctx.font = $scope.options.refTextSize + 'px bebas';
                                    switch ($scope.options.style) {
                                        case GaugeStyles.horizontal:
                                            ctx.fillText(refText, width / 2 - refTextWidth / 2, height - $scope.options.titleTextSize - $scope.options.valueTextSize - $scope.options.refTextSize - 30);
                                            break;
                                        case GaugeStyles.vertical:
                                            ctx.fillText(refText, lineWidth + 10, height / 2 + $scope.options.valueTextSize);
                                            break;
                                        default:
                                            ctx.textBaseline = "top";
                                            ctx.fillText(refText, width / 2 - refTextWidth / 2, (height + $scope.options.valueTextSize) / 2 + 10);
                                            break;
                                    }
                                    ctx.stroke();
                                }
                                if ($scope.options.limits) {
                                    angular.forEach($scope.options.limits, function (limit) {
                                        ctx.beginPath();
                                        ctx.strokeStyle = $scope.options.limitsColor;
                                        switch ($scope.options.style) {
                                            case GaugeStyles.horizontal:
                                                ctx.lineWidth = 3;
                                                ctx.lineCap = 'square';
                                                var x = getPos(limit);
                                                ctx.moveTo(x, height - lineWidth + 2);
                                                ctx.lineTo(x, height - 2);
                                                break;
                                            case GaugeStyles.vertical:
                                                ctx.lineWidth = 3;
                                                ctx.lineCap = 'square';
                                                var y = getPos(limit);
                                                ctx.moveTo(2, y);
                                                ctx.lineTo(lineWidth - 2, y);
                                                break;
                                            default:
                                                ctx.lineWidth = $scope.options.valueLineWidth;
                                                //Angle in radians = angle in value * PI / 180
                                                var radians1 = (limit - 1) * Math.PI / 50;
                                                var radians2 = limit * Math.PI / 50;
                                                //The arc starts from the rightmost end. If we deduct 90 value from the angles
                                                //the arc will start from the topmost end
                                                ctx.arc(width / 2, height / 2, valueRadius, radians1 - 90 * Math.PI / 180, radians2 - 90 * Math.PI / 180, false);
                                                break;
                                        }
                                        ctx.stroke();
                                    });
                                }
                            }
                            $scope.$parent.$watch(attrs.options, optionsWatcher);
                        }
                    };
                }
            };
        }
        return GaugeDirective;
    }());
})(vapkse || (vapkse = {}));
