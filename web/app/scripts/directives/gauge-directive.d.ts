/// <reference path="../../../typings/angularjs/angular.d.ts" />
declare module vapkse {
    enum GaugeStyles {
        circular = 0,
        horizontal = 1,
        vertical = 2,
    }
    class GaugeOptions {
        title: string;
        backgroundColor: string;
        titleTextColor: string;
        valueColor: string | Array<any>;
        refColor: string;
        limitsColor: string;
        valueLineWidth: number;
        refLineWidth: number;
        valueTextSize: number;
        refTextSize: number;
        titleTextSize: number;
        valueText: string | Function;
        refText: string | Function;
        value: number;
        reference: number;
        speed: number;
        limits: Array<number>;
        style: GaugeStyles;
        element: JQuery;
        getColor: (colorName: string, value?: number) => string;
        getText: (textName: string, value?: number) => any;
    }
}
