/// <reference path="../../../typings/angularjs/angular.d.ts" />
'use strict';
var vapkse;
(function (vapkse) {
    if (define) {
        define(['app'], function (app) {
            app.register.constant('sliderDirection', SliderDirection);
            app.register.directive('slider', function () { return new SliderDirective(); });
        });
    }
    else {
        angular.module('vapkse').constant('sliderDirection', SliderDirection).directive('slider', function () { return new SliderDirective(); });
    }
    var tracker = (function () {
        function tracker() {
        }
        return tracker;
    }());
    var trackers = (function () {
        function trackers() {
            this.normal = new tracker();
        }
        return trackers;
    }());
    (function (SliderDirection) {
        SliderDirection[SliderDirection["horizontal"] = 0] = "horizontal";
        SliderDirection[SliderDirection["vertical"] = 1] = "vertical";
    })(vapkse.SliderDirection || (vapkse.SliderDirection = {}));
    var SliderDirection = vapkse.SliderDirection;
    var SliderOptions = (function () {
        function SliderOptions() {
            this.sliderSize = 20;
            this.textColor = '#ccc';
            this.textSize = 13;
            this.backgroundColor = '#222';
            this.valueColor = 'lightblue';
            this.valueCenterColor = '#444';
            this.markerSize = 30;
            this.start = 0;
            this.value = 50;
            this.end = 100;
            this.direction = SliderDirection.horizontal;
            this.getColor = function (colorName, value) {
                var color = this[colorName];
                if (color instanceof Array && this.limits && this.limits.length) {
                    var index = this.limits.length;
                    while (index > 0) {
                        if (value > this.limits[index - 1]) {
                            return color[index];
                        }
                        index--;
                    }
                    return color[0];
                }
                else if (typeof color === 'function') {
                    return color(value);
                }
                return color;
            };
        }
        return SliderOptions;
    }());
    vapkse.SliderOptions = SliderOptions;
    var SliderDirective = (function () {
        function SliderDirective() {
            return {
                restrict: 'E',
                replace: true,
                scope: true,
                template: '<span class="slider"><canvas></canvas></span>',
                compile: function () {
                    return {
                        pre: function (scope, element, attrs) {
                            var $scope = scope;
                            var x = 'x';
                            var y = 'y';
                            var width = 'width';
                            var height = 'height';
                            //canvas initialization
                            var canvas = element.find('canvas')[0];
                            var ctx = canvas.getContext('2d');
                            /* globals graphics */
                            var size = new graphics.ReversibleSize();
                            var trackerInfos = new trackers();
                            var movingTracker;
                            var optionsWatcher = function (value) {
                                $scope.options = value || new SliderOptions();
                                $scope.options.element = element;
                                // Complete the current option instance
                                angular.extend($scope.options, new SliderOptions(), angular.extend({}, $scope.options));
                                $scope.draw();
                            };
                            function handleStart(e) {
                                /* globals graphics */
                                var offset = new graphics.ReversiblePosition(e.clientX - element.offset().left, e.clientY - element.offset().top);
                                var buttonState = e.buttons === undefined ? e.which : e.buttons;
                                if (startTracker(offset, buttonState)) {
                                    e.preventDefault();
                                }
                            }
                            function touchStart(e) {
                                var touches = e.changedTouches;
                                if (touches.length !== 1) {
                                    return;
                                }
                                var touch = e.changedTouches[0];
                                /* globals graphics */
                                var offset = new graphics.ReversiblePosition(touch.clientX - element.offset().left, touch.clientY - element.offset().top);
                                if (startTracker(offset)) {
                                    e.preventDefault();
                                }
                            }
                            canvas.addEventListener("touchstart", touchStart, false);
                            canvas.addEventListener('mousedown', handleStart, false);
                            function startTracker(offset, buttonState) {
                                var registerEvents = function () {
                                    var $document = $(document);
                                    $document.on('mousemove', handleMove);
                                    $document.on('mouseup', releaseTracker);
                                    canvas.addEventListener("touchmove", touchMove, false);
                                    canvas.addEventListener("touchend", releaseTracker, false);
                                    canvas.addEventListener("touchcancel", releaseTracker, false);
                                    canvas.addEventListener("touchleave", releaseTracker, false);
                                };
                                // Check current tracker
                                for (var name in trackerInfos) {
                                    var info = trackerInfos[name];
                                    /* globals graphics */
                                    var c = new graphics.ReversiblePosition(Math.min(Math.max(info.pos[x], info.size / 2), size[width] - info.size / 2), info.pos[y]);
                                    if (offset[x] > c[x] - info.size / 2 && offset[x] < c[x] + info.size / 2 && offset[y] > c[y] - info.size / 2 && offset[y] < c[y] + info.size / 2) {
                                        movingTracker = trackerInfos[name];
                                        // Register next events
                                        registerEvents();
                                        return true;
                                    }
                                }
                                // Not found, move the normal tracker
                                var tracker = trackerInfos.normal;
                                if (tracker) {
                                    movingTracker = tracker;
                                    moveTracker(offset, buttonState);
                                    // Register next events
                                    registerEvents();
                                }
                                return true;
                            }
                            function handleMove(evt) {
                                var e = evt;
                                /* globals graphics */
                                var offset = new graphics.ReversiblePosition(e.clientX - element.offset().left, e.clientY - element.offset().top);
                                var buttonState = e.buttons === undefined ? e.which : e.buttons;
                                if (moveTracker(offset, buttonState)) {
                                    e.preventDefault();
                                }
                            }
                            function touchMove(e) {
                                var touches = e.changedTouches;
                                if (touches.length !== 1) {
                                    return;
                                }
                                var touch = e.changedTouches[0];
                                /* globals graphics */
                                var offset = new graphics.ReversiblePosition(touch.clientX - element.offset().left, touch.clientY - element.offset().top);
                                if (moveTracker(offset)) {
                                    e.preventDefault();
                                }
                            }
                            function moveTracker(offset, buttonState) {
                                if (!movingTracker) {
                                    return;
                                }
                                if ((buttonState && buttonState !== 1)) {
                                    // Mouse released
                                    releaseTracker();
                                    return;
                                }
                                var value = offset[x] * ($scope.options.end - $scope.options.start) / size[width];
                                value = Math.min(Math.max(value, $scope.options.start), $scope.options.end);
                                if ($scope.options.onvaluechanged) {
                                    $scope.options.onvaluechanged(value);
                                }
                                $scope.options.value = value;
                                $scope.draw();
                                return true;
                            }
                            function releaseTracker() {
                                movingTracker = null;
                                var $document = $(document);
                                $document.off('mousemove', handleMove);
                                $document.off('mouseup', releaseTracker);
                                canvas.removeEventListener("touchmove", touchMove, false);
                                canvas.removeEventListener("touchend", releaseTracker, false);
                                canvas.removeEventListener("touchcancel", releaseTracker, false);
                                canvas.removeEventListener("touchleave", releaseTracker, false);
                            }
                            $scope.draw = function () {
                                if ($scope.options.direction === SliderDirection.vertical) {
                                    x = 'y';
                                    y = 'x';
                                    width = 'height';
                                    height = 'width';
                                }
                                //dimensions
                                size.width = canvas.width = element.width();
                                size.height = canvas.height = element.height();
                                // Texts        
                                var startText = getText($scope.options.startText) || $scope.options.start;
                                var endText = getText($scope.options.endText) || $scope.options.end;
                                var trackerText = getText($scope.options.valueText) || $scope.options.value;
                                // Measure texts
                                ctx.font = $scope.options.textSize + 'px titilliumregular';
                                var startTextSize = ctx.measureText(startText).width;
                                var endTextSize = ctx.measureText(endText).width;
                                var textSize = $scope.options.direction === SliderDirection.horizontal ? $scope.options.textSize : 0;
                                if (typeof $scope.options.endText === 'function') {
                                    endText = $scope.options.endText();
                                }
                                else {
                                    endText = $scope.options.endText || $scope.options.end;
                                }
                                //Clear the canvas
                                ctx.clearRect(0, 0, size.width, size.height);
                                //Background rounded line
                                var sliderSize = $scope.options.sliderSize / 2;
                                /* globals graphics */
                                var spos = new graphics.ReversiblePosition(sliderSize / 2, textSize + $scope.options.markerSize / 2 + 5);
                                /* globals graphics */
                                var epos = new graphics.ReversiblePosition(size[width] - sliderSize / 2, spos.y);
                                ctx.beginPath();
                                ctx.strokeStyle = $scope.options.getColor('backgroundColor');
                                ctx.lineWidth = sliderSize;
                                ctx.lineCap = 'round';
                                ctx.moveTo(spos[x], spos[y]);
                                ctx.lineTo(epos[x], epos[y]);
                                //you can see the line now
                                ctx.stroke();
                                // Draw marker now
                                if ($scope.options.end !== $scope.options.start) {
                                    //marker will be a circle
                                    trackerInfos.normal = trackerInfos.normal || new tracker();
                                    trackerInfos.normal.size = $scope.options.markerSize;
                                    trackerInfos.normal.color = $scope.options.getColor('valueColor');
                                    trackerInfos.normal.centerColor = $scope.options.getColor('valueCenterColor');
                                    trackerInfos.normal.pos = {
                                        x: $scope.options.value * size[width] / ($scope.options.end - $scope.options.start),
                                        y: spos.y,
                                    };
                                    var textRect;
                                    if ($scope.options.direction === SliderDirection.horizontal) {
                                        // horizontal
                                        textRect = drawText(trackerText, trackerInfos.normal.pos.x, 0, 'center');
                                        if (textRect.x > startTextSize) {
                                            drawText(startText, 0, 0, 'left');
                                        }
                                        if (textRect.x + textRect.width < size.width - endTextSize) {
                                            drawText(endText, size.width, 0, 'right');
                                        }
                                    }
                                    else {
                                        // Verical
                                        var left = $scope.options.markerSize + 5;
                                        textRect = drawText(trackerText, left, trackerInfos.normal.pos[y] - $scope.options.textSize / 2, 'left');
                                        if (textRect.y > $scope.options.textSize) {
                                            drawText(startText, left, 0, 'left');
                                        }
                                        if (textRect.y + textRect.height < size.height - $scope.options.textSize) {
                                            drawText(endText, left, size.height - $scope.options.textSize, 'left');
                                        }
                                    }
                                    drawTracker(trackerInfos.normal);
                                }
                                else {
                                    drawText(startText, $scope.options.direction === SliderDirection.horizontal ? 0 : $scope.options.markerSize + 5, 0, 'left');
                                }
                            };
                            function getText(p) {
                                if (typeof p === 'function') {
                                    return p.call($scope.options, $scope.options.value);
                                }
                                else {
                                    return p;
                                }
                            }
                            function drawText(text, x, y, alignment) {
                                var textWidth = ctx.measureText(text).width;
                                // add the start
                                ctx.fillStyle = $scope.options.getColor('textColor');
                                var textOverflow = alignment === 'center' ? textWidth / 2 : textWidth;
                                if (alignment !== 'left' && x - textOverflow < $scope.options.sliderSize / 2) {
                                    alignment = 'left';
                                    x = 0;
                                }
                                else if (alignment !== 'right' && x + textOverflow > size.width - $scope.options.sliderSize / 2) {
                                    alignment = 'right';
                                    x = size.width;
                                }
                                if (y < 0) {
                                    y = 0;
                                }
                                else if (y + $scope.options.textSize > size.height) {
                                    y = size.height - $scope.options.textSize;
                                }
                                ctx.textAlign = alignment;
                                //adding manual value to position y since the height of the valueText cannot
                                //be measured easily. There are hacks but we will keep it manual for now.
                                ctx.fillText(text, x, y + $scope.options.textSize);
                                var left;
                                if (alignment === 'left') {
                                    left = x;
                                }
                                else if (alignment === 'right') {
                                    left = x - textWidth;
                                }
                                else {
                                    left = x - textWidth / 2;
                                }
                                /* globals graphics */
                                return new graphics.Rect(left, y, textWidth, $scope.options.textSize);
                            }
                            function drawTracker(infos) {
                                /* globals graphics */
                                var tpos = new graphics.ReversiblePosition(Math.min(Math.max(infos.pos.x, infos.size / 2.5), size[width] - infos.size / 2.5), infos.pos.y);
                                drawCircle(infos.color, infos.size, tpos);
                                if (infos.centerColor) {
                                    drawCircle(infos.centerColor, infos.size / 2, tpos);
                                }
                            }
                            function drawCircle(color, size, tpos) {
                                ctx.beginPath();
                                ctx.strokeStyle = color;
                                ctx.lineWidth = size / 2;
                                var radius = (size - ctx.lineWidth) / 4;
                                ctx.arc(tpos[x], tpos[y], radius, 0, Math.PI * 2, false);
                                ctx.stroke();
                            }
                            $scope.$parent.$watch(attrs.options, optionsWatcher);
                        },
                    };
                },
            };
        }
        return SliderDirective;
    }());
    vapkse.SliderDirective = SliderDirective;
})(vapkse || (vapkse = {}));
