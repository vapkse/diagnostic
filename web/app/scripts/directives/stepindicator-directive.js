/// <reference path="../../../typings/angularjs/angular.d.ts" />
'use strict';
var dampApp;
(function (dampApp) {
    define(['app'], function (app) {
        app.register.directive('stepindicator', function ($window) { return new StepindicatorDirective($window); });
    });
    var StepindicatorOptions = (function () {
        function StepindicatorOptions() {
            this.valueLineColor = '#fff';
            this.timeLineColor = 'hsla(195, 100%, 69%, 0.5)';
            this.labelColor = "#ccc";
            this.stepSeparatorColor = "#ccc";
            this.backgroundColor = '#222';
            this.labelTextSize = 14;
            this.progressSize = 40;
            this.speed = 0.1;
            this.stepMaxTime = 0;
            this.stepElapsedTime = 0;
            this.stepMaxValue = 0;
            this.stepValue = 0;
            this.stepIndex = 0;
            this.getColor = function (colorName, value) {
                var currentStep = this.stepIndex >= 0 && this.stepIndex < this.steps.length ? this.steps[this.stepIndex] : null;
                var color = currentStep !== null && currentStep[colorName] ? currentStep[colorName] : this[colorName];
                if (color instanceof Array && this.limits && this.limits.length) {
                    var index = this.limits.length;
                    while (index > 0) {
                        if (value > this.limits[index - 1]) {
                            return color[index];
                        }
                        index--;
                    }
                    return color[0];
                }
                else if (typeof color === 'function') {
                    return color(value);
                }
                return color;
            };
        }
        return StepindicatorOptions;
    }());
    dampApp.StepindicatorOptions = StepindicatorOptions;
    var StepindicatorDirective = (function () {
        function StepindicatorDirective($window) {
            return {
                restrict: 'E',
                template: '<canvas class="stepindicator"></canvas>',
                replace: true,
                scope: true,
                compile: function () {
                    return {
                        pre: function (scope, element, attrs) {
                            var $scope = scope;
                            var x = 'x';
                            var y = 'y';
                            var width = 'width';
                            /* globals graphics */
                            var size = new graphics.ReversibleSize();
                            var totalPercentTime = 0;
                            var totalPercentValue = 0;
                            var currentPercentValue = 0;
                            var currentPercentTime = 0;
                            var requestAnimationId = 0;
                            var animationLastTime = 0;
                            var steps = [];
                            //canvas initialization
                            var canvas = element[0];
                            var ctx = canvas.getContext('2d');
                            var optionsWatcher = function (value) {
                                $scope.options = value || new StepindicatorOptions();
                                $scope.options.element = element;
                                $scope.options.speed = $scope.options.speed || 10;
                                // Complete the current option instance
                                angular.extend($scope.options, new StepindicatorOptions(), angular.extend({}, $scope.options));
                                // Store steps
                                if ($scope.options.steps) {
                                    steps = $scope.options.steps;
                                }
                                // Current step object
                                var currentStep = $scope.options.stepIndex >= 0 && $scope.options.stepIndex < steps.length ? steps[$scope.options.stepIndex] : null;
                                if (currentStep !== null) {
                                    // Calc previous steps percent
                                    var i = $scope.options.stepIndex;
                                    var previousPercent = 0;
                                    while (--i >= 0) {
                                        previousPercent += steps[i].range || 0;
                                    }
                                    // Calc value progress in total percent
                                    totalPercentValue = previousPercent;
                                    if ($scope.options.stepMaxValue && $scope.options.stepValue && currentStep.range) {
                                        totalPercentValue += $scope.options.stepValue * currentStep.range / $scope.options.stepMaxValue;
                                    }
                                    // Calc time progress in percent
                                    totalPercentTime = previousPercent;
                                    if ($scope.options.stepMaxTime && $scope.options.stepElapsedTime && currentStep.range) {
                                        totalPercentTime += $scope.options.stepElapsedTime * currentStep.range / $scope.options.stepMaxTime;
                                    }
                                    steps[$scope.options.stepIndex].maxTime = $scope.options.stepMaxTime;
                                    steps[$scope.options.stepIndex].maxValue = $scope.options.stepMaxValue;
                                }
                                startAnimation();
                            };
                            function startAnimation() {
                                if (!$window.performance || !$window.requestAnimationFrame) {
                                    currentPercentValue = totalPercentValue;
                                    currentPercentTime = totalPercentTime;
                                }
                                else {
                                    stopAnimation();
                                    animationLastTime = $window.performance.now();
                                    requestAnimationId = $window.requestAnimationFrame(animate);
                                }
                                draw();
                            }
                            function stopAnimation() {
                                if (requestAnimationId) {
                                    window.cancelAnimationFrame(requestAnimationId);
                                    requestAnimationId = 0;
                                }
                            }
                            function animate() {
                                var valueMatch = Math.abs(currentPercentValue - totalPercentValue) < 0.01;
                                var timeMatch = Math.abs(currentPercentTime - totalPercentTime) < 0.01;
                                var now = $window.performance.now();
                                if ((valueMatch && timeMatch) || totalPercentValue < currentPercentValue - 20 || totalPercentTime < currentPercentTime - 20) {
                                    // Go down faster in case of restart or changing step
                                    currentPercentValue = totalPercentValue;
                                    currentPercentTime = totalPercentTime;
                                }
                                else {
                                    var diff = now - animationLastTime;
                                    if (valueMatch) {
                                        currentPercentValue = totalPercentValue;
                                    }
                                    else if (totalPercentValue < currentPercentValue) {
                                        currentPercentValue -= diff / $scope.options.speed;
                                        if (currentPercentValue < totalPercentValue) {
                                            currentPercentValue = totalPercentValue;
                                        }
                                    }
                                    else {
                                        currentPercentValue += diff / $scope.options.speed;
                                        if (currentPercentValue > totalPercentValue) {
                                            currentPercentValue = totalPercentValue;
                                        }
                                    }
                                    if (timeMatch) {
                                        currentPercentTime = totalPercentTime;
                                    }
                                    else if (totalPercentTime < currentPercentTime) {
                                        currentPercentTime -= diff / $scope.options.speed;
                                        if (currentPercentTime < totalPercentTime) {
                                            currentPercentTime = totalPercentTime;
                                        }
                                    }
                                    else {
                                        currentPercentTime += diff / $scope.options.speed;
                                        if (currentPercentTime > totalPercentTime) {
                                            currentPercentTime = totalPercentTime;
                                        }
                                    }
                                }
                                draw();
                                if (!valueMatch || !timeMatch) {
                                    // Next frame
                                    animationLastTime = now;
                                    requestAnimationId = window.requestAnimationFrame(animate);
                                }
                            }
                            function draw() {
                                var previousPercent = 0;
                                //dimensions
                                size.width = canvas.width = element.width();
                                size.height = canvas.height = element.height();
                                //Clear the canvas
                                ctx.clearRect(0, 0, size.width, size.height);
                                var currentStep = $scope.options.stepIndex >= 0 && $scope.options.stepIndex < steps.length ? steps[$scope.options.stepIndex] : null;
                                if (currentStep === null) {
                                    return;
                                }
                                if (currentStep.range !== 0) {
                                    // Calc current step from the value percentage
                                    var ii = -1;
                                    while (++ii < steps.length) {
                                        if (steps[ii].range > 0) {
                                            if (currentPercentValue >= previousPercent && currentPercentValue < previousPercent + steps[ii].range) {
                                                currentStep = steps[ii];
                                                break;
                                            }
                                            previousPercent += steps[ii].range;
                                        }
                                    }
                                    // Ensure that the current time is on the same step
                                    if (currentPercentTime <= previousPercent) {
                                        currentPercentTime = previousPercent;
                                    }
                                    else if (currentStep && currentPercentTime > previousPercent + currentStep.range) {
                                        currentPercentTime = previousPercent = previousPercent + currentStep.range;
                                    }
                                }
                                // Measure texts
                                var textSize = $scope.options.labelTextSize;
                                var progressSize = $scope.options.progressSize / 2;
                                // Draw status text
                                if (currentStep.label) {
                                    ctx.fillStyle = $scope.options.getColor('labelColor');
                                    ctx.font = textSize + 'px titilliumregular';
                                    ctx.fillText(currentStep.label, progressSize / 2, textSize);
                                }
                                // Draw elapsed time text
                                var label = '';
                                if (currentStep.maxTime) {
                                    // Calc elapsed time from percent
                                    var p = (currentPercentTime - previousPercent) / currentStep.range;
                                    /* globals numeral */
                                    var elapsedTime = numeral(p * currentStep.maxTime);
                                    /* globals numeral */
                                    var maxTime = numeral(currentStep.maxTime);
                                    label += 'elapsed: ' + elapsedTime.format('00') + ' / ' + maxTime.format('00') + 's';
                                }
                                // Draw value target
                                if (currentStep.maxValue) {
                                    /* globals numeral */
                                    var targetPct = numeral((currentPercentValue - previousPercent) * 100 / currentStep.range);
                                    label += '         target: ' + targetPct.format('00') + '%';
                                }
                                if (label.length > 0) {
                                    var textWidth = ctx.measureText(label).width;
                                    ctx.fillStyle = $scope.options.getColor('labelColor');
                                    ctx.font = textSize + 'px titilliumregular';
                                    ctx.fillText(label, size.width - progressSize / 2 - textWidth, textSize);
                                }
                                var top = textSize ? textSize + 2 : 0;
                                if (currentStep.range > 0) {
                                    /* globals graphics */
                                    var spos = new graphics.ReversiblePosition(progressSize / 2, top + $scope.options.progressSize / 2);
                                    /* globals graphics */
                                    var epos = new graphics.ReversiblePosition(size[width] - progressSize / 2, spos.y);
                                    //Background rounded line
                                    ctx.beginPath();
                                    ctx.strokeStyle = $scope.options.getColor('backgroundColor');
                                    ctx.lineWidth = progressSize;
                                    ctx.lineCap = 'round';
                                    ctx.moveTo(spos[x], spos[y]);
                                    ctx.lineTo(epos[x], epos[y]);
                                    ctx.stroke();
                                    // Draw target progression
                                    if (currentPercentValue) {
                                        ctx.beginPath();
                                        ctx.strokeStyle = $scope.options.getColor('valueLineColor');
                                        ctx.lineWidth = progressSize;
                                        ctx.lineCap = 'round';
                                        ctx.moveTo(spos[x], spos[y]);
                                        ctx.lineTo(spos[x] + currentPercentValue * (epos[x] - spos[x]) / 100, epos[y]);
                                        ctx.stroke();
                                    }
                                    // Calc time progression length
                                    if (currentPercentTime) {
                                        // Draw time progression
                                        ctx.beginPath();
                                        ctx.strokeStyle = $scope.options.getColor('timeLineColor');
                                        ctx.lineWidth = progressSize;
                                        ctx.lineCap = 'round';
                                        ctx.moveTo(spos[x], spos[y]);
                                        ctx.lineTo(spos[x] + currentPercentTime * (epos[x] - spos[x]) / 100, epos[y]);
                                        ctx.stroke();
                                    }
                                    // Draw the steps
                                    var totalRange = 0;
                                    top += $scope.options.progressSize / 4;
                                    for (var i = 0; i < steps.length; i++) {
                                        var step = steps[i];
                                        if (step.range > 0) {
                                            totalRange += step.range;
                                            if (totalRange < 100) {
                                                var left = spos[x] + totalRange * (epos[x] - spos[x]) / 100;
                                                ctx.beginPath();
                                                ctx.strokeStyle = $scope.options.getColor('stepSeparatorColor');
                                                ctx.lineWidth = 1;
                                                ctx.lineCap = 'square';
                                                ctx.moveTo(left, top);
                                                ctx.lineTo(left, top + progressSize);
                                                ctx.stroke();
                                            }
                                        }
                                    }
                                }
                            }
                            $scope.$parent.$watch(attrs.options, optionsWatcher);
                        },
                    };
                }
            };
        }
        return StepindicatorDirective;
    }());
    dampApp.StepindicatorDirective = StepindicatorDirective;
})(dampApp || (dampApp = {}));
