/// <reference path="../../../typings/angularjs/angular.d.ts" />
declare module vapkse {
    class DatePickerOptions {
        autoFocus: boolean;
        dateFormat: string;
        position: JQueryUI.JQueryPositionOptions;
        parentElement: JQuery;
        _selectedDate: Date;
        selectedDate: (date?: Date) => any;
        close: (value?: boolean) => void;
        open: () => any;
        left: number;
        element: JQuery;
        isVisible: (value?: boolean) => any;
        onopening: (e: DatePickerOpeningEvent) => void;
        onopened: () => any;
        onclosed: () => any;
        onresized: () => any;
        ondateselected: (date: Date) => void;
    }
    interface DatePickerOpeningEvent extends Event {
        options: DatePickerOptions;
    }
}
