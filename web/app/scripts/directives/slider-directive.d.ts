/// <reference path="../../../typings/angularjs/angular.d.ts" />
declare module vapkse {
    enum SliderDirection {
        horizontal = 0,
        vertical = 1,
    }
    class SliderOptions {
        sliderSize: number;
        textColor: string;
        textSize: number;
        backgroundColor: string;
        valueColor: string;
        valueCenterColor: string;
        markerSize: number;
        start: number;
        value: number;
        end: number;
        startText: any;
        valueText: any;
        endText: any;
        direction: SliderDirection;
        getColor: Function;
        element: JQuery;
        onvaluechanged: Function;
    }
    class SliderDirective implements ng.IDirective {
        constructor();
    }
}
