/// <reference path="../../../typings/angularjs/angular.d.ts" />
'use strict';
var Translation;
(function (Translation) {
    var TranslateProvider = (function () {
        function TranslateProvider() {
        }
        TranslateProvider.configure = function ($httpProvider, $translateProvider) {
            // Load translation files
            $.getJSON('translations/translations-fr.json', function (data) {
                $translateProvider.translations('fr', data);
            });
            $.getJSON('translations/translations-en.json', function (data) {
                $translateProvider.translations('en', data);
            });
            $translateProvider.preferredLanguage(window.sessionStorage.getItem('lang') || 'en');
        };
        TranslateProvider.$inject = ['$httpProvider', '$translateProvider'];
        return TranslateProvider;
    }());
    Translation.TranslateProvider = TranslateProvider;
})(Translation || (Translation = {}));
