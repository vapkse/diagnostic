/// <reference path="../../../typings/angularjs/angular.d.ts" />
declare module SocketIO {
    interface ISocketOptions extends SocketIOClient.ConnectOpts {
        scope: ng.IScope;
        prefix: string;
        ioSocket: SocketIOClient.Socket;
    }
    interface ICallback extends Function {
        __ng: any;
    }
    class SocketProviders {
        static provider(): {
            $get: ($rootScope: ng.IRootScopeService, $timeout: ng.ITimeoutService) => (options: ISocketOptions) => {
                on: (eventName: string, callback: ICallback) => void;
                addListener: (eventName: string, callback: ICallback) => void;
                once: (eventName: string, callback: ICallback) => void;
                emit: () => any;
                removeListener: (ev: string, fn: ICallback) => any;
                removeAllListeners: () => any;
                disconnect: () => SocketIOClient.Socket;
                forward: (events: any, scope: ng.IScope) => void;
            };
        };
        static factory(socketFactory: SocketIOClientStatic): SocketIOClient.Socket;
    }
}
