/// <reference path="../../../typings/angularjs/angular.d.ts" />
'use strict';
var SocketIO;
(function (SocketIO) {
    var SocketProviders = (function () {
        function SocketProviders() {
        }
        // Socket.io Provider
        // angular.module('socketProviders', []).provider('socketFactory', SocketIO.SocketProviders.provider).factory('socket', SocketIO.SocketProviders.factory);
        SocketProviders.provider = function () {
            // when forwarding events, prefix the event name
            var defaultPrefix = 'socket:';
            return {
                $get: function ($rootScope, $timeout) {
                    var asyncAngularify = function (socket, callback) {
                        return callback ? function () {
                            var args = arguments;
                            $timeout(function () {
                                callback.apply(socket, args);
                            }, 0);
                        } : angular.noop;
                    };
                    return function socketFactory(options) {
                        var socket = (options && options.ioSocket) || io.connect();
                        var prefix = (options && options.prefix) || defaultPrefix;
                        var defaultScope = (options && options.scope) || $rootScope;
                        var addListener = function (eventName, callback) {
                            socket.on(eventName, callback.__ng = asyncAngularify(socket, callback));
                        };
                        var addOnceListener = function (eventName, callback) {
                            socket.once(eventName, callback.__ng = asyncAngularify(socket, callback));
                        };
                        var wrappedSocket = {
                            on: addListener,
                            addListener: addListener,
                            once: addOnceListener,
                            emit: function () {
                                var lastIndex = arguments.length - 1;
                                var cbk = arguments[lastIndex];
                                if (typeof cbk === 'function') {
                                    cbk = asyncAngularify(socket, cbk);
                                    arguments[lastIndex] = cbk;
                                }
                                return socket.emit.apply(socket, arguments);
                            },
                            removeListener: function (ev, fn) {
                                if (fn && fn.__ng) {
                                    arguments[1] = fn.__ng;
                                }
                                return socket.removeListener.apply(socket, arguments);
                            },
                            removeAllListeners: function () {
                                return socket.removeAllListeners.apply(socket, arguments);
                            },
                            disconnect: function () {
                                return socket.disconnect();
                            },
                            // when socket.on('someEvent', fn (data) { ... }),
                            // call scope.$broadcast('someEvent', data)
                            forward: function (events, scope) {
                                if (events instanceof Array === false) {
                                    events = [events];
                                }
                                if (!scope) {
                                    scope = defaultScope;
                                }
                                events.forEach(function (eventName) {
                                    var prefixedEvent = prefix + eventName;
                                    var forwardBroadcast = asyncAngularify(socket, function (data) {
                                        scope.$broadcast(prefixedEvent, data);
                                    });
                                    scope.$on('$destroy', function () {
                                        socket.removeListener(eventName, forwardBroadcast);
                                    });
                                    socket.on(eventName, forwardBroadcast);
                                });
                            }
                        };
                        return wrappedSocket;
                    };
                },
            };
        };
        SocketProviders.factory = function (socketFactory) {
            return socketFactory();
        };
        return SocketProviders;
    }());
    SocketIO.SocketProviders = SocketProviders;
})(SocketIO || (SocketIO = {}));
