/// <reference path="../../../typings/angularjs/angular.d.ts" />
declare module Utilities {
    class Filters {
        static propertyMissing(): (items: any[], property: string) => any[];
        static propertyExists(): (items: any[], property: string) => any[];
        static getByProperty(): (propertyName: string, propertyValue: string, items: any[]) => any;
        static find(): (items: any[], predicate: Function) => any;
    }
}
