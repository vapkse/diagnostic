/// <reference path="../../../typings/angularjs/angular.d.ts" />
declare module Translation {
    class TranslateProvider {
        static $inject: string[];
        static configure($httpProvider: ng.IHttpProvider, $translateProvider: ng.translate.ITranslateProvider): void;
    }
}
