/// <reference path="../../../typings/angularjs/angular.d.ts" />

'use strict';

module Translation {
    export class TranslateProvider {
        static $inject = ['$httpProvider', '$translateProvider'];
        public static configure($httpProvider: ng.IHttpProvider, $translateProvider: ng.translate.ITranslateProvider) {
            // Load translation files
            $.getJSON('translations/translations-fr.json', function(data) {
                $translateProvider.translations('fr', data);
            });

            $.getJSON('translations/translations-en.json', function(data) {
                $translateProvider.translations('en', data);
            });

            $translateProvider.preferredLanguage(window.sessionStorage.getItem('lang') || 'en');
        }
    }
}