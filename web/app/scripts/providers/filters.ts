/// <reference path="../../../typings/angularjs/angular.d.ts" />
'use strict';

module Utilities {
    export class Filters {
        public static propertyMissing() {
            return function(items: Array<any>, property: string): Array<any> {
                var returnObj: Array<any> = [];
                if (items) {
                    for (var i: number = 0; i < items.length; i++) {
                        var test: Function = property !== undefined ? items[i][property] : items[i];
                        if (test === undefined || test === null) {
                            returnObj.push(items[i]);
                        }
                    }
                }
                return returnObj;
            }
        }

        public static propertyExists() {
            return function(items: Array<any>, property: string): Array<any> {
                var returnObj: Array<any> = [];
                if (items) {
                    for (var i: number = 0; i < items.length; i++) {
                        var test: Function = property !== undefined ? items[i][property] : items[i];
                        if (test !== undefined && test !== null) {
                            returnObj.push(items[i]);
                        }
                    }
                }
                return returnObj;
            }
        }
        
        public static getByProperty(){
            return function(propertyName: string, propertyValue: string, items: Array<any>) {
                for (var i = 0; i < items.length; i++) {
                    if (items[i][propertyName] === propertyValue) {
                        return items[i];
                    }
                }
                return null;
            };            
        }
        
        public static find(){
            return function(items: Array<any>, predicate: Function) {
                var i = 0, len = items.length;
                for (; i < len; i++) {
                    if (predicate(items[i]) === true) {
                        return items[i];
                    }
                }
                return null;
            };            
        }
    }
}