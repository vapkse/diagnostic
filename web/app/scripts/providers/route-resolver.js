/// <reference path="../../../typings/angularjs/angular.d.ts" />
'use strict';
var Routage;
(function (Routage) {
    var RouteResolver = (function () {
        function RouteResolver() {
        }
        // Route resolver provider
        // angular.module('routeResolverServices', ['config']).provider('routeResolver', Routage.RouteResolver.provider);
        RouteResolver.provider = function (config) {
            var resolveDependencies = function ($q, $rootScope, dependencies) {
                var defer = $q.defer();
                requirejs(dependencies, function () {
                    defer.resolve();
                    $rootScope.$apply();
                });
                return defer.promise;
            };
            return {
                $get: function () {
                    return this;
                },
                resolve: function (template, controller, dependencies, css) {
                    var routeDef = {
                        templateUrl: config.route.templates[template],
                        controller: controller,
                        reloadOnSearch: false,
                        resolve: {
                            load: ['$q', '$rootScope',
                                function ($q, $rootScope) {
                                    angular.forEach(css || [], function (cssName) {
                                        var link = document.createElement("link");
                                        link.type = "text/css";
                                        link.rel = "stylesheet";
                                        link.href = config.route.css[cssName];
                                        document.getElementsByTagName("head")[0].appendChild(link);
                                    });
                                    return resolveDependencies($q, $rootScope, dependencies || [controller]);
                                }]
                        }
                    };
                    return routeDef;
                }
            };
        };
        return RouteResolver;
    }());
    Routage.RouteResolver = RouteResolver;
})(Routage || (Routage = {}));
