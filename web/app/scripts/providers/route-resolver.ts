﻿/// <reference path="../../../typings/angularjs/angular.d.ts" />

'use strict';

module Routage {
	export interface IRoutageConfig {
		route: IRouteDescription
	}

	export interface IRouteDescription extends RequireConfig {
        css: { [key: string]: string; },
        templates: { [key: string]: string; },
        priority: Array<string>
    }	
	
	export interface IRegisterProvider {
		controller: Function,
		directive: Function,
		filter: Function,
		factory: Function,
		service: Function,
		provider: Function,
		constant: Function
	}
 
	export interface IModule extends ng.IModule {
		register: IRegisterProvider
	}
	
    export class RouteResolver {
		// Route resolver provider
		// angular.module('routeResolverServices', ['config']).provider('routeResolver', Routage.RouteResolver.provider);
        public static provider(config: IRoutageConfig) {
			var resolveDependencies = function($q: ng.IQService, $rootScope: ng.IRootScopeService, dependencies: Array<string>) {
				var defer = $q.defer();
				requirejs(dependencies, function() {
					defer.resolve();
					$rootScope.$apply();
				});
	
				return defer.promise;
			};
	
			return {
				$get: function() {
					return this;
				},
	
				resolve: function(template: string, controller: string, dependencies: Array<string>, css?: Array<string>) {
					var routeDef = {
						templateUrl: config.route.templates[template],
						controller: controller,
						reloadOnSearch: false,
						resolve: {
							load: ['$q', '$rootScope',
								function($q: ng.IQService, $rootScope: ng.IRootScopeService) {
									angular.forEach(css || [], function(cssName) {
										var link = document.createElement("link");
										link.type = "text/css";
										link.rel = "stylesheet";
										link.href = config.route.css[cssName];
										document.getElementsByTagName("head")[0].appendChild(link);
									});
	
									return resolveDependencies($q, $rootScope, dependencies || [controller]);
								}]
						}
					}
	
					return routeDef;
				}
			};
        }	
	}
}
