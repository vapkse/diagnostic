/// <reference path="../../../typings/angularjs/angular.d.ts" />
'use strict';
var Utilities;
(function (Utilities) {
    var Filters = (function () {
        function Filters() {
        }
        Filters.propertyMissing = function () {
            return function (items, property) {
                var returnObj = [];
                if (items) {
                    for (var i = 0; i < items.length; i++) {
                        var test = property !== undefined ? items[i][property] : items[i];
                        if (test === undefined || test === null) {
                            returnObj.push(items[i]);
                        }
                    }
                }
                return returnObj;
            };
        };
        Filters.propertyExists = function () {
            return function (items, property) {
                var returnObj = [];
                if (items) {
                    for (var i = 0; i < items.length; i++) {
                        var test = property !== undefined ? items[i][property] : items[i];
                        if (test !== undefined && test !== null) {
                            returnObj.push(items[i]);
                        }
                    }
                }
                return returnObj;
            };
        };
        Filters.getByProperty = function () {
            return function (propertyName, propertyValue, items) {
                for (var i = 0; i < items.length; i++) {
                    if (items[i][propertyName] === propertyValue) {
                        return items[i];
                    }
                }
                return null;
            };
        };
        Filters.find = function () {
            return function (items, predicate) {
                var i = 0, len = items.length;
                for (; i < len; i++) {
                    if (predicate(items[i]) === true) {
                        return items[i];
                    }
                }
                return null;
            };
        };
        return Filters;
    }());
    Utilities.Filters = Filters;
})(Utilities || (Utilities = {}));
