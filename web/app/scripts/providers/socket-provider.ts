/// <reference path="../../../typings/angularjs/angular.d.ts" />

'use strict';

module SocketIO {
    export interface ISocketOptions extends SocketIOClient.ConnectOpts {
        scope: ng.IScope,
        prefix: string,
        ioSocket: SocketIOClient.Socket
    }

    export interface ICallback extends Function {
        __ng: any
    }

    export class SocketProviders {        
        // Socket.io Provider
        // angular.module('socketProviders', []).provider('socketFactory', SocketIO.SocketProviders.provider).factory('socket', SocketIO.SocketProviders.factory);
        public static provider() {
            // when forwarding events, prefix the event name
            var defaultPrefix = 'socket:'

            return {
                $get: function($rootScope: ng.IRootScopeService, $timeout: ng.ITimeoutService) {
                    var asyncAngularify: Function = function(socket: SocketIOClient.Socket, callback: Function) {
                        return callback ? function() {
                            var args = arguments;
                            $timeout(function() {
                                callback.apply(socket, args);
                            }, 0);
                        } : angular.noop;
                    };

                    return function socketFactory(options: ISocketOptions) {
                        var socket = (options && options.ioSocket) || io.connect();
                        var prefix = (options && options.prefix) || defaultPrefix;
                        var defaultScope = (options && options.scope) || $rootScope;

                        var addListener = function(eventName: string, callback: ICallback) {
                            socket.on(eventName, callback.__ng = asyncAngularify(socket, callback));
                        };

                        var addOnceListener = function(eventName: string, callback: ICallback) {
                            socket.once(eventName, callback.__ng = asyncAngularify(socket, callback));
                        };

                        var wrappedSocket = {
                            on: addListener,
                            addListener: addListener,
                            once: addOnceListener,

                            emit: function() {
                                var lastIndex: number = arguments.length - 1;
                                var cbk = arguments[lastIndex];
                                if (typeof cbk === 'function') {
                                    cbk = asyncAngularify(socket, cbk);
                                    arguments[lastIndex] = cbk;
                                }
                                return socket.emit.apply(socket, arguments);
                            },

                            removeListener: function(ev: string, fn: ICallback) {
                                if (fn && fn.__ng) {
                                    arguments[1] = fn.__ng;
                                }
                                return socket.removeListener.apply(socket, arguments);
                            },

                            removeAllListeners: function() {
                                return socket.removeAllListeners.apply(socket, arguments);
                            },

                            disconnect: function() {
                                return socket.disconnect();
                            },
    
                            // when socket.on('someEvent', fn (data) { ... }),
                            // call scope.$broadcast('someEvent', data)
                            forward: function(events: any, scope: ng.IScope) {
                                if (events instanceof Array === false) {
                                    events = [events];
                                }
                                if (!scope) {
                                    scope = defaultScope;
                                }
                                events.forEach(function(eventName: string) {
                                    var prefixedEvent: string = prefix + eventName;
                                    var forwardBroadcast: Function = asyncAngularify(socket, function(data: any) {
                                        scope.$broadcast(prefixedEvent, data);
                                    });
                                    scope.$on('$destroy', function() {
                                        socket.removeListener(eventName, forwardBroadcast);
                                    });
                                    socket.on(eventName, forwardBroadcast);
                                });
                            }
                        };

                        return wrappedSocket;
                    };
                },
            }
        }

        public static factory(socketFactory: SocketIOClientStatic) {
            return socketFactory();
        }
    }
}
