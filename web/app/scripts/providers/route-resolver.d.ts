/// <reference path="../../../typings/angularjs/angular.d.ts" />
declare module Routage {
    interface IRoutageConfig {
        route: IRouteDescription;
    }
    interface IRouteDescription extends RequireConfig {
        css: {
            [key: string]: string;
        };
        templates: {
            [key: string]: string;
        };
        priority: Array<string>;
    }
    interface IRegisterProvider {
        controller: Function;
        directive: Function;
        filter: Function;
        factory: Function;
        service: Function;
        provider: Function;
        constant: Function;
    }
    interface IModule extends ng.IModule {
        register: IRegisterProvider;
    }
    class RouteResolver {
        static provider(config: IRoutageConfig): {
            $get: () => any;
            resolve: (template: string, controller: string, dependencies: string[], css?: string[]) => {
                templateUrl: string;
                controller: string;
                reloadOnSearch: boolean;
                resolve: {
                    load: (string | (($q: ng.IQService, $rootScope: ng.IRootScopeService) => ng.IPromise<{}>))[];
                };
            };
        };
    }
}
