/// <reference path="../../../typings/angularjs/angular.d.ts" />
var dampApp;
(function (dampApp) {
    'use strict';
    define(['app', 'proxyService'], function (app) {
        app.register.service('dataService', DataService);
    });
    var DataService = (function () {
        function DataService(proxyService) {
            return {
                getAmpInfos: function () {
                    return proxyService.request({
                        method: 'get',
                        cachekey: 'ampinfos',
                        request: 'getjson',
                        args: {
                            filename: 'ampinfos',
                        }
                    });
                },
                postAmpData: function (id) {
                    return proxyService.request({
                        method: 'post',
                        cachekey: 'ampdata' + id,
                        request: 'ampdata',
                        args: {
                            id: id
                        }
                    });
                },
                postAmpStatus: function () {
                    return proxyService.request({
                        method: 'post',
                        cachekey: 'ampstatus',
                        request: 'ampstatus',
                    });
                },
                postAmpHistory: function (id) {
                    return proxyService.request({
                        method: 'post',
                        cachekey: 'amphistory' + id,
                        request: 'amphistory',
                        args: {
                            id: id
                        }
                    });
                },
                getAmpHistoryDatas: function (id, filename) {
                    return proxyService.request({
                        method: 'get',
                        cachekey: 'amphistory_' + id + '_' + filename,
                        request: 'getamphistdata',
                        args: {
                            id: id,
                            hist: filename
                        }
                    });
                },
                postLogList: function (next) {
                    return proxyService.request({
                        method: 'post',
                        request: 'loglist',
                        args: {
                            next: next || -1,
                        }
                    });
                }
            };
        }
        DataService.$inject = ['proxyService'];
        return DataService;
    }());
})(dampApp || (dampApp = {}));
