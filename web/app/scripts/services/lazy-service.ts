/// <reference path="../../../typings/angularjs/angular.d.ts" />

module LazyLoading {
    'use strict';

    if (define) {
        define(['app'], function(app: Routage.IModule) {
            app.register.service('lazyService', LazyService);
        });
    } else {
        angular.module('lazyServices').service('lazyService', LazyService);                
    }

    export class LazyOptions {
        controllerUrl: string;
        templateUrl: string;
        template: any;
        html: Function;
        scope: ng.IScope;
        container: JQuery;
        controller: string;
    }

    export interface ILazyService {
        lazyTemplate(options: LazyOptions, parameters?: any): void            
    } 
    
    class LazyService {
        static $inject = ['$compile', '$controller', '$rootScope', '$q'];
        public constructor($compile: ng.ICompileService, $controller: ng.IControllerService, $rootScope: ng.IRootScopeService, $q: ng.IQService) {
            function load(options: LazyOptions, parameters: any) {
                var lazyBody = (function(options: LazyOptions) {
                    if (options.template) {
                        if (angular.isString(options.template)) {
                            // Simple string template
                            return '<div class="lazy-body">' + options.template + '</div>';
                        }
                        else {
                            // jQuery/JQlite wrapped object
                            return '<div class="lazy-body">' + options.template.html() + '</div>';
                        }
                    }
                    else {
                        // Template url
                        return '<div class="lazy-body" ng-include="\'' + options.templateUrl + '\'"></div>';
                    }
                })(options);

                // Scope
                var scope = options.scope || $rootScope.$new();
                var container = options.container || $('body');

                var lazyEl = angular.element(lazyBody);
                var element = $compile(lazyEl)(scope);
                container.append(lazyEl);

                if (options.controller) {
                    var locals = angular.extend({
                        $scope: scope,
                        $element: element
                    }, parameters);

                    var ctrl = $controller(options.controller, locals);
                    lazyEl.contents().data('$ngControllerController', ctrl);
                }
            }

            function lazy(options: LazyOptions, parameters: any) {
                if (options.controllerUrl) {
                    var defer = $q.defer();
                    requirejs([options.controllerUrl], function() {
                        defer.resolve();
                        $rootScope.$apply();
                    });

                    defer.promise.then(function() {
                        load(options, parameters);
                    });
                }
                else {
                    load(options, parameters);
                }

            }

            return {
                lazyTemplate: function(options: LazyOptions, parameters: any) {
                    if (!options.templateUrl) {
                        throw 'template url is mandatory.';
                    }
                    if (!options.controller && options.controllerUrl) {
                        throw 'controller name url is mandatory if controllerUrl is specified.';
                    }

                    return lazy(options, parameters);
                },
            };
        }
    }
}
