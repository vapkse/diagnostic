/// <reference path="../../../typings/angularjs/angular.d.ts" />
declare module Modal {
    interface IModalScope extends ng.IScope {
        $modalClose(): void;
        $modalCancel(isBackdrop: boolean): void;
        $modalSuccess(): void;
        htmlHandler(e: Event): void;
        $title: string;
        $modalSuccessLabel: string;
        $modalCancelLabel: string;
    }
    class IModalCbOptions {
        label: string;
        fn: Function;
    }
    class ModalOptions {
        id: string;
        template: any;
        templateUrl: string;
        title: string;
        backdrop: boolean;
        backdropCancel: boolean;
        backdropClass: string;
        controllerUrl: string;
        html: Function;
        scope: ng.IScope;
        container: JQuery;
        controller: ng.IControllerService;
        headerTemplate: string;
        footerTemplate: string;
        modalClass: string;
        css: {
            [index: string]: any;
        };
        parent: JQuery;
        parentClass: string;
        animEnterCss: string;
        animExitCss: string;
        animDuration: number;
        success: IModalCbOptions;
        cancel: IModalCbOptions;
        backdropClicked: Function;
        onclosed: Function;
        cancelOnEsc: boolean;
    }
    interface IModalService {
        ContextMenu(templateUrl: string, options: any, parameters: any): IModalScope;
        PopupWindow(templateUrl: string, options: any, parameters: any): IModalScope;
        closeAllModalInstances(): void;
        addModalInstance(name: string, closefn: Function): void;
        removeModalInstance(name: string): void;
    }
}
