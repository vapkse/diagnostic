/// <reference path="../../../typings/angularjs/angular.d.ts" />
declare module dampApp {
    interface IDataService {
        getAmpInfos(): ng.IPromise<SocketIO.IProxyRequestResult>;
        postAmpData(id: number): ng.IPromise<SocketIO.IProxyRequestResult>;
        postAmpStatus(): ng.IPromise<SocketIO.IProxyRequestResult>;
        postAmpHistory(id: number): ng.IPromise<SocketIO.IProxyRequestResult>;
        getAmpHistoryDatas(id: number, filename: string): ng.IPromise<SocketIO.IProxyRequestResult>;
        postLogList(next: number): ng.IPromise<SocketIO.IProxyRequestResult>;
    }
}
