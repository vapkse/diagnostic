/// <reference path="../../../typings/angularjs/angular.d.ts" />

module Modal {
    'use strict';

    if (define) {
        define(['app'], function(app: Routage.IModule) {
            app.register.service('modalService', ModalService);
        });
    } else {
        angular.module('modalServices').service('modalService', ModalService);
    }

    export interface IModalScope extends ng.IScope {
        $modalClose(): void;
        $modalCancel(isBackdrop: boolean): void;
        $modalSuccess(): void;
        htmlHandler(e: Event): void,
        $title: string;
        $modalSuccessLabel: string;
        $modalCancelLabel: string;
    }

    export class IModalCbOptions {
        label: string;
        fn: Function;
    }

    export class ModalOptions {
        id: string;
        template: any;
        templateUrl: string;
        title: string = '';
        backdrop: boolean = true;
        backdropCancel: boolean = true;
        backdropClass: string;
        controllerUrl: string;
        html: Function;
        scope: ng.IScope;
        container: JQuery;
        controller: ng.IControllerService;
        headerTemplate: string;
        footerTemplate: string;
        modalClass: string;
        css: { [index: string]: any };
        parent: JQuery;
        parentClass: string;
        animEnterCss: string;
        animExitCss: string;
        animDuration: number;
        success: IModalCbOptions;
        cancel: IModalCbOptions;
        backdropClicked: Function;
        onclosed: Function;
        cancelOnEsc: boolean;
    }

    export interface IModalService {
        ContextMenu(templateUrl: string, options: any, parameters: any): IModalScope
        PopupWindow(templateUrl: string, options: any, parameters: any): IModalScope
        closeAllModalInstances(): void,
        addModalInstance(name: string, closefn: Function): void,
        removeModalInstance(name: string): void,
    }

    class ModalService {
        static $inject = ['$document', '$compile', '$rootScope', '$controller', '$timeout'];
        static modalInstances: { [index: string]: Function } = {};
        public constructor($document: Document, $compile: ng.ICompileService, $rootScope: ng.IRootScopeService, $controller: ng.IControllerService, $timeout: ng.ITimeoutService) {
            var contextMenuDefaults: ModalOptions = new ModalOptions();
            contextMenuDefaults.backdrop = false;
            contextMenuDefaults.backdropCancel = false;
            contextMenuDefaults.success = {
                label: 'OK',
                fn: null
            };
            contextMenuDefaults.cancel = {
                label: 'Cancel',
                fn: null
            };
            contextMenuDefaults.modalClass = 'menu';
            contextMenuDefaults.css = {
                top: '130px',
                width: '130px',
            }


            var popupWindowDefaults: ModalOptions = new ModalOptions();
            popupWindowDefaults.title = 'Default Title';
            popupWindowDefaults.backdrop = true;
            popupWindowDefaults.success = {
                label: 'OK',
                fn: null
            };
            popupWindowDefaults.cancel = {
                label: 'Close',
                fn: null
            };
            popupWindowDefaults.backdropCancel = true;
            popupWindowDefaults.footerTemplate = '<div class="modal-footer"><button class="btn" ng-click="$modalCancel()">{{$modalCancelLabel}}</button><button class="btn btn-primary" ng-click="$modalSuccess()">{{$modalSuccessLabel}}</button></div>';
            popupWindowDefaults.headerTemplate = '<div class="modal-header"><button type="button" class="btn closebtn icon icon-cancel" ng-click="$modalCancel()"></button><h2>{{$title}}</h2></div>';
            popupWindowDefaults.cancelOnEsc = true;
            popupWindowDefaults.css = {
                top: '10%',
                width: '560px'
            };

            class Modal {
                public constructor(options: ModalOptions, parameters: any) {
                    var key: string;
                    var idAttr = options.id ? ' id="' + options.id + '" ' : '';
                    var headerTemplate = options.headerTemplate;
                    var footerTemplate = options.footerTemplate;
                    var modalBody = (function() {
                        if (options.template) {
                            if (angular.isString(options.template)) {
                                // Simple string template
                                return '<div class="modal-body">' + options.template + '</div>';
                            }
                            else {
                                // jQuery/JQlite wrapped object
                                return '<div class="modal-body">' + options.template.html() + '</div>';
                            }
                        }
                        else {
                            // Template url
                            return '<div class="modal-body" ng-include="\'' + options.templateUrl + '\'"></div>';
                        }
                    })();
                    //We don't have the scope we're gonna use yet, so just get a compile function for modal
                    var modalEl = angular.element('<div class="' + options.modalClass + ' modal ' + options.animEnterCss + '"' + idAttr + ' style="display: block;"><div class="modal-dialog"><div class="modal-content">' + headerTemplate + modalBody + footerTemplate + '</div></div></div>');

                    for (key in options.css) {
                        modalEl.css(key.replace('_', '-'), options.css[key]);
                    }
                    var divHTML = "<div ";
                    if (options.backdropCancel) {
                        divHTML += 'ng-click="$modalCancel(true)"';
                    }
                    divHTML += ">";
                    var backdropEl: ng.IAugmentedJQuery;
                    if (options.backdrop) {
                        backdropEl = angular.element(divHTML);
                        backdropEl.addClass(options.backdropClass);
                        backdropEl.addClass('modal-backdrop fade in');
                    }

                    if (options.parent && options.parentClass) {
                        $(options.parent).addClass(options.parentClass);
                    }

                    var handleEscPressed = function(event: JQueryKeyEventObject) {
                        if (event.keyCode === 27 && options.cancelOnEsc) {
                            scope.$modalCancel(false);
                        }
                    };

                    var body: JQuery = $(body);
                    body.on('keydown', handleEscPressed);

                    var closeFn = function() {
                        body.off('keydown');
                        if (scope.htmlHandler) {
                            body.off('click');
                        }

                        modalEl.removeClass(options.animEnterCss);
                        modalEl.addClass(options.animExitCss);

                        var duration = options.animDuration || 500;
                        if (!options.animExitCss) {
                            duration = 0;
                        }
                        $timeout(function() {
                            modalEl.remove();
                        }, duration);

                        if (backdropEl) {
                            backdropEl.remove();
                        }

                        if (options.parent && options.parentClass) {
                            $(options.parent).removeClass(options.parentClass);
                        }

                        if (options.onclosed) {
                            options.onclosed();
                        }
                    };

                    var ctrl: {};
                    var locals: any;
                    var scope: IModalScope = <IModalScope>(options.scope || $rootScope.$new());

                    scope.$title = options.title;
                    scope.$modalClose = closeFn;
                    scope.$modalCancel = function(isBackdrop: boolean) {
                        var e = {
                            cancel: false,
                            scope: scope,
                            isBackdrop: isBackdrop || false,
                            options: options
                        };
                        var callFn = options.cancel.fn || closeFn;
                        var promise = callFn.call(this, e);
                        if (promise && promise.then) {
                            promise.then(function() {
                                scope.$modalClose();
                            });
                        }
                        else {
                            if (e.cancel) {
                                return;
                            }
                            scope.$modalClose();
                        }
                    };
                    scope.$modalSuccess = function() {
                        var e = {
                            cancel: false,
                            scope: scope
                        };
                        var callFn = options.success.fn || closeFn;
                        var promise = callFn.call(this, e);
                        if (promise && promise.then) {
                            promise.then(function() {
                                scope.$modalClose();
                            });
                        }
                        else {
                            if (e.cancel) {
                                return;
                            }
                            scope.$modalClose();
                        }
                    };
                    scope.$modalSuccessLabel = options.success.label;
                    scope.$modalCancelLabel = options.cancel.label;

                    var element = $compile(modalEl)(scope);
                    body.append(modalEl);
                    if (backdropEl) {
                        $compile(backdropEl)(scope);
                        body.append(backdropEl);
                    }
                    else {
                        // let the menu open
                        $timeout(function() {
                            scope.htmlHandler = function(event: Event) {
                                var target = $(event.target);
                                if (element[0] !== target[0] && !element.has(target[0])) {
                                    scope.$modalCancel(true);
                                }
                            };

                            body.on('click', scope.htmlHandler);
                        }, 200);
                    }

                    if (options.controller) {
                        locals = angular.extend({
                            $scope: scope,
                            $element: element
                        }, parameters);
                        ctrl = $controller(options.controller, locals);
                        // Yes, ngControllerController is not a typo
                        modalEl.contents().data('$ngControllerController', ctrl);
                    }

                    $timeout(function() {
                        modalEl.addClass('in');
                    }, 200);

                    return scope;
                }
            }

            return {
                ContextMenu: function(templateUrl: string, options: any, parameters: any) {
                    // Handle arguments if optional template isn't provided.
                    if (angular.isObject(templateUrl)) {
                        parameters = options;
                        options = templateUrl;
                    }
                    else {
                        options.templateUrl = templateUrl;
                    }

                    options = angular.extend({}, contextMenuDefaults, options);

                    return new Modal(options, parameters);
                },

                PopupWindow: function(templateUrl: string, options: any, parameters: any) {
                    // Handle arguments if optional template isn't provided.
                    if (angular.isObject(templateUrl)) {
                        parameters = options;
                        options = templateUrl;
                    }
                    else {
                        options.templateUrl = templateUrl;
                    }

                    options = angular.extend({}, popupWindowDefaults, options);

                    return new Modal(options, parameters);
                },

                closeAllModalInstances: function() {
                    for (var name in ModalService.modalInstances) {
                        if (typeof ModalService.modalInstances[name] === 'function') {
                            ModalService.modalInstances[name]();
                        }
                    }
                },

                addModalInstance: function(name: string, closefn: Function) {
                    ModalService.modalInstances[name] = closefn;
                },

                removeModalInstance: function(name: string) {
                    delete ModalService.modalInstances[name];
                },
            };
        }
    }
}

