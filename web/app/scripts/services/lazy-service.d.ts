/// <reference path="../../../typings/angularjs/angular.d.ts" />
declare module LazyLoading {
    class LazyOptions {
        controllerUrl: string;
        templateUrl: string;
        template: any;
        html: Function;
        scope: ng.IScope;
        container: JQuery;
        controller: string;
    }
    interface ILazyService {
        lazyTemplate(options: LazyOptions, parameters?: any): void;
    }
}
