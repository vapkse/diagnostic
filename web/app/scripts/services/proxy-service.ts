'use strict';

module SocketIO {

    if (define) {
        define(['app'], function(app: Routage.IModule) {
            app.register.service('proxyService', ProxyService);
        });
    } else {
        angular.module('proxyServices', ['socketProviders']).service('proxyService', ProxyService);
    }
    
    export interface IProxyService {
        request(params: IProxyRequestParams): ng.IPromise<IProxyRequestResult>
    }
    
    class ProxyService {
        static $inject = ['$http', '$q', '$cacheFactory', 'socket', '$rootScope'];
        public constructor($http: ng.IHttpProvider, $q: ng.IQService, $cacheFactory: ng.ICacheFactoryService, socket: SocketIOClient.Socket, $rootScope: ng.IRootScopeService) {
            // This is the extent of $cacheFactory's configuration
            var metaCache = $cacheFactory('metaCache', {
                // This cache can hold 1000 items
                capacity: 1000
            });

            socket.on('response', function(receivedData: IProxyRequestResponse) {
                var result = receivedData.result;
                if (result.status === 'ok') {
                    if (result.cachekey) {
                        // Update cache
                        if (result.mode === 'update' && result.data.list) {
                            var data: IProxyRequestResultDatas = metaCache.get(result.cachekey);
                            data.list = data.list || [];
                            for (var i = 0; i < result.data.list.length; i++) {
                                data.list.push(result.data.list[i]);
                            }

                            metaCache.put(result.cachekey, data);
                            result.data = data;
                        }
                        else {
                            metaCache.put(result.cachekey, result.data);
                        }
                    }
                    $rootScope.$emit('response', result);
                }
                else {
                    $rootScope.$emit('error', result);
                }
            });

            return {                
                request: function(params: IProxyRequestParams) {
                    var deferred = $q.defer();

                    params.args = params.args || {};

                    var cachekey = params.cachekey;
                    if (cachekey) {
                        var data = metaCache.get(cachekey);
                        if (data) {
                            var result = <IProxyRequestResult>params;
                            result.data = data;
                            if (params.method === 'post') {
                                $rootScope.$emit('response', result);
                            }
                            deferred.resolve(result);
                            return deferred.promise;
                        }
                    }

                    if (!params.method || params.method === 'post') {
                        socket.emit('request', params);
                    }
                    else {
                        socket.emit('request', params, function(result: IProxyRequestResult) {
                            if (result.status === 'ok') {
                                if (cachekey) {
                                    metaCache.put(cachekey, result.data);
                                }

                                deferred.resolve(result);
                            }
                            else {
                                deferred.reject(result);
                            }
                        });
                    }
                    return deferred.promise;
                }
            }
        }
    }
}

