declare module SocketIO {
    interface IProxyService {
        request(params: IProxyRequestParams): ng.IPromise<IProxyRequestResult>;
    }
}
