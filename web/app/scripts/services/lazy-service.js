/// <reference path="../../../typings/angularjs/angular.d.ts" />
var LazyLoading;
(function (LazyLoading) {
    'use strict';
    if (define) {
        define(['app'], function (app) {
            app.register.service('lazyService', LazyService);
        });
    }
    else {
        angular.module('lazyServices').service('lazyService', LazyService);
    }
    var LazyOptions = (function () {
        function LazyOptions() {
        }
        return LazyOptions;
    }());
    LazyLoading.LazyOptions = LazyOptions;
    var LazyService = (function () {
        function LazyService($compile, $controller, $rootScope, $q) {
            function load(options, parameters) {
                var lazyBody = (function (options) {
                    if (options.template) {
                        if (angular.isString(options.template)) {
                            // Simple string template
                            return '<div class="lazy-body">' + options.template + '</div>';
                        }
                        else {
                            // jQuery/JQlite wrapped object
                            return '<div class="lazy-body">' + options.template.html() + '</div>';
                        }
                    }
                    else {
                        // Template url
                        return '<div class="lazy-body" ng-include="\'' + options.templateUrl + '\'"></div>';
                    }
                })(options);
                // Scope
                var scope = options.scope || $rootScope.$new();
                var container = options.container || $('body');
                var lazyEl = angular.element(lazyBody);
                var element = $compile(lazyEl)(scope);
                container.append(lazyEl);
                if (options.controller) {
                    var locals = angular.extend({
                        $scope: scope,
                        $element: element
                    }, parameters);
                    var ctrl = $controller(options.controller, locals);
                    lazyEl.contents().data('$ngControllerController', ctrl);
                }
            }
            function lazy(options, parameters) {
                if (options.controllerUrl) {
                    var defer = $q.defer();
                    requirejs([options.controllerUrl], function () {
                        defer.resolve();
                        $rootScope.$apply();
                    });
                    defer.promise.then(function () {
                        load(options, parameters);
                    });
                }
                else {
                    load(options, parameters);
                }
            }
            return {
                lazyTemplate: function (options, parameters) {
                    if (!options.templateUrl) {
                        throw 'template url is mandatory.';
                    }
                    if (!options.controller && options.controllerUrl) {
                        throw 'controller name url is mandatory if controllerUrl is specified.';
                    }
                    return lazy(options, parameters);
                },
            };
        }
        LazyService.$inject = ['$compile', '$controller', '$rootScope', '$q'];
        return LazyService;
    }());
})(LazyLoading || (LazyLoading = {}));
