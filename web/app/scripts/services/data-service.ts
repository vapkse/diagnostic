/// <reference path="../../../typings/angularjs/angular.d.ts" />

module dampApp {
    'use strict';

    define(['app', 'proxyService'], function(app: Routage.IModule) {
        app.register.service('dataService', DataService);
    });

    export interface IDataService {
        getAmpInfos(): ng.IPromise<SocketIO.IProxyRequestResult>,
        postAmpData(id: number): ng.IPromise<SocketIO.IProxyRequestResult>,
        postAmpStatus(): ng.IPromise<SocketIO.IProxyRequestResult>,
        postAmpHistory(id: number): ng.IPromise<SocketIO.IProxyRequestResult>,
        getAmpHistoryDatas(id: number, filename: string): ng.IPromise<SocketIO.IProxyRequestResult>,   
        postLogList(next: number): ng.IPromise<SocketIO.IProxyRequestResult>,
    }
    
    class DataService {
        static $inject = ['proxyService'];
        public constructor(proxyService: SocketIO.IProxyService) {
            return {
                getAmpInfos: function() {
                    return proxyService.request({
                        method: 'get',
                        cachekey: 'ampinfos',
                        request: 'getjson',
                        args: {
                            filename: 'ampinfos',
                        }
                    });
                },
                postAmpData: function(id: number) {
                    return proxyService.request({
                        method: 'post',
                        cachekey: 'ampdata' + id,
                        request: 'ampdata',
                        args: {
                            id: id
                        }
                    });
                },
                postAmpStatus: function() {
                    return proxyService.request({
                        method: 'post',
                        cachekey: 'ampstatus',
                        request: 'ampstatus',
                    });
                },
                postAmpHistory: function(id: number) {
                    return proxyService.request({
                        method: 'post',
                        cachekey: 'amphistory' + id,
                        request: 'amphistory',
                        args: {
                            id: id
                        }
                    });
                },
                getAmpHistoryDatas: function(id: number, filename: string) {
                    return proxyService.request({
                        method: 'get',
                        cachekey: 'amphistory_' + id + '_' + filename,
                        request: 'getamphistdata',
                        args: {
                            id: id,
                            hist: filename
                        }
                    });
                },
                postLogList: function(next: number) {
                    return proxyService.request({
                        method: 'post',
                        request: 'loglist',
                        args: {
                            next: next || -1,
                        }
                    });
                }
            }
        }
    }
}
