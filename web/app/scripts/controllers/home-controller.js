/// <reference path="../../../typings/angularjs/angular.d.ts" />
'use strict';
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var dampApp;
(function (dampApp) {
    define(['app', 'dataService', 'lazyService', 'multiselectDirective', 'sliderDirective', 'titleDirective'], function (app) {
        app.register.controller('homeController', function ($rootScope, $scope, dataService, $location, config, $filter, $route, $timeout, lazyService) { return new HomeController($rootScope, $scope, dataService, $location, config, $filter, $route, $timeout, lazyService); });
    });
    var FileNameDecoder = (function () {
        function FileNameDecoder(filename) {
            var _this = this;
            var fileargs = filename.split('_');
            _this.datenum = parseInt(fileargs[0]);
            _this.session = parseInt(fileargs[1]);
            _this.tick = parseInt(fileargs[2]);
        }
        return FileNameDecoder;
    }());
    var History = (function (_super) {
        __extends(History, _super);
        function History(filename) {
            _super.call(this, filename);
            this.toString = function () {
                var _this = this;
                return _this.date.toLocaleDateString() + '-' + _this.session + ' (' + (_this.tick > 0 ? _this.duty.toLocaleTimeString() : 'Archive') + ')';
            };
            var _this = this;
            var y = parseInt(filename.substr(0, 4));
            var m = parseInt(filename.substr(4, 2));
            var d = parseInt(filename.substr(6, 2));
            _this.date = new Date(y, m - 1, d);
            _this.duty = new Date(1970, 1, 1, 0, 0, 0, _this.tick);
        }
        return History;
    }(FileNameDecoder));
    var HomeController = (function () {
        function HomeController(rootScope, scope, dataService, location, config, $filter, $route, $timeout, lazyService) {
            var $rootScope = rootScope;
            var $scope = scope;
            var $location = location;
            var displayDatasTimer;
            var animationTimers = {};
            var timeoutTimers = {};
            var historyTimer;
            $scope.ampStatus = {};
            // Get amplifiers info list
            var promise = dataService.getAmpInfos().then(function (infos) {
                var params = $location.search();
                $scope.ampInfos = infos.data;
                // Ask server to post all amplifiers status
                dataService.postAmpStatus();
                // Get selected amp on the url
                var selectedAmp;
                var id = parseInt(params.id);
                if (id && !isNaN(id)) {
                    selectedAmp = $scope.ampInfos.filter(function (value) {
                        return value.id === id;
                    });
                }
                if (selectedAmp && selectedAmp.length) {
                    $scope.selectedAmp = selectedAmp[0];
                    $timeout(function () {
                        /* global LazyLoading */
                        var opt = new LazyLoading.LazyOptions();
                        opt.controllerUrl = $scope.selectedAmp.controllerUrl;
                        opt.templateUrl = $scope.selectedAmp.templateUrl;
                        opt.scope = $scope;
                        opt.container = $('#ampinfos');
                        opt.controller = $scope.selectedAmp.controller;
                        lazyService.lazyTemplate(opt);
                    }, 100);
                    // Ask server to post amp data
                    dataService.postAmpData(params.id);
                    // Ask server to post history file list
                    dataService.postAmpHistory(params.id);
                    // Get history on the url
                    if (params.hist) {
                        loadDeferedHistory(params.id, params.hist);
                    }
                    else {
                        $scope.historyFolded = true;
                    }
                }
            });
            promise['catch'](function (result) {
                $scope.lastErr = result;
            });
            var loadDeferedHistory = function (id, filename) {
                if (historyTimer) {
                    $timeout.cancel(historyTimer);
                }
                historyTimer = $timeout(function () {
                    var promise = dataService.getAmpHistoryDatas(id, filename);
                    promise.then(function (requestResult) {
                        var result = requestResult;
                        $scope.selectedHistory = new History(filename);
                        $scope.selectedHistory.datas = result.data.list;
                        // Load slider
                        /* global vapkse */
                        var opt = new vapkse.SliderOptions();
                        opt.start = 0;
                        opt.end = result.data.list.length - 1;
                        opt.value = $location.search().time || 0;
                        opt.startText = function () {
                            return this.valueText(this.start);
                        };
                        opt.endText = function () {
                            return this.valueText(this.end);
                        };
                        opt.valueText = function (value) {
                            var index = Math.round(value);
                            var tick = index < result.data.list.length ? result.data.list[index].tick : 0;
                            var date = new Date(1970, 1, 1, 0, 0, 0, tick);
                            return date.toLocaleTimeString();
                        };
                        opt.onvaluechanged = function (value) {
                            var i = Math.round(value);
                            var data = $scope.selectedHistory.datas[i];
                            displayDatas(data);
                            $location.search('time', i);
                            $location.$$compose();
                        };
                        $scope.slhistory = opt;
                        displayDatas($scope.selectedHistory.datas[$scope.slhistory.value]);
                        $scope.historyFolded = false;
                    });
                    promise['catch'](function (result) {
                        $scope.lastErr = result;
                    });
                    historyTimer = null;
                }, 200);
            };
            // Register to post responses
            delete $rootScope.$$listeners.response;
            $rootScope.$on('response', function (e, result) {
                switch (result.request) {
                    case 'ampdata':
                        if ($scope.selectedAmp && $scope.selectedAmp.id === result.args.id) {
                            $scope.selectedAmp.fileName = result.fileName;
                            $scope.selectedAmp.data = result.data;
                            if (!$scope.selectedHistory) {
                                result.data.fromMeasure = true;
                                displayDatas(result.data);
                            }
                        }
                        setAlive(result.args.id);
                        break;
                    case 'ampstatus':
                        for (var i = 0; i < result.data.length; i++) {
                            setAlive(result.data[i]);
                        }
                        break;
                    case 'amphistory':
                        var ampiid = $location.search().id;
                        if (result.args.id === ampiid) {
                            var list = result.data.list.sort(function (a, b) {
                                var aa = new FileNameDecoder(a);
                                var bb = new FileNameDecoder(b);
                                var result = aa.datenum - bb.datenum;
                                if (result === 0) {
                                    result = aa.session - bb.session;
                                    if (result === 0) {
                                        result = aa.tick - bb.tick;
                                    }
                                }
                                return -result;
                            });
                            var items = ($scope.mshistory && $scope.mshistory.items) || [];
                            for (var ii = 0; ii < list.length; ii++) {
                                items.push(result.data.list[ii]);
                            }
                            /* global vapkse */
                            var opts = new vapkse.multiselect.Options();
                            opts.items = items;
                            opts.selectedValue = $location.search().hist;
                            opts.selectedtext = !$location.search().hist && items.length > 0 ? "Select an history.." : "No history";
                            opts._isReadOnly = items.length === 0;
                            opts.getItemText = function (item) {
                                if (!item) {
                                    return '';
                                }
                                var history = new History(item);
                                return history.toString();
                            };
                            opts.onitemselected = function (item) {
                                if (!item) {
                                    return;
                                }
                                if ($location.search().hist === item.value()) {
                                    return;
                                }
                                $location.search('hist', item.value()).search('time', 0);
                                $location.$$compose();
                                loadDeferedHistory(ampiid, item.value());
                            };
                            $scope.mshistory = opts;
                        }
                        break;
                }
            });
            $scope.historyFoldChanged = function (value, userclick) {
                if (userclick && $scope.selectedAmp) {
                    if (value) {
                        delete $location.$$search.hist;
                        delete $location.$$search.time;
                        $location.$$compose();
                        delete $scope.selectedHistory;
                        displayDatas($scope.selectedAmp.data);
                    }
                    else {
                        var item = $scope.mshistory && $scope.mshistory.selectedItem();
                        if (item) {
                            $location.search('hist', item.value());
                            $location.search('time', 0);
                            $location.$$compose();
                            loadDeferedHistory($location.search().id, item.value());
                        }
                    }
                }
            };
            $scope.getAmpStatusLabel = function () {
                if ($scope.selectedAmp && $scope.selectedAmp.id) {
                    if ($scope.selectedHistory) {
                        return 'History of the session ' + $scope.selectedHistory.toString();
                    }
                    else if ($scope.ampStatus[$scope.selectedAmp.id]) {
                        return animationTimers[$scope.selectedAmp.id] ? 'Running..' : 'Running?';
                    }
                    else {
                        return 'Stopped';
                    }
                }
                return '';
            };
            $scope.getAmpLabelClass = function (id) {
                var className = [];
                if ($scope.ampStatus[id]) {
                    className.push('ampon');
                }
                if ($scope.selectedAmp && $scope.selectedAmp.id === id) {
                    className.push('selected');
                }
                return className.join(' ');
            };
            $scope.getSelectedAmpClass = function () {
                var className = $scope.selectedAmp ? 'amp' + $scope.selectedAmp.id : '';
                if ($scope.selectedAmp && $scope.ampStatus[$scope.selectedAmp.id]) {
                    className += ' ampon';
                }
                if ($scope.selectedHistory) {
                    className += ' history';
                }
                return className;
            };
            $scope.getPanelButtonClass = function (selector) {
                var relatedElement = $(selector);
                return relatedElement.hasClass('uk-active') ? 'open' : '';
            };
            $scope.goto = function (ampid) {
                delete $location.$$search.hist;
                delete $location.$$search.time;
                if (ampid) {
                    $location.$$search.id = String(ampid);
                }
                else {
                    delete $location.$$search.id;
                    $location.path(config.page.logs);
                }
                $location.$$compose();
                $route.reload();
            };
            var displayDatas = function (data) {
                if (displayDatasTimer) {
                    $timeout.cancel(displayDatasTimer);
                }
                displayDatasTimer = $timeout(function () {
                    $scope.displayedDatas = data;
                    displayDatasTimer = undefined;
                }, 25);
            };
            var setAlive = function (id) {
                var label = $('.damp #menu .amp' + id);
                label.addClass('animate');
                $scope.ampStatus[id] = Date.now();
                if (animationTimers[id]) {
                    $timeout.cancel(animationTimers[id]);
                }
                if (timeoutTimers[id]) {
                    $timeout.cancel(timeoutTimers[id]);
                }
                animationTimers[id] = $timeout(function (id) {
                    var label = $('.damp #menu .amp' + id);
                    label.removeClass('animate');
                    animationTimers[id] = null;
                    timeoutTimers[id] = $timeout(function (id) {
                        // Time out, amp is off
                        $scope.ampStatus[id] = null;
                        if ($scope.selectedAmp && $scope.selectedAmp.id === id) {
                            $scope.selectedAmp.data = null;
                            displayDatas(null);
                        }
                        timeoutTimers[id] = null;
                    }.bind(this, id), 6000);
                }.bind(this, id), 6000);
            };
            if (!$location.search().id) {
                $.UIkit.offcanvas.show('#menu');
            }
            else {
                $.UIkit.offcanvas.hide();
            }
        }
        return HomeController;
    }());
    dampApp.HomeController = HomeController;
})(dampApp || (dampApp = {}));
