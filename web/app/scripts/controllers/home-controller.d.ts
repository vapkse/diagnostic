/// <reference path="../../../typings/angularjs/angular.d.ts" />
declare module dampApp {
    class HomeController {
        constructor(rootScope: ng.IRootScopeService, scope: ng.IScope, dataService: IDataService, location: ng.ILocationService, config: IConfig, $filter: ng.IFilterService, $route: ng.route.IRouteService, $timeout: ng.ITimeoutService, lazyService: LazyLoading.ILazyService);
    }
}
