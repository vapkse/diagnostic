/// <reference path="../../../typings/angularjs/angular.d.ts" />

'use strict';

module dampApp {
    define(['app', 'dataService', 'uikit'], function(app: Routage.IModule) {
        app.register.controller('logsController', ($rootScope: ng.IRootScopeService, $scope: ng.IScope, dataService: IDataService) => new LogsController($rootScope, $scope, dataService));
    });

    interface localScope extends ng.IScope {
        logs: Array<any>,
    }
    
    interface localRootScope extends ng.IRootScopeService {
        $$listeners: any,
    }

    export class LogsController {
        constructor(rootScope: ng.IRootScopeService, scope: ng.IScope, dataService: IDataService) {
            var $scope = <localScope>scope;
            var $rootScope = <localRootScope>rootScope;
            
            // Ask server to post the list of logs
            dataService.postLogList(65);

            // Register to post responses
            delete $rootScope.$$listeners.response;
            $rootScope.$on('response', function(e, res) {
                var result = <SocketIO.IProxyRequestResult>res;
                switch (result.request) {
                    case 'loglist':
                        $scope.logs = result.data.list;
                }
            });

            $.UIkit.offcanvas.offcanvas.hide();
        }
    }
} 
