/// <reference path="../../../typings/angularjs/angular.d.ts" />
'use strict';
var dampApp;
(function (dampApp) {
    define(['app', 'dataService', 'uikit'], function (app) {
        app.register.controller('logsController', function ($rootScope, $scope, dataService) { return new LogsController($rootScope, $scope, dataService); });
    });
    var LogsController = (function () {
        function LogsController(rootScope, scope, dataService) {
            var $scope = scope;
            var $rootScope = rootScope;
            // Ask server to post the list of logs
            dataService.postLogList(65);
            // Register to post responses
            delete $rootScope.$$listeners.response;
            $rootScope.$on('response', function (e, res) {
                var result = res;
                switch (result.request) {
                    case 'loglist':
                        $scope.logs = result.data.list;
                }
            });
            $.UIkit.offcanvas.offcanvas.hide();
        }
        return LogsController;
    }());
    dampApp.LogsController = LogsController;
})(dampApp || (dampApp = {}));
