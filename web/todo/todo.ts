// Log managment and conversion for archives or external logs
'use strict';

import fs = require('fs');
import path = require('path');
import config = require('../config');

export class todo {
    constructor() {
        var _this: todo = this;

        setInterval(function() {

            // Convert existing logs from Arduino to history files
            try {
                if (!config.path.todo || !fs.existsSync(config.path.todo)) {
                    return;
                }

                fs.readdir(config.path.todo, function(err, files) {
                    if (!err) {
                        for (var i = 0; i < files.length; i++) {
                            if (files[i].indexOf('20150101') === 0) {
                                var filename = path.join(config.path.todo, '/' + files[i]);
                                var fc = fs.readFileSync(filename, 'utf8');
                                if (fc.indexOf('{"tick"') === 0) {
                                    var pos = fc.indexOf('"logs":[');
                                    if (pos > 0) {
                                        var fo = fc.substr(pos + 8, fc.length - pos - 10);
                                        fs.writeFileSync(filename, fo, 'utf8');
                                    }
                                } else {
                                    //debugger;
                                }                                
                            }
                        }
                    }
                    else {
                        throw err;
                    }
                });
            }
            catch (e) {
                console.error("Log analysis error: " + e.message || e);
            }
        }, 1000);
    }
}

