﻿/// <reference path="./typings/body-parser/body-parser.d.ts" />
/// <reference path="./typings/cookie-parser/cookie-parser.d.ts" />
/// <reference path="./typings/debug/debug.d.ts" />
/// <reference path="./typings/express/express.d.ts" />
/// <reference path="./typings/node/node.d.ts" />
/// <reference path="./typings/serve-favicon/serve-favicon.d.ts" />

'use strict';

import express = require('express');
import path = require('path');
import favicon = require('serve-favicon');
import log4js = require('log4js');
import cookieParser = require('cookie-parser');
import bodyParser = require('body-parser');
import http = require('http');
import ejs = require('ejs');
import server = require('./server');
import config = require('./config');

var logtofile = false;
var loglevel = log4js.levels.INFO;
var app = express();

// Read command line arguments
process.argv.forEach(function(val: string) {
    switch (val) {
        case '-logdbg':
            loglevel = log4js.levels.DEBUG;
            break;

        case '-logtofile':
            logtofile = true;
            break;
    }
});

var appenders = [{
    type: 'console',
    filename: ''
}];

if (logtofile) {
    appenders.push({
        type: 'file',
        filename: config.path.base + '\\server.log',
    });
}

log4js.configure({
    appenders: appenders
});

var logger = log4js.getLogger();
logger.setLevel(loglevel);

// view engine setup
app.set('port', config.listenPort || 8000);
app.set('views', config.path.views);
app.set('view engine', 'ejs');
app.engine('html', ejs.renderFile);

// uncomment after placing your favicon in /public
//app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(log4js.connectLogger(logger, {
    level: loglevel
}));
app.use(bodyParser.json());
app.use(bodyParser.json({ type: 'application/vnd.api+json' })); // parse application/vnd.api+json as json
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use(express.static(config.path.app));

app.use('/*', function(req, res){
  res.sendfile(config.path.app + '/index.html');
});

var httpServer = http.createServer(app).listen(app.get('port'), function() {
    process.title = 'Diagnostic Server';
    console.log('Diagnostic server listening on port ' + app.get('port'));
});

new server.server(httpServer, logger);

export = app;
